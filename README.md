# Sistema de control de Áreas Privadas de Conservación

![os_badge](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/os_badge.svg)
![server_apache](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/server_apache.svg)
![database_mysql](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/database_mysql.svg)
![database_postgresql](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/database_postgresql.svg)
![backend_php](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/backend_php.svg)
![library_openlayers](https://gitlab.com/pronaturaveracruz/geoportal-apc/raw/master/images/badges/library_openlayers.svg)
![server_geoserver](https://gitlab.com/pronaturaveracruz/geoportal-apc/raw/master/images/badges/server_geoserver.svg)

Portal para visualizar en un mapa las áreas privadas de conservación.

<http://geoportal.pronaturaveracruz.org>