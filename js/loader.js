Ext.Loader.setConfig({
	enabled: true,
	disableCaching: false,
	paths: {
		GeoExt: "includes/geoext2-2.1.0/src/GeoExt",
		Ext: "includes/ext-5.1.0/src",
		'Ext.ux': 'includes/ext-5.1.0/examples/ux'
	}
});