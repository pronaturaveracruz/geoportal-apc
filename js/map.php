<?php 
error_reporting(0);
date_default_timezone_set("Mexico/General");
include("../includes/conexion.php");
$link = ConectarsePostgreSQL();
session_start();
$usuario_actual   = $_SESSION['usuario'];
$rutaGeoserver = HOST_GEOSERVER;
#VALIDAMOS SI TIENE ACCESO EL USUARIO DE LA SESIÓN
$validUser = pg_query($link, "SELECT nombre_completo
    FROM control_acceso
    INNER JOIN plataforma ON control_acceso.id_plataforma = plataforma.id_plataforma
    INNER JOIN usuario ON control_acceso.id_usuario = usuario.id_usuario
    WHERE control_acceso.id_plataforma='2' AND usuario.nombre_usuario='$usuario_actual'");
$par_validUser = pg_affected_rows($validUser);

if ($par_validUser === 0) {
    $access = "denied";
} else {
    $access = "granted";
    $pfr_validUser = pg_fetch_row($validUser);
    $nombre_completo = $pfr_validUser[0];
}
?>

<script>
Ext.require(
    [
        //EXTJS 5.1
        'Ext.container.Viewport',
        'Ext.layout.container.Border',
        'Ext.tree.plugin.TreeViewDragDrop',
        'Ext.state.Manager',
        'Ext.state.CookieProvider',
        'Ext.window.MessageBox',
        'Ext.Component',
        'Ext.tab.Panel',
        'Ext.panel.Panel',
        'Ext.data.TreeStore',
        'Ext.dom.Query',
        'Ext.grid.Panel',
        'Ext.data.Store',
        //GEOEXT 2.1
        'GeoExt.panel.Map',
        'GeoExt.data.LayerTreeModel',
        'GeoExt.tree.BaseLayerContainer',
        'GeoExt.tree.OverlayLayerContainer',
        'GeoExt.data.LayerModel',
        'GeoExt.tree.Panel',
        'GeoExt.Action',
        'GeoExt.OverviewMap',
        'GeoExt.tree.View',
        'GeoExt.tree.Column',
        'GeoExt.slider.LayerOpacity',
        'GeoExt.slider.Tip',
        'GeoExt.window.Popup'
    ]
);


Ext.application({
    name: 'GeoAPC',
    launch: function () {

        //CAMBIAMOS EL IDIOMA DE OPENLAYERS
        OpenLayers.Lang.setCode("es");

        //VARIABLES DENTRO DEL AMBITO DE LA APLICACIÓN
        var toolbarItems = [],
            action, actions = {},
            bueno2, cuantos_temas_visibles = 1;

        //RUTA DE GEOSERVER
        var rutaGeoserver = "<?php echo $rutaGeoserver; ?>";


        //MAPA BASE ARCGIS REST (ESRI)
        var esri = new OpenLayers.Layer.XYZ("ESRI", "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/${z}/${y}/${x}", {
            isBaseLayer: true,
            sphericalMercator: true,
            displayInLayerSwitcher: false
        });
        //AREAS PRIVADAS DE CONSERVACIÓN
        var shape_puntos = new OpenLayers.Layer.WMS("shape_puntos", rutaGeoserver, {
            layers: 'shape_puntos_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        var shape_poligonos = new OpenLayers.Layer.WMS("shape_poligonos", rutaGeoserver, {
            layers: 'shape_poligonos_ccl_merge_final',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        var mergePuntos = new OpenLayers.Layer.WMS("mergePuntos", rutaGeoserver, {
            layers: 'mergePuntos_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        var mergePoligonos = new OpenLayers.Layer.WMS("mergePoligonos", rutaGeoserver, {
            layers: 'mergePoligonos_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //AREAS DESTINADAS VOLUNTARIAMENTE A LA CONSERVACIÓN (ADVC)
        var shape_advc = new OpenLayers.Layer.WMS("shape_advc", rutaGeoserver, {
            layers: 'shape_advc_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //CARTAS TOPOGRAFICAS (VARIABLES GLOBALES)
        shape_corrientes_agua = new OpenLayers.Layer.WMS("Corrientes de agua", rutaGeoserver, {
            layers: 'shape_corrientes_agua_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_cuerpos_agua = new OpenLayers.Layer.WMS("Cuerpos de agua", rutaGeoserver, {
            layers: 'shape_cuerpos_agua_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_localidades = new OpenLayers.Layer.WMS("Localidades", rutaGeoserver, {
            layers: 'shape_localidades_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_vialidades = new OpenLayers.Layer.WMS("Vialidades", rutaGeoserver, {
            layers: 'shape_vialidades_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_estados = new OpenLayers.Layer.WMS("Estados", rutaGeoserver, {
            layers: 'shape_estados_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_municipios = new OpenLayers.Layer.WMS("Municipios", rutaGeoserver, {
            layers: 'shape_municipios_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //INFORMACIÓN FISICA Y BIOLÓGICA
        shape_edafologia = new OpenLayers.Layer.WMS("Edafología", rutaGeoserver, {
            layers: 'shape_edafologia_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_geologia = new OpenLayers.Layer.WMS("Geología", rutaGeoserver, {
            layers: 'shape_geologia_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_climas = new OpenLayers.Layer.WMS("Climas", rutaGeoserver, {
            layers: 'shape_climas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_cuencas = new OpenLayers.Layer.WMS("Cuencas", rutaGeoserver, {
            layers: 'shape_cuencas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_precTotAnual = new OpenLayers.Layer.WMS("Precipitación total anual", rutaGeoserver, {
            layers: 'shape_precipitacion_total_anual_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_vegetacion = new OpenLayers.Layer.WMS("Vegetación serie V", rutaGeoserver, {
            layers: 'shape_vegetacion_serie_v_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_vegetacion_s6 = new OpenLayers.Layer.WMS("Vegetación serie VI", rutaGeoserver, {
            layers: 'shape_usv250s6_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });

        shape_zonas_funcionales = new OpenLayers.Layer.WMS("Zonas funcionales", rutaGeoserver, {
            layers: 'shape_zonas_funcionales_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //ÁREAS PROTEGIDAS
        shape_anpFederales = new OpenLayers.Layer.WMS("ANP Federales", rutaGeoserver, {
            layers: 'shape_anps_federales_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_anpEstatales = new OpenLayers.Layer.WMS("ANP Estatales", rutaGeoserver, {
            layers: 'shape_anps_estatales_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_anpMunicipales = new OpenLayers.Layer.WMS("ANP Municipales", rutaGeoserver, {
            layers: 'shape_anps_municipales_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_zonas_arqueologicas = new OpenLayers.Layer.WMS("Zonas arqueológicas", rutaGeoserver, {
            layers: 'shape_zonas_arqueologicas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //ORDENAMIENTOS ECOLOGICOS
        shape_oe_bobos = new OpenLayers.Layer.WMS("OE Bobos", rutaGeoserver, {
            layers: 'shape_oe_bobos_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_oe_coatzacoalcos = new OpenLayers.Layer.WMS("OE Coatzacoalcos", rutaGeoserver, {
            layers: 'shape_oe_coatzacoalcos_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_oe_tuxpan = new OpenLayers.Layer.WMS("OE Tuxpan", rutaGeoserver, {
            layers: 'shape_oe_tuxpan_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //SUBSISTEMA DE ALTA RESOLUCIÓN (SAR-MOD)
        shape_sarmod = new OpenLayers.Layer.WMS("Cobertura SAR-MOD", rutaGeoserver, {
            layers: 'shape_sarmod_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });

        //FIGURAS PRIORITARIAS
        shape_aicas = new OpenLayers.Layer.WMS("AICAS", rutaGeoserver, {
            layers: 'shape_aicas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_regionesTerrestres = new OpenLayers.Layer.WMS("Regiones terrestres prioritarias", rutaGeoserver, {
            layers: 'shape_regiones_terre_prioritarias_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_regionesHidrologicas = new OpenLayers.Layer.WMS("Regiones hidrologicas prioritarias", rutaGeoserver, {
            layers: 'shape_regiones_hidro_prioritarias_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_sitiosPrioritarios = new OpenLayers.Layer.WMS("Sitios prioritarios terrestres", rutaGeoserver, {
            layers: 'shape_sitios_prioritarios_terrestres_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_sitiosRamsar = new OpenLayers.Layer.WMS("Sitios Ramsar", rutaGeoserver, {
            layers: 'shape_ramsar_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //PAGO POR SERVICIOS AMBIENTALES
        shape_psa2005 = new OpenLayers.Layer.WMS("2005", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2005',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2006 = new OpenLayers.Layer.WMS("2006", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2006',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2007 = new OpenLayers.Layer.WMS("2007", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2007',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2008 = new OpenLayers.Layer.WMS("2008", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2008',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2009 = new OpenLayers.Layer.WMS("2009", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2009',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2010 = new OpenLayers.Layer.WMS("2010", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2010',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2011 = new OpenLayers.Layer.WMS("2011", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2011',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2012 = new OpenLayers.Layer.WMS("2012", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2012',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2013 = new OpenLayers.Layer.WMS("2013", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2013',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_psa2014 = new OpenLayers.Layer.WMS("2014", rutaGeoserver, {
            layers: 'shape_pago_servicios_ambientales_ccl_2014',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //INFORMACIÓN SOCIAL
        shape_zero_extinction = new OpenLayers.Layer.WMS("Cero extinción", rutaGeoserver, {
            layers: 'shape_zero_extinction_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_regiones_indigenas = new OpenLayers.Layer.WMS("Regiones indigenas", rutaGeoserver, {
            layers: 'shape_regiones_indigenas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //PAISAJES GEOECOLOGICOS
        shape_aproximacion1 = new OpenLayers.Layer.WMS("Aproximacion 1", rutaGeoserver, {
            layers: 'shape_aproximacion_1_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //ATRIBUTOS DEL DESARROLLO SUSTENTABLE
        shape_upgs = new OpenLayers.Layer.WMS("UPGs", rutaGeoserver, {
            layers: 'shape_upgs_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_umas = new OpenLayers.Layer.WMS("UMAS", rutaGeoserver, {
            layers: 'shape_umas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_sitios_ecoturisticos = "";
        //ZONAS DE RESTAURACIÓN
        shape_sitios_prior_restauracion2016_ccl = new OpenLayers.Layer.WMS("Sitios prioritarios de restauración", rutaGeoserver, {
            layers: 'shape_sitios_prior_restauracion2016_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_zonas_restauracion_lgeepa_ccl = new OpenLayers.Layer.WMS("Zonas de restauración LGEEPA", rutaGeoserver, {
            layers: 'shape_zonas_restauracion_lgeepa_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //RIESGOS Y PELIGROS
        shape_granizadas_ccl = new OpenLayers.Layer.WMS("Granizadas", rutaGeoserver, {
            layers: 'shape_granizadas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });


        //MODIFICAR
        shape_heladas_ccl = new OpenLayers.Layer.WMS("Índice de días con heladas por municipio", rutaGeoserver, {
            layers: 'shape_heladas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_nevadas_ccl = new OpenLayers.Layer.WMS("Índice de peligro por nevadas por municipio", rutaGeoserver, {
            layers: 'shape_nevadas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_temp_minima_extrema_ccl = new OpenLayers.Layer.WMS("Índice de temperatura mínima extrema por municipio", rutaGeoserver, {
            layers: 'shape_temp_minima_extrema_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_peligrobajastemperaturas_tempminheladas_ccl = new OpenLayers.Layer.WMS("Peligro por bajas temperaturas índices temperatura mínima heladas", rutaGeoserver, {
            layers: 'shape_peligrobajastemperaturas_tempminheladas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });


        shape_peligro_ciclonestropicales_ccl = new OpenLayers.Layer.WMS("Peligro por ciclones tropicales", rutaGeoserver, {
            layers: 'shape_peligro_ciclonestropicales_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_peligro_sequia_ccl = new OpenLayers.Layer.WMS("Peligro por sequía", rutaGeoserver, {
            layers: 'shape_peligro_sequia_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        }); //NEW
        shape_peligro_sequia_escalante2005_ccl = new OpenLayers.Layer.WMS("Peligro por sequía (Escalante Sandoval 2005)", rutaGeoserver, {
            layers: 'shape_peligro_sequia_escalante2005_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //shape_presencia_tornados_mun_mexico_ccl                 = new OpenLayers.Layer.WMS("Presencia de tornados en municipios de México",rutaGeoserver,{layers: 'shape_presencia_tornados_mun_mexico_ccl', format: 'image/png',transparent: true},{singleTile: true, visibility: false, isBaseLayer: false, displayInLayerSwitcher: true});
        shape_riesgo_por_bajastemperaturas_ccl = new OpenLayers.Layer.WMS("Riesgo por bajas temperaturas", rutaGeoserver, {
            layers: 'shape_riesgo_por_bajastemperaturas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_riesgo_por_granizo_ccl = new OpenLayers.Layer.WMS("Riesgo por granizo", rutaGeoserver, {
            layers: 'shape_riesgo_por_granizo_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_rieso_por_inundacion_ccl = new OpenLayers.Layer.WMS("Riesgo por inundación", rutaGeoserver, {
            layers: 'shape_rieso_por_inundacion_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_rieso_por_tormentaselectricas_ccl = new OpenLayers.Layer.WMS("Riesgo por tormentas electricas", rutaGeoserver, {
            layers: 'shape_rieso_por_tormentaselectricas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_sequias_historicas_ccl = new OpenLayers.Layer.WMS("Sequías historicas", rutaGeoserver, {
            layers: 'shape_sequias_historicas_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_zonas_susceptibles_hundimientosdeslizamientos_ccl = new OpenLayers.Layer.WMS("Zonas susceptibles a hundimientos y deslizamientos", rutaGeoserver, {
            layers: 'shape_zonas_susceptibles_hundimientosdeslizamientos_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });

        //INFORMACIÓN PRONATURA
        shape_areaTrabajo = new OpenLayers.Layer.WMS("Nueva area de trabajo", rutaGeoserver, {
            layers: 'shape_nueva_area_trabajopronatura_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        shape_viveros = new OpenLayers.Layer.WMS("Red de viveros de biodiversidad", rutaGeoserver, {
            layers: 'shape_viveros_biodiversidad_ccl',
            format: 'image/png',
            transparent: true
        }, {
            singleTile: true,
            visibility: false,
            isBaseLayer: false,
            displayInLayerSwitcher: true
        });
        //VECTORES
        /*REGLAS PARA EL CLUSTER DE PUNTOS*/
        var colors = {
            low: "rgb(181, 226, 140)",
            middle: "rgb(241, 211, 87)",
            high: "rgb(253, 156, 115)"
        };

        var lowRule = new OpenLayers.Rule({
            filter: new OpenLayers.Filter.Comparison({
                type: OpenLayers.Filter.Comparison.LESS_THAN,
                property: "count",
                value: 15
            }),
            symbolizer: {
                fillColor: colors.low,
                fillOpacity: 0.9,
                strokeColor: colors.low,
                strokeOpacity: 0.5,
                strokeWidth: 12,
                pointRadius: 10,
                label: "${count}",
                labelOutlineWidth: 1,
                fontColor: "#ffffff",
                fontOpacity: 0.8,
                fontSize: "12px"
            }
        });

        var middleRule = new OpenLayers.Rule({
            filter: new OpenLayers.Filter.Comparison({
                type: OpenLayers.Filter.Comparison.BETWEEN,
                property: "count",
                lowerBoundary: 16,
                upperBoundary: 50
            }),
            symbolizer: {
                fillColor: colors.middle,
                fillOpacity: 0.9,
                strokeColor: colors.middle,
                strokeOpacity: 0.5,
                strokeWidth: 12,
                pointRadius: 15,
                label: "${count}",
                labelOutlineWidth: 1,
                fontColor: "#ffffff",
                fontOpacity: 0.8,
                fontSize: "12px"
            }
        });

        var highRule = new OpenLayers.Rule({
            filter: new OpenLayers.Filter.Comparison({
                type: OpenLayers.Filter.Comparison.GREATER_THAN,
                property: "count",
                value: 50
            }),
            symbolizer: {
                fillColor: colors.high,
                fillOpacity: 0.9,
                strokeColor: colors.high,
                strokeOpacity: 0.5,
                strokeWidth: 12,
                pointRadius: 20,
                label: "${count}",
                labelOutlineWidth: 1,
                fontColor: "#ffffff",
                fontOpacity: 0.8,
                fontSize: "12px"
            }
        });

        var style = new OpenLayers.Style(null, {
            rules: [lowRule, middleRule, highRule]
        });

        //CREAMOS EL VECTOR DE PUNTOS
        var vectorPuntos = new OpenLayers.Layer.Vector("vectorPuntos", {
            displayInLayerSwitcher: false,
            protocol: new OpenLayers.Protocol.HTTP({
                url: rutaGeoserver + "pronatura/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pronatura:mergePuntos_ccl&maxFeatures=1000&outputFormat=json&srsName=epsg:3857",
                format: new OpenLayers.Format.GeoJSON()
            }),
            renderers: ['Canvas', 'SVG'],
            strategies: [
                new OpenLayers.Strategy.Fixed(),
                new OpenLayers.Strategy.AnimatedCluster({
                    distance: 45,
                    animationMethod: OpenLayers.Easing.Expo.easeOut,
                    animationDuration: 105

                })
            ],
            styleMap: new OpenLayers.StyleMap(style)
        });

        var estiloPoligonos = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style({
                cursor: "cursor",
                strokeColor: "#d19d16",
                fillColor: "#d19d16",
                fillOpacity: 0.5,
                strokeOpacity: 1,
                strokeWidth: 2
            }),
            "select": new OpenLayers.Style({
                cursor: "cursor",
                strokeColor: "#ff8a2b",
                fillColor: "#ff8a2b",
                fillOpacity: 0.5,
                strokeOpacity: 1,
                strokeWidth: 2
            }),
            "temporary": new OpenLayers.Style({
                cursor: "cursor",
                strokeColor: "#ff8a2b",
                fillColor: "#ff8a2b",
                fillOpacity: 0.5,
                strokeOpacity: 1,
                strokeWidth: 2
            })
        });

        var refreshLayer = new OpenLayers.Strategy.Refresh({
            force: true,
            active: true
        });
        var vectorProtocol = new OpenLayers.Protocol.HTTP({
            url: rutaGeoserver + 'pronatura/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pronatura:mergePoligonos_ccl&maxFeatures=1000&outputFormat=json&srsName=epsg:3857',
            format: new OpenLayers.Format.GeoJSON()
        });

        var vectorStrategies = [new OpenLayers.Strategy.Fixed(), refreshLayer];
        var vectorPoligonos = new OpenLayers.Layer.Vector("Vectores_poligonos", {
            visibility: false,
            displayInLayerSwitcher: false,
            styleMap: estiloPoligonos,
            strategies: [new OpenLayers.Strategy.Fixed()],
            projection: new OpenLayers.Projection("EPSG:3857"),
            protocol: vectorProtocol,
            strategies: vectorStrategies
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////



        //CREAMOS EL VECTOR DE POLIGONOS DE ADVC////////////////////////////////////////////////////////////////////
        var estiloPoligonosADVC = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style({
                cursor: "cursor",
                strokeColor: "#c95151",
                fillColor: "#cc6666",
                fillOpacity: 0.5,
                strokeOpacity: 1,
                strokeWidth: 2
            }),
            "select": new OpenLayers.Style({
                cursor: "cursor",
                strokeColor: "#ff8a2b",
                fillColor: "#ff8a2b",
                fillOpacity: 0.5,
                strokeOpacity: 1,
                strokeWidth: 2
            }),
            "temporary": new OpenLayers.Style({
                cursor: "cursor",
                strokeColor: "#ff8a2b",
                fillColor: "#ff8a2b",
                fillOpacity: 0.5,
                strokeOpacity: 1,
                strokeWidth: 2
            })
        });
        var refreshLayerADVC = new OpenLayers.Strategy.Refresh({
            force: true,
            active: true
        });
        var vectorProtocolADVC = new OpenLayers.Protocol.HTTP({
            url: rutaGeoserver + 'pronatura/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pronatura:shape_advc_ccl&maxFeatures=1000&outputFormat=json&srsName=epsg:3857',
            format: new OpenLayers.Format.GeoJSON()
        });
        var vectorStrategiesADVC = [new OpenLayers.Strategy.Fixed(), refreshLayerADVC];
        var vectorPoligonosADVC = new OpenLayers.Layer.Vector("ADVC", {
            visibility: false,
            displayInLayerSwitcher: false,
            styleMap: estiloPoligonosADVC,
            strategies: [new OpenLayers.Strategy.Fixed()],
            projection: new OpenLayers.Projection("EPSG:3857"),
            protocol: vectorProtocolADVC,
            strategies: vectorStrategiesADVC
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////





        //FUNCION AL SELECCIONAR UN PUNTO (CLUSTER)
        function onPointSelect(point) {
            var folio = "";
            //LIMPIAMOS LA TABLA DEL PANEL DERECHO (PARA SOLO MOSTRAR INFORMACION RECIEN CLICKEADA)
            Ext.getCmp('panelAtributos').removeAll();
            Ext.getCmp('panelAtributos').doLayout();

            if (!point.cluster) //SIN CLUSTER
            {
                nombre_apc = point.attributes['nombre_apc_principal'];
                folio = point.attributes['folio_principal'];
                hectareas = point.attributes['hectareas_principal'];
                estado = point.attributes['estado_principal'];
                municipio = point.attributes['municipio_principal'];
                vigencia = point.attributes['vigencia_principal'];
                fecha_certificacion = point.attributes['fecha_certificacion_principal'];
                numero_sedema = point.attributes['numero_sedema_principal'];
                cuenta_plan_manejo = point.attributes['cuenta_plan_manejo_principal'];

                pasarAlGrid(nombre_apc, folio, hectareas, estado, municipio, vigencia, fecha_certificacion, numero_sedema, cuenta_plan_manejo);
            } else //CONTIENE CLUSTER
            {
                desc = "<div style='margin-left:15px; margin-right:15px;'><b style='font-family:Roboto; color:#666666; font-size:15px;'>" + "Áreas Privadas de Conservación (" + point.cluster.length + ")</b><hr><ol class='rectangle-list-gray'>";
                cmax = point.cluster.length - 1;
                for (c = cmax; c >= 0; c--) {
                    nombre_apc = point.cluster[c].attributes.nombre_apc_principal;
                    folio = point.cluster[c].attributes.folio_principal;
                    hectareas = point.cluster[c].attributes.hectareas_principal;
                    estado = point.cluster[c].attributes.estado_principal;
                    municipio = point.cluster[c].attributes.municipio_principal;
                    vigencia = point.cluster[c].attributes.vigencia_principal;
                    fecha_certificacion = point.cluster[c].attributes.fecha_certificacion_principal;
                    numero_sedema = point.cluster[c].attributes.numero_sedema_principal;
                    cuenta_plan_manejo = point.cluster[c].attributes.cuenta_plan_manejo_principal;

                    funcion_completa = 'pasarAlGrid("' + nombre_apc + '","' + folio + '","' + hectareas + '","' + estado + '","' + municipio + '","' + vigencia + '","' + fecha_certificacion + '","' + numero_sedema + '","' + cuenta_plan_manejo + '")';
                    desc += "<li><a href='javascript:void(0)' onclick='" + funcion_completa + "'>" + point.cluster[c].attributes.nombre_apc_principal + '</a></li>';
                }
                desc += '</ol></div>';
                cerrarPopup(false);

                if (point.popup == null) {
                    point.popup = new OpenLayers.Popup.FramedCloud(
                        'popup',
                        new OpenLayers.LonLat(point.geometry.x, point.geometry.y),
                        null,
                        desc,
                        new OpenLayers.Icon(
                            '',
                            new OpenLayers.Size(0, 0),
                            new OpenLayers.Pixel(0, 0)
                        ),
                        true, //tiene boton de cerrar (true o false)
                        cerrarPopup(true) //accion al cerrar
                    );

                    point.popup.minSize = new OpenLayers.Size(200, 40);
                    point.popup.maxSize = new OpenLayers.Size(350, 300);
                    point.popup.autoSize = true;
                    point.popup.offset = 7;

                    point.popup.calculateNewPx = function (px) {
                        if (point.popup.size !== null) {
                            px = px.add(point.popup.size.w / 2 * -1 + point.popup.offset, point.popup.size.h * -1 + point.popup.offset);
                            return px;
                        }
                    };
                    map.addPopup(point.popup);
                    point.popup.show();
                } else {
                    point.popup.setContentHTML(desc);
                    point.popup.toggle();
                }

                if (point.popup.visible()) {
                    currentPopup = point.popup;
                } else {
                    closePane();
                }
            }
        }

        //ADDED FOR ADVCs
        function onADVCSelect(selected) {
            //LIMPIAMOS LA TABLA DEL PANEL DERECHO (PARA SOLO MOSTRAR INFORMACION RECIEN CLICKEADA)
            Ext.getCmp('panelAtributos').removeAll();
            Ext.getCmp('panelAtributos').doLayout();

            var tipo = selected.attributes['tipo'];
            var nom_predio = selected.attributes['nom_predio'];
            var instrume = selected.attributes['instrume'];
            var fecha = selected.attributes['fecha'];
            var vigencia = selected.attributes['vigencia'];
            var estado = selected.attributes['estado'];
            var municipio = selected.attributes['municipio'];
            var fuente = selected.attributes['fuente'];
            var sup_instr = selected.attributes['sup_instr'];
            var ecosistema = selected.attributes['ecosistema'];
            var flora_059 = selected.attributes['flora_059'];
            var fauna_059 = selected.attributes['fauna_059'];

            var items = [];
            items.push({
                xtype: "propertygrid",
                header: true,
                hideHeaders: true,
                sortableColumns: false,
                listeners: {
                    'beforeedit': {
                        fn: function () {
                            return false;
                        }
                    }
                },
                title: 'Información del ADVC',
                source: {
                    "Tipo": tipo,
                    "Nombre del predio": nom_predio,
                    "Instrumento": instrume,
                    "Fecha": fecha,
                    "Vigencia": vigencia,
                    "Estado": estado,
                    "Municipio": municipio,
                    "Fuente": fuente,
                    "Superficie": sup_instr,
                    "Ecosistema": ecosistema,
                    "Flora 059": flora_059,
                    "Fauna 059": fauna_059,
                }
            });
            var panelADVC = Ext.create('Ext.panel.Panel', {
                header: false,
                width: 320,
                border: false,
                flex: 1,
                items: items
            });

            //DESTRUIR EL panelConcentrador (POR SI YA EXISTIA)
            if (Ext.getCmp('panelConcentrador'))
                Ext.getCmp('panelConcentrador').destroy();

            var panelConcentrador = Ext.create('Ext.panel.Panel', {
                id: 'panelConcentrador',
                layout: {
                    type: 'vbox',
                    align: 'left'
                },
                resizable: false,
                shadow: true,
                border: false,
                items: [
                    panelADVC
                ]
            });

            var panelAtributos = Ext.getCmp('panelAtributos');
            panelAtributos.removeAll();
            panelAtributos.add(panelConcentrador);
            panelAtributos.doLayout();
            var panelDerecho = Ext.getCmp('panelDerecho');
            panelDerecho.setActiveTab(panelAtributos);

            /////////////////////////////////////////////////////


        }

        function onUnSelectFeature() {
            var panelAtributos = Ext.getCmp('panelAtributos');
            panelAtributos.removeAll();
            panelAtributos.doLayout();
        }


        //FUNCIÓN QUE REGRESA LA LATITUD Y LONGITUD AL PASAR EL MOUSE
        function mousePosFmtOut(lonLat) {
            var coordenadas = this.prefix + '<b>Longitud:</b> ' + lonLat.lon.toFixed(this.numDigits) + '&nbsp;&nbsp;<b style="color:crimson;">|</b>&nbsp;&nbsp;<b>Latitud:</b> ' + lonLat.lat.toFixed(this.numDigits) + this.suffix;
            var barraCoordenadas = Ext.getCmp('barraCoordenadas');
            barraCoordenadas.setRawValue(coordenadas);
            x = '';
            return x;
        }

        //CREAMOS EL MAPA
        var map = new OpenLayers.Map({
            fallThrough: true,
            projection: 'EPSG:3857',
            units: "km",
            controls: [
                new OpenLayers.Control.Navigation({
                    dragPanOptions: {
                        enableKinetic: true
                    }
                }),
                new OpenLayers.Control.MousePosition({
                    prefix: '<div style=" font-size: 12px; color:dimgray; font-family:Roboto;">',
                    suffix: '</div>',
                    separator: ' | ',
                    numDigits: 4,
                    emptyString: '',
                    formatOutput: mousePosFmtOut,
                    displayProjection: "EPSG:4326"
                }),
                //new OpenLayers.Control.Scale(OpenLayers.Util.getElement('scale')),
                new OpenLayers.Control.Scale(),
                new OpenLayers.Control.ScaleBar({
                    showMinorMeasures: true,
                    abbreviateLabel: true,
                    scaleText: 'Escala 1:',
                    align: "left",
                    singleLine: false,
                    minWidth: 110,
                    maxWidth: 150
                })
            ],
            layers: [
                esri
            ],
            center: new OpenLayers.LonLat(-96.165, 19.385).transform('EPSG:4326', 'EPSG:3857'),
            zoom: 9
        });

        map.isValidZoomLevel = function (zoomLevel) {
            return ((zoomLevel != null) && (zoomLevel >= 5) && (zoomLevel <= 17));
        }

        //AGREGAMOS LAS CAPAS EN EL ORDEN QUE QUEREMOS SEAN SOBREPUESTAS
        map.addLayer(shape_vegetacion_s6);
        map.addLayer(shape_peligro_sequia_escalante2005_ccl);
        map.addLayer(shape_riesgo_por_bajastemperaturas_ccl);
        map.addLayer(shape_riesgo_por_granizo_ccl);
        map.addLayer(shape_rieso_por_inundacion_ccl);
        map.addLayer(shape_rieso_por_tormentaselectricas_ccl);
        map.addLayer(shape_sequias_historicas_ccl);
        map.addLayer(shape_zonas_susceptibles_hundimientosdeslizamientos_ccl);
        map.addLayer(shape_peligro_sequia_ccl);
        map.addLayer(shape_peligro_ciclonestropicales_ccl);
        map.addLayer(shape_peligrobajastemperaturas_tempminheladas_ccl);
        map.addLayer(shape_temp_minima_extrema_ccl);
        map.addLayer(shape_nevadas_ccl);
        map.addLayer(shape_heladas_ccl);
        map.addLayer(shape_granizadas_ccl);
        map.addLayer(shape_sitios_prior_restauracion2016_ccl);
        map.addLayer(shape_zonas_restauracion_lgeepa_ccl);
        map.addLayer(shape_sarmod);
        map.addLayer(shape_oe_bobos);
        map.addLayer(shape_oe_coatzacoalcos);
        map.addLayer(shape_oe_tuxpan);
        map.addLayer(shape_aicas);
        map.addLayer(shape_climas);
        map.addLayer(shape_cuencas);
        map.addLayer(shape_zonas_funcionales);
        map.addLayer(shape_umas);
        map.addLayer(shape_psa2005);
        map.addLayer(shape_psa2006);
        map.addLayer(shape_psa2007);
        map.addLayer(shape_psa2008);
        map.addLayer(shape_psa2009);
        map.addLayer(shape_psa2010);
        map.addLayer(shape_psa2011);
        map.addLayer(shape_psa2012);
        map.addLayer(shape_psa2013);
        map.addLayer(shape_psa2014);
        map.addLayer(shape_vegetacion);
        map.addLayer(shape_zonas_arqueologicas);
        map.addLayer(shape_zero_extinction);
        map.addLayer(shape_viveros);
        map.addLayer(shape_regiones_indigenas);
        map.addLayer(shape_upgs);
        map.addLayer(shape_edafologia);
        map.addLayer(shape_precTotAnual);
        map.addLayer(shape_geologia);
        map.addLayer(shape_anpFederales);
        map.addLayer(shape_anpEstatales);
        map.addLayer(shape_anpMunicipales);
        map.addLayer(shape_regionesTerrestres);
        map.addLayer(shape_regionesHidrologicas);
        map.addLayer(shape_sitiosPrioritarios);
        map.addLayer(shape_sitiosRamsar);
        map.addLayer(shape_areaTrabajo);
        map.addLayer(shape_vialidades);
        map.addLayer(shape_cuerpos_agua);
        map.addLayer(shape_corrientes_agua);
        map.addLayer(shape_localidades);
        map.addLayer(shape_municipios);
        map.addLayer(shape_estados);
        map.addLayer(vectorPoligonosADVC);
        map.addLayer(vectorPuntos);
        map.addLayer(vectorPoligonos);

        //VALOR MAS ALTO ES EL DE HASTA ARRIBA
        vectorPoligonos.setZIndex(500);
        vectorPuntos.setZIndex(499);
        vectorPoligonosADVC.setZIndex(498);
        shape_estados.setZIndex(497);
        shape_municipios.setZIndex(496);
        shape_localidades.setZIndex(495);
        shape_corrientes_agua.setZIndex(494);
        shape_cuerpos_agua.setZIndex(493);
        shape_vialidades.setZIndex(492);

        vectorPuntos.setVisibility(true);
        vectorPoligonos.setVisibility(true);
        vectorPoligonosADVC.setVisibility(true);


        //CREAMOS EL GETFEATUREINFO
        featureInfo = new OpenLayers.Control.WMSGetFeatureInfo({
            url: rutaGeoserver + 'pronatura/wms',
            queryVisible: true,
            infoFormat: "application/vnd.ogc.gml",
            maxFeatures: 100,
            layers: [shape_anpFederales],
            eventListeners: {
                "getfeatureinfo": function (e) {

                    var mostrarActasCabildo = 0;
                    var cualANPEstatal = "";

                    var items = [];
                    Ext.each(e.features, function (feature) {
                        //console.log('GML Feature: ' + feature.gml.featureType);

                        function toTitleCase(str) {
                            var cadena = str.toLowerCase();
                            var cadena_formateada = "";
                            arreglo_cadena = cadena.split(" ");
                            for (i = 0; i < arreglo_cadena.length; i++) {
                                if (i != 0) {
                                    if (
                                        arreglo_cadena[i] != "de" &&
                                        arreglo_cadena[i] != "del" &&
                                        arreglo_cadena[i] != "y" &&
                                        arreglo_cadena[i] != "o" &&
                                        arreglo_cadena[i] != "el" &&
                                        arreglo_cadena[i] != "los" &&
                                        arreglo_cadena[i] != "las" &&
                                        arreglo_cadena[i] != "la" &&
                                        arreglo_cadena[i] != "en" &&
                                        arreglo_cadena[i] != "que"
                                    ) {
                                        cadena_formateada = cadena_formateada + arreglo_cadena[i].charAt(0).toUpperCase() + arreglo_cadena[i].substr(1) + " ";
                                    } else {
                                        cadena_formateada = cadena_formateada + arreglo_cadena[i] + " ";
                                    }
                                } else if (i == 0) {
                                    cadena_formateada = cadena_formateada + arreglo_cadena[i].charAt(0).toUpperCase() + arreglo_cadena[i].substr(1) + " ";
                                }
                            }
                            return cadena_formateada;
                        }


                        //FUNCIÓN PARA REEMPLAZAR TODAS LAS OCURRENCIAS
                        function replaceAll(str, find, replace) {
                            return str.replace(new RegExp(find, 'g'), replace);
                        }

                        //FUNCIÓN PARA DAR FORMATO A LAS FECHAS
                        function formatDate(fecha) {
                            var fecha_separada = fecha.split("-");
                            var anyo = fecha_separada[0];
                            var mes = fecha_separada[1];
                            var dia = fecha_separada[2];
                            switch (mes) {
                                case '01':
                                    mes = 'Enero';
                                    break;
                                case '02':
                                    mes = 'Febrero';
                                    break;
                                case '03':
                                    mes = 'Marzo';
                                    break;
                                case '04':
                                    mes = 'Abril';
                                    break;
                                case '05':
                                    mes = 'Mayo';
                                    break;
                                case '06':
                                    mes = "Junio";
                                    break;
                                case '07':
                                    mes = "Julio";
                                    break;
                                case '08':
                                    mes = "Agosto";
                                    break;
                                case '09':
                                    mes = "Septiembre";
                                    break;
                                case '10':
                                    mes = "Octubre";
                                    break;
                                case '11':
                                    mes = "Noviembre";
                                    break;
                                case '12':
                                    mes = "Diciembre";
                                    break;
                                default:
                                    break;
                            }
                            mes = mes.toLowerCase();
                            return dia + " de " + mes + " de " + anyo;
                        }

                        //DEFINIMOS EL TITULO DEL GRID Y SUS ELEMENTOS PARA CADA UNA DE LAS CAPAS
                        var titulo = "";
                        var arreglo = new Array();
                        var objetos = [];

                        //CORRIENTES DE AGUA
                        if (feature.gml.featureType == 'shape_corrientes_agua_ccl') {
                            titulo = "Corrientes de agua";
                            arreglo = new Array();
                            objetos = {
                                'Tipo': toTitleCase(feature.attributes.tipo),
                                'Longitud [km]': parseFloat(feature.attributes.length_km).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //CUERPOS DE AGUA
                        if (feature.gml.featureType == 'shape_cuerpos_agua_ccl') {
                            titulo = "Cuerpos de agua";
                            arreglo = new Array();
                            //DAMOS FORMATO A ENTIDAD PARA ELIMINAR EL GUION BAJO
                            var entidad = toTitleCase(feature.attributes.entidad);
                            entidad = entidad.replace("Cuerpo_agua", "Cuerpo de agua");
                            objetos = {
                                'Tipo': toTitleCase(feature.attributes.tipo),
                                'Entidad': entidad,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //LOCALIDADES
                        if (feature.gml.featureType == 'shape_localidades_ccl') {
                            titulo = "Localidades";
                            arreglo = new Array();
                            objetos = {
                                'Localidad': toTitleCase(feature.attributes.nom_loc),
                                'Municipio': toTitleCase(feature.attributes.nom_mun),
                                'Estado': toTitleCase(feature.attributes.nom_ent)
                            };
                            arreglo.push(objetos);
                        }
                        //VIALIDADES
                        if (feature.gml.featureType == 'shape_vialidades_ccl') {
                            titulo = "Vialidades";
                            arreglo = new Array();
                            objetos = {
                                'Tipo': feature.attributes.tipo,
                                'Entidad': toTitleCase(feature.attributes.entidad),
                                'Número de vías': feature.attributes.num_vias,
                                'Derecho': feature.attributes.derecho,
                                'Jurisdicción': feature.attributes.jurisdic,
                                'Condición': feature.attributes.condicion,
                                'Longitud [km]': parseFloat(feature.attributes.length_km).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //ESTADOS
                        if (feature.gml.featureType == 'shape_estados_ccl') {
                            titulo = "Estados";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.nom_ent),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //MUNICIPIOS
                        if (feature.gml.featureType == 'shape_municipios_ccl') {
                            titulo = "Municipios";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.nom_mun),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //VEGETACIÓN
                        if (feature.gml.featureType == 'shape_vegetacion_serie_v_ccl') {
                            titulo = "Uso de suelo y vegetación V";
                            arreglo = new Array();
                            objetos = {
                                'Tipo de vegetación': toTitleCase(feature.attributes.descripcio),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //VEGETACION SERIE 6 (shape_vegetacion_s6 || shape_usv250s6_ccl)
                        if (feature.gml.featureType == 'shape_usv250s6_ccl') {
                            titulo = "Uso de suelo y vegetación VI";
                            arreglo = new Array();
                            objetos = {
                                'Tipo de vegetación': toTitleCase(feature.attributes.descripcio),
                                'Área': parseFloat(feature.attributes.area).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //ZONAS ARQUEOLÓGICAS
                        if (feature.gml.featureType == 'shape_zonas_arqueologicas_ccl') {
                            titulo = "Zonas arqueológicas";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.zonaarqueo),
                                'Estado': toTitleCase(feature.attributes.estado),
                                'Municipio': toTitleCase(feature.attributes.municipio),
                                'Culturas': toTitleCase(feature.attributes.culturas)
                            };
                            arreglo.push(objetos);
                        }
                        //ZERO EXTINCTION
                        if (feature.gml.featureType == 'shape_zero_extinction_ccl') {
                            titulo = "Especies en sitios cero extinción";
                            var riesgo = feature.attributes.en_riesgo;
                            if (!riesgo)
                                riesgo = "No especificado";
                            else
                                toTitleCase(riesgo);

                            arreglo = new Array();
                            objetos = {
                                'Especie': toTitleCase(feature.attributes.especie),
                                'Riesgo': riesgo,
                                'Estado': toTitleCase(feature.attributes.estado),
                                'Municipio': toTitleCase(feature.attributes.municipio),
                                'Localidad': toTitleCase(feature.attributes.localidad),
                                'Área protegida': toTitleCase(feature.attributes.area_prote)
                            };
                            arreglo.push(objetos);
                        }
                        //VIVEROS
                        if (feature.gml.featureType == 'shape_viveros_biodiversidad_ccl') {
                            titulo = "Red de viveros de biodiversidad";
                            arreglo = new Array();
                            objetos = {
                                'Vivero': toTitleCase(feature.attributes.vivero),
                                'Tipo': toTitleCase(feature.attributes.tipo),
                                'Estado': toTitleCase(feature.attributes.estado),
                                'Municipio': toTitleCase(feature.attributes.municipio),
                                'Localidad': toTitleCase(feature.attributes.localidad)
                            };
                            arreglo.push(objetos);
                        }
                        //APROXIMACIÓN 1
                        if (feature.gml.featureType == 'shape_aproximacion_1_ccl') {
                            titulo = "Aproximación 1";
                            arreglo = new Array();

                            var clase = feature.attributes.clase;
                            var subclase = feature.attributes.subclase;
                            var grupo = feature.attributes.grupo;
                            var subgrupo = feature.attributes.subgrupo;
                            var paisaje_p1 = feature.attributes.paisaje_p1;
                            var paisaje_p2 = feature.attributes.paisaje_p2;

                            if (!clase) clase = "No especificada";
                            else clase = toTitleCase(clase);
                            if (!subclase) subclase = "No especificada";
                            else subclase = toTitleCase(subclase);
                            if (!grupo) grupo = "No especificado";
                            else grupo = toTitleCase(grupo);
                            if (!subgrupo) subgrupo = "No especificado";
                            else subgrupo = toTitleCase(subgrupo);
                            if (!paisaje_p1) paisaje_p1 = "No especificado";
                            else paisaje_p1 = toTitleCase(paisaje_p1);
                            if (!paisaje_p2) paisaje_p2 = "No especificado";
                            else paisaje_p2 = toTitleCase(paisaje_p2);

                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.eco_name),
                                'Clase': clase,
                                'Subclase': subclase,
                                'Grupo': grupo,
                                'Subgrupo': subgrupo,
                                'Paisaje 1': paisaje_p1,
                                'Paisaje 2': paisaje_p2
                            };
                            arreglo.push(objetos);
                        }
                        //REGIONES INDIGENAS
                        if (feature.gml.featureType == 'shape_regiones_indigenas_ccl') {
                            titulo = "Regiones indigenas";
                            arreglo = new Array();
                            objetos = {
                                'Lengua': toTitleCase(feature.attributes.lengua),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //UPGS
                        if (feature.gml.featureType == 'shape_upgs_ccl') {
                            titulo = "Unidades Productoras de Germoplasma";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.nombre),
                                'Especie': toTitleCase(feature.attributes.especie),
                                'Gerencia regional': toTitleCase(feature.attributes.gerencia_r),
                                'Estado': toTitleCase(feature.attributes.estado),
                                'Municipio': toTitleCase(feature.attributes.municipio),
                                'Dependencia': toTitleCase(feature.attributes.dependenci),
                                'Propietario': toTitleCase(feature.attributes.propietari),
                                'Densidad': toTitleCase(feature.attributes.densidad),
                                'Rendimiento': toTitleCase(feature.attributes.rendimient),
                                'Fecha de recolección': feature.attributes.fecha_reco
                            };
                            arreglo.push(objetos);
                        }
                        //EDAFOLOGÍA
                        if (feature.gml.featureType == 'shape_edafologia_ccl') {
                            titulo = "Edafología";
                            arreglo = new Array();
                            objetos = {
                                'Tipo de suelo': toTitleCase(feature.attributes.descripcio),
                                'Tipo de textura': toTitleCase(feature.attributes.desc_tex),
                                'Fase física': toTitleCase(feature.attributes.desc_fasfi),
                                'Fase química': toTitleCase(feature.attributes.desc_faqui),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //GEOLOGÍA
                        else if (feature.gml.featureType == 'shape_geologia_ccl') {
                            titulo = "Geología";
                            arreglo = new Array();
                            objetos = {
                                'Tipo': toTitleCase(feature.attributes.tipo_ley),
                                'Clave': feature.attributes.clave,
                                'Entidad': toTitleCase(feature.attributes.entidad_le),
                                'Era': toTitleCase(feature.attributes.era_ley),
                                'Período': toTitleCase(feature.attributes.periodo_le),
                                'Época': toTitleCase(feature.attributes.epoca_ley),
                                'Origen': toTitleCase(feature.attributes.origen_ley),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //CLIMAS
                        else if (feature.gml.featureType == 'shape_climas_ccl') {
                            titulo = "Climas";
                            arreglo = new Array();
                            var temperatura_climas = replaceAll(feature.attributes.des_tem, "§", "°");
                            var precipitacion_climas = replaceAll(feature.attributes.desc_prec, "¤", "ñ");

                            //ARREGLAR ORTOGRAFÍA
                            temperatura_climas = replaceAll(temperatura_climas, "Arido", "Árido");
                            temperatura_climas = replaceAll(temperatura_climas, "arido", "árido");
                            temperatura_climas = replaceAll(temperatura_climas, "Calido", "Cálido");
                            temperatura_climas = replaceAll(temperatura_climas, "calido", "cálido");
                            temperatura_climas = replaceAll(temperatura_climas, "Semiarido", "Semiárido");
                            temperatura_climas = replaceAll(temperatura_climas, "semiarido", "semiárido");
                            temperatura_climas = replaceAll(temperatura_climas, "Semicalido", "Semicálido");
                            temperatura_climas = replaceAll(temperatura_climas, "semicalido", "semicálido");
                            temperatura_climas = replaceAll(temperatura_climas, "mas", "más");
                            temperatura_climas = replaceAll(temperatura_climas, "Semifrio", "Semifrío");
                            temperatura_climas = replaceAll(temperatura_climas, "semifrio", "semifrío");
                            temperatura_climas = replaceAll(temperatura_climas, "Subhumedo", "Subhúmedo");
                            temperatura_climas = replaceAll(temperatura_climas, "subhumedo", "subhúmedo");
                            temperatura_climas = replaceAll(temperatura_climas, "humedo", "húmedo");
                            temperatura_climas = replaceAll(temperatura_climas, "Humedo", "Húmedo");
                            precipitacion_climas = replaceAll(precipitacion_climas, "Precipitacion", "Precipitación");
                            precipitacion_climas = replaceAll(precipitacion_climas, "precipitacion", "precipitación");
                            precipitacion_climas = replaceAll(precipitacion_climas, "Indice", "Índice");
                            precipitacion_climas = replaceAll(precipitacion_climas, "indice", "índice");

                            objetos = {
                                'Tipo': feature.attributes.clima_tipo,
                                'Temperatura': temperatura_climas,
                                'Precipitación': precipitacion_climas,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //CUENCAS HIDROGRAFICAS
                        else if (feature.gml.featureType == 'shape_cuencas_ccl') {
                            titulo = "Cuencas hidrográficas";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': feature.attributes.toponimo,
                                'Estado': feature.attributes.edo_cuen,
                                'Tipo de cuenca': feature.attributes.tipo_cuen,
                                'Altura máxima [m]': feature.attributes.alt_max,
                                'Altura mínima [m]': feature.attributes.alt_min,
                                'Desnivel': feature.attributes.dif_alt,
                                'Región Hidrológica': feature.attributes.reg_hid,
                                'Subregión Hidrológica': feature.attributes.sub_hid,
                                'Tipo de patrón de drenaje': feature.attributes.tipo_dren,
                                'Código': feature.attributes.codigo,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }

                        //ZONAS FUNCIONALES
                        else if (feature.gml.featureType == 'shape_zonas_funcionales_ccl') {
                            titulo = "Zonas funcionales";
                            arreglo = new Array();
                            objetos = {
                                'Zona': feature.attributes.zonalt_ley,
                                'Nombre de la cuenca': feature.attributes.cuenca_nom,
                                'Región hidrológica': feature.attributes.reg_hidro,
                                'Subregión hidrológica': feature.attributes.subreg_hid,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //ORDENAMIENTOS ECOLOGICOS
                        else if (feature.gml.featureType == 'shape_oe_bobos_ccl') {
                            titulo = "OE Bobos";
                            arreglo = new Array();
                            objetos = {
                                'UGA': feature.attributes.uga,
                                'Politicas': feature.attributes.politicas_
                            };
                            arreglo.push(objetos);
                        }
                        //SARMOD
                        else if (feature.gml.featureType == 'shape_sarmod_ccl') {
                            titulo = "Cobertura SAR-MOD";
                            arreglo = new Array();
                            objetos = {
                                'Nombre del ANP': feature.attributes.nombre,
                                'Categoría decreto DOF': feature.attributes.cat_decret,
                                'Categoría de manejo de las ANP': feature.attributes.cat_manejo,
                                'Regionalización': feature.attributes.region,
                                'Ultima publicación DOF': feature.attributes.ult_dof
                            };
                            arreglo.push(objetos);
                        } else if (feature.gml.featureType == 'shape_oe_coatzacoalcos_ccl') {
                            titulo = "OE Coatzacoalcos";
                            arreglo = new Array();
                            objetos = {
                                'Uso politico': feature.attributes.uso_politi,
                                'Usos comunes': feature.attributes.usos_com
                            };
                            arreglo.push(objetos);
                        } else if (feature.gml.featureType == 'shape_oe_tuxpan_ccl') {
                            titulo = "OE Tuxpan";
                            arreglo = new Array();
                            objetos = {
                                'Politica': feature.attributes.politica,
                                'Uso politico': feature.attributes.uso_politi,
                                'Uso predominante': feature.attributes.usopredom
                            };
                            arreglo.push(objetos);
                        }
                        //ZONAS RESTAURACIÓN
                        else if (feature.gml.featureType == 'shape_sitios_prior_restauracion2016_ccl') {
                            titulo = "Sitios prioritarios de restauración";
                            arreglo = new Array();
                            objetos = {
                                'Prioridad': feature.attributes.prioridad,
                                'Tipo': feature.attributes.tipo,
                                'Área [km<sup>2</sup>]': feature.attributes.area_km2
                            };
                            arreglo.push(objetos);
                        } else if (feature.gml.featureType == 'shape_zonas_restauracion_lgeepa_ccl') {
                            titulo = "Zonas de restauración LGEEPA";
                            arreglo = new Array();
                            objetos = {
                                'Zona': feature.attributes.zona,
                                'Municipio': feature.attributes.municipio,
                                'Estado': feature.attributes.estado
                            };
                            arreglo.push(objetos);
                        }
                        //GRANIZADAS
                        else if (feature.gml.featureType == 'shape_granizadas_ccl') {
                            titulo = "Granizadas";
                            arreglo = new Array();
                            objetos = {
                                'Intensidad': feature.attributes.inf_compl
                            };
                            arreglo.push(objetos);
                        }
                        //HELADAS
                        else if (feature.gml.featureType == 'shape_heladas_ccl') {
                            titulo = "Índice de días con heladas por municipio";
                            arreglo = new Array();
                            objetos = {
                                'Indice de días con heladas': feature.attributes.peligro_po,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.estado_1
                            };
                            arreglo.push(objetos);
                        }
                        //NEVADAS
                        else if (feature.gml.featureType == 'shape_nevadas_ccl') {
                            titulo = "Índice de peligro por nevadas por municipio";
                            arreglo = new Array();
                            objetos = {
                                'Indice de peligro por nevadas': feature.attributes.cla_nev,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //TEMPERATURA MINIMA EXTREMA
                        else if (feature.gml.featureType == 'shape_temp_minima_extrema_ccl') {
                            titulo = "Índice de temperatura mínima extrema por municipio";
                            arreglo = new Array();
                            objetos = {
                                'Indice de peligro': feature.attributes.peligro__3,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.estado
                            };
                            arreglo.push(objetos);
                        }
                        //PELIGRO POR BAJAS TEMPERATURAS (ÍNDICES TEMPERATURA MÍNIMA HELADAS)
                        else if (feature.gml.featureType == 'shape_peligrobajastemperaturas_tempminheladas_ccl') {
                            titulo = "Peligro por bajas temperaturas índices temperatura mínima heladas";
                            arreglo = new Array();
                            objetos = {
                                'Indice de peligro': feature.attributes.cla_bajtem,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //PELIGRO POR CICLONES TROPICALES
                        else if (feature.gml.featureType == 'shape_peligro_ciclonestropicales_ccl') {
                            titulo = "Peligro por ciclones tropicales";
                            arreglo = new Array();
                            objetos = {
                                'Indice de peligro': feature.attributes.cla_cictro,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //PELIGRO POR SEQUIA
                        else if (feature.gml.featureType == 'shape_peligro_sequia_ccl') {
                            titulo = "Peligro por sequía";
                            arreglo = new Array();
                            objetos = {
                                'Indice de peligro': feature.attributes.cla_seq,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //PELIGRO POR SEQUIA (ESCALANTE 2005)
                        else if (feature.gml.featureType == 'shape_peligro_sequia_escalante2005_ccl') {
                            titulo = "Peligro por sequía (Escalante Sandoval 2005)";
                            arreglo = new Array();
                            objetos = {
                                //'Tipo de sequía': feature.attributes.sequia,
                                'Tipo de sequía': toTitleCase(feature.attributes.sequia),
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //PRESENCIA DE TORNADOS EN MUNICIPIOS DE MEXICO
                        else if (feature.gml.featureType == 'shape_presencia_tornados_mun_mexico_ccl') {
                            titulo = "Presencia de tornados en municipios de México";
                            arreglo = new Array();
                            objetos = {
                                'Índice': feature.attributes.ip_torn,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //RIESGO POR BAJAS TEMPERATURAS
                        else if (feature.gml.featureType == 'shape_riesgo_por_bajastemperaturas_ccl') {
                            titulo = "Riesgo por bajas temperaturas";
                            arreglo = new Array();
                            objetos = {
                                'Riesgo': feature.attributes.rcla_bajte,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //RIESGO POR GRANIZO
                        else if (feature.gml.featureType == 'shape_riesgo_por_granizo_ccl') {
                            titulo = "Riesgo por granizo";
                            arreglo = new Array();
                            objetos = {
                                'Riesgo': feature.attributes.rcla_gra,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //RIESGO POR INUNDACIÓN
                        else if (feature.gml.featureType == 'shape_rieso_por_inundacion_ccl') {
                            titulo = "Riesgo por inundación";
                            arreglo = new Array();
                            objetos = {
                                'Riesgo': feature.attributes.rinun,
                                'Municipio': feature.attributes.municipio,
                                'Estado': feature.attributes.noment
                            };
                            arreglo.push(objetos);
                        }
                        //RIESGO POR TORMENTAS ELECTRICAS
                        else if (feature.gml.featureType == 'shape_rieso_por_tormentaselectricas_ccl') {
                            titulo = "Riesgo por tormentas electricas";
                            arreglo = new Array();
                            objetos = {
                                'Riesgo': feature.attributes.rcla_torel,
                                'Municipio': feature.attributes.nom_mun,
                                'Estado': feature.attributes.nom_ent
                            };
                            arreglo.push(objetos);
                        }
                        //SEQUIAS HISTORICAS
                        else if (feature.gml.featureType == 'shape_sequias_historicas_ccl') {
                            titulo = "Sequías historicas";
                            arreglo = new Array();
                            objetos = {
                                'Período': feature.attributes.periodo
                            };
                            arreglo.push(objetos);
                        }
                        //ZONAS SUSCEPTIBLES A HUNDIMIENTOS Y DESLIZAMIENTOS
                        else if (feature.gml.featureType == 'shape_zonas_susceptibles_hundimientosdeslizamientos_ccl') {
                            titulo = "Zonas susceptibles a hundimientos y deslizamientos";
                            arreglo = new Array();
                            objetos = {
                                'Zona': feature.attributes.zona
                            };
                            arreglo.push(objetos);
                        }
                        //UMAS
                        else if (feature.gml.featureType == 'shape_umas_ccl') {
                            titulo = "UMAS";
                            arreglo = new Array();
                            objetos = {
                                'Clave': feature.attributes.clave,
                                'Nombre UMA': feature.attributes.nombre_uma,
                                'Fecha': feature.attributes.fecha,
                                'Estado': feature.attributes.nom_ent,
                                'Municipio': feature.attributes.nom_mun,
                                'Vigencia': feature.attributes.vigencia,
                                'Propietario': feature.attributes.propietari,
                                'Tenencia': feature.attributes.tenencia,
                                'Autorización': feature.attributes.autorizaci,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2005
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2005') {
                            titulo = "PSA 2005";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2006
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2006') {
                            titulo = "PSA 2006";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2007
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2007') {
                            titulo = "PSA 2007";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2008
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2008') {
                            titulo = "PSA 2008";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2009
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2009') {
                            titulo = "PSA 2009";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2010
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2010') {
                            titulo = "PSA 2010";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2011
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2011') {
                            titulo = "PSA 2011";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2012
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2012') {
                            titulo = "PSA 2012";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2013
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2013') {
                            titulo = "PSA 2013";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }
                        //PSA 2014
                        else if (feature.gml.featureType == 'shape_pago_servicios_ambientales_ccl_2014') {
                            titulo = "PSA 2014";
                            arreglo = new Array();
                            objetos = {
                                'Modalidad': feature.attributes.modalidad,
                                'Superficie': feature.attributes.superficie,
                                'Monto total': feature.attributes.monto_tota,
                                'Estado': feature.attributes.subreg_hid,
                                'Municipio': feature.attributes.municipio
                            };
                            arreglo.push(objetos);
                        }

                        //PRECIPITACION TOTAL ANUAL
                        else if (feature.gml.featureType == 'shape_precipitacion_total_anual_ccl') {
                            titulo = "Precipitación total anual";
                            arreglo = new Array();
                            var rango = toTitleCase(feature.attributes.preci_rang);
                            rango = rango.replace("A", "a");
                            objetos = {
                                'Rango de precipitación [mm]': rango,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //ANPS FEDERALES
                        else if (feature.gml.featureType == 'shape_anps_federales_ccl') {
                            titulo = "Áreas Naturales Protegidas (federales)";
                            arreglo = new Array();
                            //EXCEPCIONES DE TEXTO CAPITALIZADO
                            var nombre_anp = feature.attributes.nombre;

                            objetos = {
                                'Nombre': nombre_anp,
                                'Categoría de decreto según el DOF': feature.attributes.cat_decret,
                                'Categoría que define al ANP por su vocación y características, de acuerdo a las modificaciones realizadas en la LGEEPA.': feature.attributes.cat_manejo,
                                'Estados': feature.attributes.estados,
                                'Municipios': feature.attributes.municipios,
                                'Región': feature.attributes.region,
                                'Superficie total [ha]': feature.attributes.superficie,
                                'Superficie terrestre [ha]': feature.attributes.s_terres,
                                'Superficie marina [ha]': feature.attributes.s_marina,
                                'Fecha de la primera publicación en el DOF': formatDate(feature.attributes.prim_dec),
                                'Fecha de la última publicación en el DOF': formatDate(feature.attributes.ult_dof),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //ANP ESTATALES
                        else if (feature.gml.featureType == 'shape_anps_estatales_ccl') {
                            titulo = "Áreas Naturales Protegidas (estatales)";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.nombre),
                                'Tipo': feature.attributes.tipo,
                                'Estado': feature.attributes.estado,
                                'Fuente': feature.attributes.fuente,
                                'Superficie [ha]': feature.attributes.sup_dec2,
                            };
                            arreglo.push(objetos);
                        }
                        //ANP MUNICIPALES
                        else if (feature.gml.featureType == 'shape_anps_municipales_ccl') {

                            if (feature.attributes.nombre == "Reserva Ecológica Municipal de Tlacotalpan" || feature.attributes.nombre == "Reserva Ecológica Municipal de Acula") {
                                cualANPEstatal = feature.attributes.nombre;
                                mostrarActasCabildo = 1;
                            }

                            titulo = "Áreas Naturales Protegidas (municipales)";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.nombre),
                                'Estado': feature.attributes.estado,
                                'Fecha del decreto': formatDate(feature.attributes.f_dec),
                                'Categoria': feature.attributes.categoria,
                                'Fuente': feature.attributes.fuente,
                                'Superficie [ha]': feature.attributes.sup_dec,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //AICAS
                        else if (feature.gml.featureType == 'shape_aicas_ccl') {
                            titulo = "AICA";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.nombre),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //RTP
                        else if (feature.gml.featureType == 'shape_regiones_terre_prioritarias_ccl') {
                            titulo = "Regiones terrestres prioritarias";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': feature.attributes.nombre,
                                'Clave': feature.attributes.clave,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //RHP
                        else if (feature.gml.featureType == 'shape_regiones_hidro_prioritarias_ccl') {
                            titulo = "Regiones hidrológicas prioritarias";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': feature.attributes.nombre,
                                'Región': feature.attributes.region,
                                'Biodiversidad': feature.attributes.biodiv,
                                'Amenaza/riesgo': feature.attributes.amenaza,
                                'Uso': feature.attributes.uso,
                                'Falta de información': feature.attributes.descono,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }
                        //SPT
                        else if (feature.gml.featureType == 'shape_sitios_prioritarios_terrestres_ccl') {
                            titulo = "Sitios prioritarios terrestres";
                            arreglo = new Array();
                            objetos = {
                                'Prioridad': toTitleCase(feature.attributes.prioridad),
                                'Clave': feature.attributes.clave,
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        } else if (feature.gml.featureType == 'shape_ramsar_ccl') {
                            titulo = "Sitios Ramsar";
                            arreglo = new Array();
                            objetos = {
                                'Nombre': toTitleCase(feature.attributes.ramsar),
                                'Estado': feature.attributes.estado,
                                'Municipios': feature.attributes.municipios,
                                'Fecha': formatDate(feature.attributes.fecha),
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        } else if (feature.gml.featureType == 'shape_nueva_area_trabajopronatura_ccl') {
                            titulo = "Nueva área de trabajo de Pronatura";
                            arreglo = new Array();
                            objetos = {
                                'Área [km<sup>2</sup>]': parseFloat(feature.attributes.area_km2).toFixed(2)
                            };
                            arreglo.push(objetos);
                        }


                        items.push({
                            xtype: "propertygrid",
                            sortableColumns: false,
                            listeners: {
                                beforeedit: function () {
                                    return false;
                                }
                            },
                            title: titulo,
                            source: arreglo[0]
                        });
                    });

                    Ext.grid.PropertyColumnModel.prototype.nameText = 'Atributo';
                    Ext.grid.PropertyColumnModel.prototype.valueText = 'Valor';

                    bueno2 = Ext.create('Ext.panel.Panel', {
                        width: 325,
                        autoHeight: true,
                        layout: "fit",
                        resizable: false,
                        shadow: true,
                        border: false,
                        items: items
                    });


                    var panelAtributos = Ext.getCmp('panelAtributos');
                    panelAtributos.removeAll();
                    panelAtributos.add(bueno2);

                    //AGREGAR PANEL DE ACTAS DE CABILDO
                    if (mostrarActasCabildo == 1) {
                        if (cualANPEstatal == "Reserva Ecológica Municipal de Tlacotalpan")
                            nombreArchivo = "acta_cabildo_tlacotalpan.pdf";
                        else if (cualANPEstatal == "Reserva Ecológica Municipal de Acula")
                            nombreArchivo = "acta_cabildo_acula.pdf";

                        var panelActasCabildo = Ext.create('Ext.panel.Panel', {
                            title: 'Descargar',
                            header: true,
                            icon: 'images/new_icons/download.png',
                            width: 320,
                            border: false,
                            flex: 1,
                            bodyStyle: {
                                textAlign: 'center',
                                padding: '0px'
                            },
                            items: [{
                                xtype: 'button',
                                icon: 'images/new_icons/file_extension_pdf.png',
                                style: {
                                    backgroundColor: '#f5f5f5',
                                    borderColor: 'transparent',
                                    margin: '5px'
                                },
                                text: '<span style="font-weight:bold; color:#2C96D4;">Acta de cabildo</span>',
                                href: 'resources/actas_cabildo/' + nombreArchivo
                            }]
                        });
                        panelAtributos.add(panelActasCabildo);
                    }


                    panelAtributos.doLayout();
                    panelDerecho.setActiveTab(panelAtributos);
                }
            }
        });

        //#######################################
        selectControl = new OpenLayers.Control.SelectFeature(
            [vectorPuntos, vectorPoligonos, vectorPoligonosADVC], {
                clickout: true,
                toggle: false,
                multiple: false,
                hover: false,
                renderIntent: "temporary"
            }
        );

        map.addControl(selectControl);
        selectControl.activate();

        //FALTABA AÑADIRLO Y HABILITARLO
        map.addControl(featureInfo);
        featureInfo.activate();

        vectorPuntos.events.on({
            "featureselected": function (e) {
                onPointSelect(e.feature);
            },
            "featureunselected": function (e) {
                //Do nothing
                onUnSelectFeature();
            }
        });

        vectorPoligonos.events.on({
            "featureselected": function (e) {
                console.warn(e);
                onPointSelect(e.feature);
            },
            "featureunselected": function (e) {
                //Do nothing
                onUnSelectFeature();
            }
        });

        vectorPoligonosADVC.events.on({
            "featureselected": function (e) {
                onADVCSelect(e.feature);
            },
            "featureunselected": function (e) {
                //Do nothing
                onUnSelectFeature();
            }
        });
        //#####################################

        //FUNCION PARA LIMITAR A LA REPUBLICA MEXICANA EL PANEO
        function maxExtentBounds(llx, llxy, trx, trxy, epsg) {
            var point1 = new OpenLayers.Geometry.Point(llx, llxy),
                point2 = new OpenLayers.Geometry.Point(trx, trxy),
                bounds = new OpenLayers.Bounds();
            epsg = epsg || "EPSG:4326";
            point1.transform(
                new OpenLayers.Projection(epsg), map.getProjectionObject());
            point2.transform(
                new OpenLayers.Projection(epsg), map.getProjectionObject());
            bounds.extend(point1);
            bounds.extend(point2);
            bounds.toBBOX();
            return bounds;
        }
        map.restrictedExtent = maxExtentBounds(-118, 33.0, -83, 14.7);

        //RECARGAR MAPA///////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            handler: function () {
                window.location.reload();
            },
            map: map,
            //text: "Recargar mapa",
            tooltip: 'Recargar mapa',
            icon: 'images/new_icons/update.png'
        });
        actions["home"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        toolbarItems.push('-');
        //FIN DE LIMPIAR MAPA////////////////////////////////

        //ACERCAR////////////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            //text: "Acercar",
            control: new OpenLayers.Control.ZoomBox({
                alwaysZoom: true
            }),
            map: map,
            toggleGroup: "tools",
            group: "tools",
            cls: 'x-btn-text-icon',
            tooltip: 'Acercar',
            icon: 'images/new_icons/magnifier_zoom_in.png'
        });
        actions["zoom_in"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        //FIN DE ACERCAR/////////////////////////////////////

        //ALEJAR/////////////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            //text: "Alejar",
            control: new OpenLayers.Control.ZoomBox({
                alwaysZoom: true,
                out: true
            }),
            map: map,
            toggleGroup: "tools",
            group: "tools",
            cls: 'x-btn-text-icon',
            tooltip: 'Alejar',
            icon: 'images/new_icons/magnifier_zoom_out.png'
        });
        actions["zoom_out"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        //FIN DE ALEJAR///////////////////////////////////////

        //MOVER///////////////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            //text: "Mover",
            control: new OpenLayers.Control.Navigation(),
            map: map,
            toggleGroup: "tools",
            group: "tools",
            cls: 'x-btn-text-icon',
            tooltip: 'Mover',
            icon: 'images/new_icons/hand.png',
            pressed: true,
            allowDepress: false
        });
        actions["paneo"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        //FIN DE MOVER////////////////////////////////////////

        //VER TODO EL MAPA////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            //text: "Ver todo el mapa",
            control: new OpenLayers.Control.ZoomToMaxExtent(),
            map: map,
            cls: 'x-btn-text-icon',
            tooltip: "Ver todo el mapa",
            icon: 'images/new_icons/world.png'
        });
        actions["max_extent"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        toolbarItems.push("-");
        //FIN DE VER TODO EL MAPA//////////////////////////////

        //TEMAS VISIBLES//////////////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            id: 'temas_visibles',
            text: 'Temas visibles: <b>2</b>',
            style: 'font-family:Roboto; color:dimgray; text-align:center;',
            width: 'auto'
        });
        actions["tema_activo"] = action;
        toolbarItems.push(Ext.create('Ext.toolbar.TextItem', action));
        toolbarItems.push("-");
        //FIN DE TEMA ACTIVO///////////////////////////////////////

        //TEMA ACTIVO//////////////////////////////////////////////
        action = Ext.create('GeoExt.Action', {
            id: 'capa_activa',
            text: '<b>Ningún</b> tema esta activo',
            style: 'font-family:Roboto; color:dimgray; text-align:center;',
            width: 'auto'
        });
        actions["ca"] = action;
        toolbarItems.push(Ext.create('Ext.toolbar.TextItem', action));
        toolbarItems.push("-");
        //FIN DE TEMA ACTIVO///////////////////////////////////////

        //BOTON DESCARGAR TEMA//////////////////////////////////////////////
        toolbarItems.push(Ext.create('Ext.Button', {
            text: 'Descargar Tema',
            id: 'btn_descargar_metadato',
            cls: 'button-text',
            disabledCls: 'button-text-disabled',
            disabled: true,
            hidden: true,
            listeners: {
                disable: function (cmp, eOpts) {
                    //console.log("Button disabled");
                },
                enable: function (cmp, eOpts) {
                    //console.log("Button enabled");
                    cmp.setText("Descargar Tema");
                }
            },
            menu: {
                xtype: 'menu',
                items: [{
                        text: 'Shapefile',
                        iconCls: 'zip_icon',
                        tooltip: 'Proyección: Cónica conforme de Lambert (WGS84)'
                    },
                    {
                        text: 'KML',
                        id: 'the_kml',
                        iconCls: 'kml_icon',
                        handler: function (button, event) {
                            //DETERMINAR LA CAPA ACTUAL
                            var capa_actual = "";
                            var ca = Ext.getCmp('capa_activa').getEl().dom.innerHTML;
                            ca = ca.replace("Tema activo: <b>", "");
                            ca = ca.replace("</b>", "");
                            ca = ca.trim();
                            //console.log('ca:' + ca);
                            //PONERLE EL NOMBRE DEL LAYER QUE TIENE EN GEOSERVER A LA VARIABLE 'capa_activa' 
                            switch (ca) {
                                case "ADVC":
                                    capa_actual = "shape_advc_ccl";
                                    break;
                                case "Corrientes de agua":
                                    capa_actual = "shape_corrientes_agua_ccl";
                                    break;
                                case "Cuerpos de agua":
                                    capa_actual = "shape_cuerpos_agua_ccl";
                                    break;
                                case "Localidades":
                                    capa_actual = "shape_localidades_ccl";
                                    break;
                                case "Vialidades":
                                    capa_actual = "shape_vialidades_ccl";
                                    break;
                                case "Estados":
                                    capa_actual = "shape_estados_ccl";
                                    break;
                                case "Municipios":
                                    capa_actual = "shape_municipios_ccl";
                                    break;
                                case "Edafología":
                                    capa_actual = "shape_edafologia_ccl";
                                    break;
                                case "Geología":
                                    capa_actual = "shape_geologia_ccl";
                                    break;
                                case "Climas":
                                    capa_actual = "shape_climas_ccl";
                                    break;
                                case "Cuencas":
                                    capa_actual = "shape_cuencas_ccl";
                                    break;
                                case "Zonas funcionales":
                                    capa_actual = "shape_zonas_funcionales_ccl";
                                    break;
                                case "OE Bobos":
                                    capa_actual = "shape_oe_bobos_ccl";
                                    break;
                                case "OE Coatzacoalcos":
                                    capa_actual = "shape_oe_coatzacoalcos_ccl";
                                    break;
                                case "OE Tuxpan":
                                    capa_actual = "shape_oe_tuxpan_ccl";
                                    break;
                                case "Cobertura SAR-MOD":
                                    capa_actual = "shape_sarmod_ccl";
                                    break;
                                case "Sitios prioritarios de restauración":
                                    capa_actual = "shape_sitios_prior_restauracion2016_ccl";
                                    break;
                                case "Zonas de restauración LGEEPA":
                                    capa_actual = "shape_zonas_restauracion_lgeepa_ccl";
                                    break;
                                case "Granizadas":
                                    capa_actual = "shape_granizadas_ccl";
                                    break;
                                case "Índice de días con heladas por municipio":
                                    capa_actual = "shape_heladas_ccl";
                                    break;
                                case "Índice de peligro por nevadas por municipio":
                                    capa_actual = "shape_nevadas_ccl";
                                    break;
                                case "Índice de temperatura mínima extrema por municipio":
                                    capa_actual = "shape_temp_minima_extrema_ccl";
                                    break;
                                case "Peligro por bajas temperaturas índices temperatura mínima heladas":
                                    capa_actual = "shape_peligrobajastemperaturas_tempminheladas_ccl";
                                    break;
                                case "Peligro por ciclones tropicales":
                                    capa_actual = "shape_peligro_ciclonestropicales_ccl";
                                    break;
                                case "Peligro por sequía":
                                    capa_actual = "shape_peligro_sequia_ccl";
                                    break;
                                    //news
                                case "Peligro por sequía (Escalante Sandoval 2005)":
                                    capa_actual = "shape_peligro_sequia_escalante2005_ccl";
                                    break;
                                case "Presencia de tornados en municipios de México":
                                    capa_actual = "shape_peligrshape_presencia_tornados_mun_mexico_cclo_sequia_ccl";
                                    break;
                                case "Riesgo por bajas temperaturas":
                                    capa_actual = "shape_riesgo_por_bajastemperaturas_ccl";
                                    break;
                                case "Riesgo por granizo":
                                    capa_actual = "shape_riesgo_por_granizo_ccl";
                                    break;
                                case "Riesgo por inundación":
                                    capa_actual = "shape_rieso_por_inundacion_ccl";
                                    break;
                                case "Riesgo por tormentas electricas":
                                    capa_actual = "shape_rieso_por_tormentaselectricas_ccl";
                                    break;
                                case "Sequías historicas":
                                    capa_actual = "shape_sequias_historicas_ccl";
                                    break;
                                case "Zonas susceptibles a hundimientos y deslizamientos":
                                    capa_actual = "shape_zonas_susceptibles_hundimientosdeslizamientos_ccl";
                                    break;
                                case "UMAS":
                                    capa_actual = "shape_umas_ccl";
                                    break;
                                case "PSA 2005":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2005";
                                    break;
                                case "PSA 2006":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2006";
                                    break;
                                case "PSA 2007":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2007";
                                    break;
                                case "PSA 2008":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2008";
                                    break;
                                case "PSA 2009":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2009";
                                    break;
                                case "PSA 2010":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2010";
                                    break;
                                case "PSA 2011":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2011";
                                    break;
                                case "PSA 2012":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2012";
                                    break;
                                case "PSA 2013":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2013";
                                    break;
                                case "PSA 2014":
                                    capa_actual = "shape_pago_servicios_ambientales_ccl_2014";
                                    break;
                                case "Precipitación total anual":
                                    capa_actual = "shape_precipitacion_total_anual_ccl";
                                    break;
                                case "Uso de suelo y vegetación V":
                                    capa_actual = "shape_vegetacion_serie_v_ccl";
                                    break;
                                case "Uso de suelo y vegetación VI":
                                    capa_actual = "shape_usv250s6_ccl";
                                    break;
                                case "ANP (federales)":
                                    capa_actual = "shape_anps_federales_ccl";
                                    break;
                                case "ANP (estatales)":
                                    capa_actual = "shape_anps_estatales_ccl";
                                    break;
                                case "ANP (municipales)":
                                    capa_actual = "shape_anps_municipales_ccl";
                                    break;
                                case "Zonas arqueológicas":
                                    capa_actual = "shape_zonas_arqueologicas_ccl";
                                    break;
                                case "Sitios Cero Extinción de anfibios":
                                    capa_actual = "shape_zero_extinction_ccl";
                                    break;
                                case "Regiones indigenas":
                                    capa_actual = "shape_regiones_indigenas_ccl";
                                    break;
                                case "Unidades Productoras de Germoplasma":
                                    capa_actual = "shape_upgs_ccl";
                                    break;
                                case "AICA":
                                    capa_actual = "shape_aicas_ccl";
                                    break;
                                case "Regiones terrestres prioritarias":
                                    capa_actual = "shape_regiones_terre_prioritarias_ccl";
                                    break;
                                case "Regiones hidrológicas prioritarias":
                                    capa_actual = "shape_regiones_hidro_prioritarias_ccl";
                                    break;
                                case "Sitios prioritarios terrestres":
                                    capa_actual = "shape_sitios_prioritarios_terrestres_ccl";
                                    break;
                                case "Sitios Ramsar":
                                    capa_actual = "shape_ramsar_ccl";
                                    break;
                                case "Aproximación 1":
                                    capa_actual = "shape_aproximacion_1_ccl";
                                    break;
                                case "Nueva área de trabajo":
                                    capa_actual = "shape_nueva_area_trabajopronatura_ccl";
                                    break;
                                case "Red de viveros de biodiversidad":
                                    capa_actual = "shape_viveros_biodiversidad_ccl";
                                    break;
                                default:
                                    break;
                            }
                            //CREAR LA RUTA CONCATENADA
                            var ruta_kml = rutaGeoserver + "pronatura/wms/kml?layers=pronatura:" + capa_actual;
                            //ABRIR LIGA DE GEOSERVER PARA GENERAR KML
                            window.open(ruta_kml, 'download');
                        }
                    }
                ],
                listeners: {
                    click: function (menu, item, e, eOpts) {
                        //console.log("ITEM: " + item.text);

                        if (item.text == 'Shapefile') {
                            if (ruta_shapefile_metadato_zip != null) {
                                //NUEVO: GENERAR EN TIEMPO REAL EL SHAPEFILE DE PUNTOS Y POLIGONOS CON OGR Y UNA SOLICITUD POST
                                if (ruta_shapefile_metadato_zip == 'GENERAR_EN_TIEMPO_REAL') {

                                    //console.log("GENERAR EN TIEMPO REAL");

                                    Ext.getCmp('btn_descargar_metadato').setText('<i class="fa fa-spinner fa-spin"></i>&nbsp;Comprimiendo archivo...').disable();
                                    $.post('getLastShapefilePuntosPoligonos.php', function (result) {
                                        if (result != 'error') {
                                            var arrayResult = result.split(",");
                                            var rutaCompleta = arrayResult[0];
                                            var nombreArchivo = arrayResult[1];
                                            var link = document.createElement("a");
                                            link.download = nombreArchivo;
                                            link.href = "../" + rutaCompleta;
                                            link.click();
                                            Ext.getCmp('btn_descargar_metadato').enable();
                                        }
                                    });
                                } else {
                                    var arrayRuta = ruta_shapefile_metadato_zip.split("/");
                                    var nombreArchivo = arrayRuta[2];
                                    var link = document.createElement("a");
                                    link.download = nombreArchivo;
                                    link.href = ruta_shapefile_metadato_zip;
                                    link.click();
                                }

                            }
                        }

                    }
                }
            }
        }));
        toolbarItems.push("-");
        //FIN DE BOTON DESCARGAR TEMA///////////////////////////////////////


        //CREAMOS EL PANEL DEL MAPA
        var panelMapa = Ext.create('GeoExt.panel.Map', {
            map: map,
            dockedItems: [{
                xtype: 'toolbar',
                id: 'mapPanel_toolbar',
                items: toolbarItems
            }]
        });

        //CREAMOS EL ALMACEN DE CAPAS
        var store = Ext.create('Ext.data.TreeStore', {
            model: 'GeoExt.data.LayerTreeModel',
            root: {
                expanded: true,
                children: []
            }
        });

        var temas_activos = [];

        //GENERAMOS EL ALMACEN Y EL PANEL DE CAPAS (PARA ORDENAR)
        var treeStoreOrder = Ext.create('Ext.data.TreeStore', {
            model: 'GeoExt.data.LayerTreeModel',
            root: {
                text: 'Arrastre hacia arriba o abajo para reordenar',
                iconCls: 'ordenar_icon',
                plugins: [{
                    ptype: 'gx_layercontainer',
                    loader: {
                        store: panelMapa.layers
                    }
                }],
                expanded: true
            }
        });

        var arbolTemasOrden = Ext.create('GeoExt.tree.Panel', {
            id: 'arbolTemasOrden',
            cls: 'x-tree-noicon',
            preventHeader: true,

            minWidth: 500,
            scrollable: 'y',

            animate: true,
            rootVisible: true,
            store: treeStoreOrder,
            viewConfig: {
                plugins: {
                    ptype: 'treeviewdragdrop'
                }
            },
            listeners: {
                afterrender: function (cmp, eOpts) {
                    cmp.hide();
                },
                beforeitemcollapse: function (node, eOpts) {
                    return false;
                }
            }
        });

        //CREAMOS EL ARBOL DE TEMAS
        var arbolTemas = Ext.create('GeoExt.tree.Panel', {
            preventHeader: true,
            id: 'arbolTemas',

            minWidth: 500,
            scrollable: 'y',
            //forceFit:true,

            animate: true,
            store: store,
            rootVisible: false,
            listeners: {
                checkchange: function (node, checked, eOpts) {
                    //CONDICION QUE EVALUA SI ESTA ACTIVADO EL CHECKBOX DE APC Y ALTERNA SU VISIBILIDAD
                    if (node.get('id') == 'tree_ubi') {
                        if (checked) {
                            vectorPuntos.setVisibility(true);
                            vectorPoligonos.setVisibility(true);
                        } else if (!checked) {
                            vectorPuntos.setVisibility(false);
                            vectorPoligonos.setVisibility(false);
                        }
                    }

                    //PARA ADVC
                    if (node.get('id') == 'tree_advc') {
                        if (checked) {
                            vectorPoligonosADVC.setVisibility(true);
                        } else if (!checked) {
                            vectorPoligonosADVC.setVisibility(false);
                        }
                    }


                    //FUNCION QUE DETERMINA QUE TEMAS ESTAN ACTIVOS
                    if (node) {
                        if (checked) {
                            temas_activos.push(node.get('id'));
                        } else if (!checked) {
                            var item = node.get('id');
                            for (var i = temas_activos.length; i--;) {
                                if (temas_activos[i] === item) {
                                    temas_activos.splice(i, 1);
                                }
                            }
                        }
                    }
                    console.warn('Temas activos: ' + temas_activos);

                    //FUNCION QUE ORDENA LAS CAPAS DE ACUERDO A COMO LAS VAMOS MARCANDO CON EL CHECKBOX
                    var tope = 492;
                    for (var i = temas_activos.length; i--;) {
                        if (temas_activos[i] == 'tree_vegetacion') {
                            shape_vegetacion.setZIndex(tope--);
                            console.warn('Vegetación: ' + shape_vegetacion.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_vegetacion_s6') {
                            shape_vegetacion_s6.setZIndex(tope--);
                            console.warn('Vegetación VI: ' + shape_vegetacion_s6.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_zonas_arqueologicas') {
                            shape_zonas_arqueologicas.setZIndex(tope--);
                            console.warn('Zonas arqueológicas: ' + shape_zonas_arqueologicas.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_zero_extinction') {
                            shape_zero_extinction.setZIndex(tope--);
                            console.warn('Zero extinction: ' + shape_zero_extinction.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_viveros') {
                            shape_viveros.setZIndex(tope--);
                            console.warn('Viveros: ' + shape_viveros.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_aproximacion1') {
                            shape_aproximacion1.setZIndex(tope--);
                            console.warn('Aproximación 1: ' + shape_aproximacion1.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_regiones_indigenas') {
                            shape_regiones_indigenas.setZIndex(tope--);
                            console.warn('Regiones indigenas: ' + shape_regiones_indigenas.getZIndex());
                        }
                        if (temas_activos[i] == 'treeUpgs') {
                            shape_upgs.setZIndex(tope--);
                            console.warn('UPGS: ' + shape_upgs.getZIndex());
                        }
                        //
                        if (temas_activos[i] == 'tree_edafologia') {
                            shape_edafologia.setZIndex(tope--);
                            console.warn('Edafologia: ' + shape_edafologia.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_geologia') {
                            shape_geologia.setZIndex(tope--);
                            console.warn('Geologia: ' + shape_geologia.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_climas') {
                            shape_climas.setZIndex(tope--);
                            console.warn('Climas: ' + shape_climas.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_cuencas') {
                            shape_cuencas.setZIndex(tope--);
                            console.warn('Cuencas: ' + shape_cuencas.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_zonas_funcionales') {
                            shape_zonas_funcionales.setZIndex(tope--);
                            console.warn('Zonas funcionales: ' + shape_zonas_funcionales.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_oe_bobos') {
                            shape_oe_bobos.setZIndex(tope--);
                            console.warn('OE Bobos: ' + shape_oe_bobos.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_oe_coatzacoalcos') {
                            shape_oe_coatzacoalcos.setZIndex(tope--);
                            console.warn('OE Coatzacoalcos: ' + shape_oe_coatzacoalcos.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_oe_tuxpan') {
                            shape_oe_tuxpan.setZIndex(tope--);
                            console.warn('OE Tuxpan: ' + shape_oe_tuxpan.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_sarmod') {
                            shape_sarmod.setZIndex(tope--);
                            console.warn('SAR-MOD: ' + shape_sarmod.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_sitios_prior_restauracion2016') {
                            shape_sitios_prior_restauracion2016_ccl.setZIndex(tope--);
                            console.warn('Sitios prioritarios restauracion 2016: ' + shape_sitios_prior_restauracion2016_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_zonas_restauracion_lgeepa') {
                            shape_zonas_restauracion_lgeepa_ccl.setZIndex(tope--);
                            console.warn('Zonas restauracion LGEEPA: ' + shape_zonas_restauracion_lgeepa_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_granizadas') {
                            shape_granizadas_ccl.setZIndex(tope--);
                            console.warn('Granizadas: ' + shape_granizadas_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_heladas') {
                            shape_heladas_ccl.setZIndex(tope--);
                            console.warn('Índice de días con heladas por municipio: ' + shape_heladas_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_nevadas') {
                            shape_nevadas_ccl.setZIndex(tope--);
                            console.warn('Índice de peligro por nevadas por municipio: ' + shape_nevadas_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_temp_minima_extrema') {
                            shape_temp_minima_extrema_ccl.setZIndex(tope--);
                            console.warn('Índice de temperatura mínima extrema por municipio: ' + shape_temp_minima_extrema_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_peligrobajastemperaturas_tempminheladas') {
                            shape_peligrobajastemperaturas_tempminheladas_ccl.setZIndex(tope--);
                            console.warn('Peligro por bajas temperaturas índices temperatura mínima heladas: ' + shape_peligrobajastemperaturas_tempminheladas_ccl.getZIndex());
                        }

                        if (temas_activos[i] == 'tree_peligro_ciclonestropicales') {
                            shape_peligro_ciclonestropicales_ccl.setZIndex(tope--);
                            console.warn('Peligro ciclones tropicales: ' + shape_peligro_ciclonestropicales_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_peligro_sequia') {
                            shape_peligro_sequia_ccl.setZIndex(tope--);
                            console.warn('Peligro por sequía: ' + shape_peligro_sequia_ccl.getZIndex());
                        }

                        //NEWS
                        if (temas_activos[i] == 'tree_peligro_sequia_escalante2005') {
                            shape_peligro_sequia_escalante2005_ccl.setZIndex(tope--);
                            console.warn('Peligro por sequía (Escalante Sandoval 2005): ' + shape_peligro_sequia_escalante2005_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_presencia_tornados_mun_mexico') {
                            shape_presencia_tornados_mun_mexico_ccl.setZIndex(tope--);
                            console.warn('Presencia de tornados en municipios de México: ' + shape_presencia_tornados_mun_mexico_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_riesgo_por_bajastemperaturas') {
                            shape_riesgo_por_bajastemperaturas_ccl.setZIndex(tope--);
                            console.warn('Riesgo por bajas temperaturas: ' + shape_riesgo_por_bajastemperaturas_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_riesgo_por_granizo') {
                            shape_riesgo_por_granizo_ccl.setZIndex(tope--);
                            console.warn('Riesgo por granizo: ' + shape_riesgo_por_granizo_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_rieso_por_inundacion') {
                            shape_rieso_por_inundacion_ccl.setZIndex(tope--);
                            console.warn('Riesgo por inundación: ' + shape_rieso_por_inundacion_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_rieso_por_tormentaselectricas') {
                            shape_rieso_por_tormentaselectricas_ccl.setZIndex(tope--);
                            console.warn('Riesgo por tormentas electricas: ' + shape_rieso_por_tormentaselectricas_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_sequias_historicas') {
                            shape_sequias_historicas_ccl.setZIndex(tope--);
                            console.warn('Sequías historicas: ' + shape_sequias_historicas_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_zonas_susceptibles_hundimientosdeslizamientos') {
                            shape_zonas_susceptibles_hundimientosdeslizamientos_ccl.setZIndex(tope--);
                            console.warn('Zonas susceptibles a hundimientos y deslizamientos: ' + shape_zonas_susceptibles_hundimientosdeslizamientos_ccl.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_umas') {
                            shape_umas.setZIndex(tope--);
                            console.warn('UMAS: ' + shape_umas.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2005') {
                            shape_psa2005.setZIndex(tope--);
                            console.warn('PSA 2005: ' + shape_psa2005.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2006') {
                            shape_psa2006.setZIndex(tope--);
                            console.warn('PSA 2006: ' + shape_psa2006.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2007') {
                            shape_psa2007.setZIndex(tope--);
                            console.warn('PSA 2007: ' + shape_psa2007.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2008') {
                            shape_psa2008.setZIndex(tope--);
                            console.warn('PSA 2008: ' + shape_psa2008.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2009') {
                            shape_psa2009.setZIndex(tope--);
                            console.warn('PSA 2009: ' + shape_psa2009.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2010') {
                            shape_psa2010.setZIndex(tope--);
                            console.warn('PSA 2010: ' + shape_psa2010.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2011') {
                            shape_psa2011.setZIndex(tope--);
                            console.warn('PSA 2011: ' + shape_psa2011.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2012') {
                            shape_psa2012.setZIndex(tope--);
                            console.warn('PSA 2012: ' + shape_psa2012.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2013') {
                            shape_psa2013.setZIndex(tope--);
                            console.warn('PSA 2013: ' + shape_psa2013.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_psa2014') {
                            shape_psa2014.setZIndex(tope--);
                            console.warn('PSA 2014: ' + shape_psa2014.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_precTotAnual') {
                            shape_precTotAnual.setZIndex(tope--);
                            console.warn('Precipitación total anual: ' + shape_precTotAnual.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_anpFederales') {
                            shape_anpFederales.setZIndex(tope--);
                            console.warn('ANP Federales: ' + shape_anpFederales.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_anpEstatales') {
                            shape_anpEstatales.setZIndex(tope--);
                            console.warn('ANP Estatales: ' + shape_anpEstatales.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_anpMunicipales') {
                            shape_anpMunicipales.setZIndex(tope--);
                            console.warn('ANP Municipales: ' + shape_anpMunicipales.getZIndex());
                        }
                        /**/
                        if (temas_activos[i] == 'tree_aicas') {
                            shape_aicas.setZIndex(tope--);
                            console.warn('AICAS: ' + shape_aicas.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_regionesTerrestres') {
                            shape_regionesTerrestres.setZIndex(tope--);
                            console.warn('Regiones terrestres: ' + shape_regionesTerrestres.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_regionesHidrologicas') {
                            shape_regionesHidrologicas.setZIndex(tope--);
                            console.warn('Regiones hidrologicas: ' + shape_regionesHidrologicas.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_sitiosPrioritarios') {
                            shape_sitiosPrioritarios.setZIndex(tope--);
                            console.warn('Sitios prioritarios: ' + shape_sitiosPrioritarios.getZIndex());
                        }
                        if (temas_activos[i] == 'tree_sitiosRamsar') {
                            shape_sitiosRamsar.setZIndex(tope--);
                            console.warn('Sitios RAMSAR: ' + shape_sitiosRamsar.getZIndex());
                        }
                        /**/
                        if (temas_activos[i] == 'tree_areaTrabajo') {
                            shape_areaTrabajo.setZIndex(tope--);
                            console.warn('Area de trabajo: ' + shape_areaTrabajo.getZIndex());
                        }
                    }




                    //FUNCION QUE DETERMINA CUANTOS TEMAS ESTAN ACTIVOS
                    if (
                        node.get('id') == 'tree_ubi' ||
                        node.get('id') == 'tree_advc' ||
                        node.get('id') == 'tree_corrientes_agua' ||
                        node.get('id') == 'tree_cuerpos_agua' ||
                        node.get('id') == 'tree_localidades' ||
                        node.get('id') == 'tree_vialidades' ||
                        node.get('id') == 'tree_estados' ||
                        node.get('id') == 'tree_municipios' ||
                        node.get('id') == 'tree_vegetacion' ||
                        node.get('id') == 'tree_vegetacion_s6' ||
                        node.get('id') == 'tree_zonas_arqueologicas' ||
                        node.get('id') == 'tree_zero_extinction' ||
                        node.get('id') == 'tree_viveros' ||
                        node.get('id') == 'tree_aproximacion1' ||
                        node.get('id') == 'tree_regiones_indigenas' ||
                        node.get('id') == 'treeUpgs' ||
                        node.get('id') == 'tree_edafologia' ||
                        node.get('id') == 'tree_geologia' ||
                        node.get('id') == 'tree_climas' ||
                        node.get('id') == 'tree_cuencas' ||
                        node.get('id') == 'tree_zonas_funcionales' ||
                        node.get('id') == 'tree_oe_bobos' ||
                        node.get('id') == 'tree_oe_coatzacoalcos' ||
                        node.get('id') == 'tree_oe_tuxpan' ||
                        node.get('id') == 'tree_sarmod' ||
                        node.get('id') == 'tree_sitios_prior_restauracion2016' ||
                        node.get('id') == 'tree_zonas_restauracion_lgeepa' ||
                        node.get('id') == 'tree_granizadas' ||
                        node.get('id') == 'tree_heladas' ||
                        node.get('id') == 'tree_nevadas' ||
                        node.get('id') == 'tree_temp_minima_extrema' ||
                        node.get('id') == 'tree_peligrobajastemperaturas_tempminheladas' ||
                        node.get('id') == 'tree_peligro_ciclonestropicales' ||
                        node.get('id') == 'tree_peligro_sequia' ||
                        node.get('id') == 'tree_peligro_sequia_escalante2005' ||
                        node.get('id') == 'tree_presencia_tornados_mun_mexico' ||
                        node.get('id') == 'tree_riesgo_por_bajastemperaturas' ||
                        node.get('id') == 'tree_riesgo_por_granizo' ||
                        node.get('id') == 'tree_rieso_por_inundacion' ||
                        node.get('id') == 'tree_rieso_por_tormentaselectricas' ||
                        node.get('id') == 'tree_sequias_historicas' ||
                        node.get('id') == 'tree_zonas_susceptibles_hundimientosdeslizamientos' ||
                        node.get('id') == 'tree_umas' ||
                        node.get('id') == 'tree_psa2005' ||
                        node.get('id') == 'tree_psa2006' ||
                        node.get('id') == 'tree_psa2007' ||
                        node.get('id') == 'tree_psa2008' ||
                        node.get('id') == 'tree_psa2009' ||
                        node.get('id') == 'tree_psa2010' ||
                        node.get('id') == 'tree_psa2011' ||
                        node.get('id') == 'tree_psa2012' ||
                        node.get('id') == 'tree_psa2013' ||
                        node.get('id') == 'tree_psa2014' ||
                        node.get('id') == 'tree_precTotAnual' ||
                        node.get('id') == 'tree_anpFederales' ||
                        node.get('id') == 'tree_anpEstatales' ||
                        node.get('id') == 'tree_anpMunicipales' ||
                        node.get('id') == 'tree_aicas' ||
                        node.get('id') == 'tree_regionesTerrestres' ||
                        node.get('id') == 'tree_regionesHidrologicas' ||
                        node.get('id') == 'tree_sitiosPrioritarios' ||
                        node.get('id') == 'tree_sitiosRamsar' ||
                        node.get('id') == 'tree_areaTrabajo'
                    ) {
                        if (checked)
                            cuantos_temas_visibles++;
                        else
                            cuantos_temas_visibles--;
                    }
                    Ext.get('temas_visibles').update('Temas visibles: <b>' + cuantos_temas_visibles + '</b>');
                },
                itemclick: function (view, rec, item, index, eventObj) {
                    var element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl;
                    Ext.getCmp('the_kml').enable().show();
                    Ext.getCmp('btn_descargar_metadato').setHidden(false);

                    if (rec.get('id') == 'tree_advc') {
                        element = rec.get('id');
                        title = "Áreas Destinadas Voluntariamente a la Conservación";
                        shapename = "shape_advc_ccl";
                        source = "<b>Comisión Nacional de Áreas Naturales Protegidas</b>";
                        summary = "La certificación es una herramienta que ayuda a los propietarios al establecimiento, administración y manejo de sus áreas naturales protegidas privadas. Es un proceso unilateral por parte del proponente, la CONANP participa como fedatario de la voluntad de conservar sus predios y de las políticas, criterios y acciones que el promovente pretende realizar para lograr sus fines.";
                        layername = "shape_anpFederales";
                        metadataHtmlUrl = "metadatos/html/metadatoADVC.html";
                        metadataZipUrl = "metadatos/zip/shape_advc_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('the_kml').disable().hide();
                    } else if (rec.get('id') == 'tree_corrientes_agua') {
                        element = rec.get('id');
                        title = "Corrientes de agua";
                        shapename = "shape_corrientes_agua_ccl";
                        source = "<b>INEGI</b>&nbsp;1:250 000";
                        summary = "Los Conjuntos de Datos Vectoriales de la Carta Topográfica, contienen la información sobre los diversos rasgos geográficos presentes en la Carta Topográfica impresa, como curvas de nivel, hidrografía, vías de comunicación, localidades, entre otros. Estos rasgos son representados digitalmente por un componente geométrico (puntos, líneas o áreas), y un componente descriptivo (los atributos del rasgo). Los topónimos o nombres geográficos se proporcionan en un producto separado como conjunto de datos toponímicos.";
                        layername = "shape_corrientes_agua";
                        metadataHtmlUrl = "metadatos/html/conjunto_datos_vectoriales_ct_1_250k.html";
                        metadataZipUrl = "metadatos/zip/shape_corrientes_agua_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_cuerpos_agua') {
                        element = rec.get('id');
                        title = "Cuerpos de agua";
                        shapename = "shape_cuerpos_agua_ccl";
                        source = "<b>INEGI</b>&nbsp;1:250 000";
                        summary = "Los Conjuntos de Datos Vectoriales de la Carta Topográfica, contienen la información sobre los diversos rasgos geográficos presentes en la Carta Topográfica impresa, como curvas de nivel, hidrografía, vías de comunicación, localidades, entre otros. Estos rasgos son representados digitalmente por un componente geométrico (puntos, líneas o áreas), y un componente descriptivo (los atributos del rasgo). Los topónimos o nombres geográficos se proporcionan en un producto separado como conjunto de datos toponímicos.";
                        layername = "shape_cuerpos_agua";
                        metadataHtmlUrl = "metadatos/html/conjunto_datos_vectoriales_ct_1_250k.html";
                        metadataZipUrl = "metadatos/zip/shape_cuerpos_agua_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_localidades') {
                        element = rec.get('id');
                        title = "Localidades";
                        shapename = "shape_localidades_ccl";
                        source = "<b>INEGI</b>&nbsp;1:250 000. 'Localidades de la República Mexicana, 2010'. Obtenido de Principales resultados por localidad (ITER). Censo de Población y Vivienda 2010.";
                        summary = "El mapa contiene la identificación geográfica de las 192 245 localidades habitadas, como resultado del Censo de Población y Vivienda del 2010. Cada registro incluye los valores de longitud, latitud, altitud y población total.";
                        layername = "shape_localidades";
                        metadataHtmlUrl = "metadatos/html/localidades.html";
                        metadataZipUrl = "metadatos/zip/shape_localidades_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_vialidades') {
                        element = rec.get('id');
                        title = "Vialidades";
                        shapename = "shape_vialidades_ccl";
                        source = "<b>INEGI</b>&nbsp;1:250 000";
                        summary = "Los Conjuntos de Datos Vectoriales de la Carta Topográfica escala 1:250 000, contienen la información sobre los diversos rasgos geográficos presentes en la Carta Topográfica impresa, como curvas de nivel, hidrografía, vias de comunicación, localidades, entre otros. Estos rasgos son representados digitalmente por un componente geométrico (puntos, líneas o áreas), y un componente descriptivo (los atributos del rasgo). Los topónimos o nombres geográficos se proporcionan en un producto separado como conjunto de datos toponímicos.";
                        layername = "shape_vialidades";
                        metadataHtmlUrl = "metadatos/html/conjunto_datos_vectoriales_ct_1_250k.html";
                        metadataZipUrl = "metadatos/zip/shape_vialidades_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_ubi') {
                        element = rec.get('id');
                        title = "Áreas Privadas de Conservación";
                        shapename = "shape_puntos_legend_ccl";
                        source = "<b>Pronatura Veracruz A.C.</b>";
                        summary = "Las APC se representan en dos formas:<br><br><b>Puntos:</b>&nbsp;Corresponden a APC que no cuentan con una delimitación del predio. Cuando varias APC estan muy cerca, se muestra un círculo con un número en el centro. Esto significa que bajo el círculo hay dicha cantidad de APC. Al hacer clic o hacer zoom se mostrará mas detalle.<br><br><b>Polígonos:</b>&nbsp;Son APC que sí tienen delimitada su extensión. Se representan con un borde amarillo.";
                        layername = "shape_anpFederales";
                        metadataHtmlUrl = "metadatos/html/metadato.html";
                        //metadataZipUrl  = "metadatos/zip/shape_apcs_puntos_poligonos_ccl.zip";
                        metadataZipUrl = "GENERAR_EN_TIEMPO_REAL";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('the_kml').disable().hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                    } else if (rec.get('id') == 'tree_estados') {
                        element = rec.get('id');
                        title = "Estados";
                        shapename = "shape_estados_paraSimbologia_ccl";
                        source = "<b>INEGI</b> 1:250 000";
                        summary = "El Marco Geoestadístico (Junio 2016) es la división del país en Áreas Geoestadísticas con tres niveles de desagregación: Estatal (AGEE), Municipal (AGEM) y Básica (AGEB), Polígonos Urbanos y Rurales, Territorio Insular y la Integración Territorial del País. Los Límites Estatales y Municipales se componen en algunos de sus trazos de límites político-administrativos, en su mayoría son límites geoestadísticos.";
                        layername = "shape_estados";
                        metadataHtmlUrl = "metadatos/html/marco_geoestadistico_2016.html";
                        metadataZipUrl = "metadatos/zip/shape_estados_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_municipios') {
                        element = rec.get('id');
                        title = "Municipios";
                        shapename = "shape_municipios_ccl";
                        source = "<b>INEGI</b> 1:250 000";
                        summary = "El Marco Geoestadístico (Junio 2016) es la división del país en Áreas Geoestadísticas con tres niveles de desagregación: Estatal (AGEE), Municipal (AGEM) y Básica (AGEB), Polígonos Urbanos y Rurales, Territorio Insular y la Integración Territorial del País. Los Límites Estatales y Municipales se componen en algunos de sus trazos de límites político-administrativos, en su mayoría son límites geoestadísticos.";
                        layername = "shape_municipios";
                        metadataHtmlUrl = "metadatos/html/marco_geoestadistico_2016.html";
                        metadataZipUrl = "metadatos/zip/shape_municipios_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    }
                    //NUEVAS
                    else if (rec.get('id') == 'tree_vegetacion') {
                        element = rec.get('id');
                        title = "Uso de suelo y vegetación V";
                        shapename = "shape_vegetacion_serie_v_ccl";
                        source = "<b>INEGI</b>&nbsp;1:250 000";
                        summary = "Los Conjuntos de Datos Vectoriales de Uso del Suelo y Vegetación, Escala 1:250 000 - Serie V, contiene información del Uso del Suelo y Vegetación obtenida a partir de la aplicación de técnicas de fotointerpretación con imágenes de satélite Landsat TM5 seleccionadas del año 2011. Esta interpretación está apoyada con trabajos de campo realizado de . Los Conjuntos de Datos contienen la ubicación, distribución y extensión de diferentes comunidades vegetales y usos agrícolas con sus respectivas variantes en tipos de vegetación, de usos agrícolas, e información ecológica relevante. Dicha información geográfica digital contiene datos estructurados en forma vectorial codificados de acuerdo con el Diccionario de Datos Vectoriales de Uso del Suelo y Vegetación Serie IV para la Escala 1:250 000 aplicables a las diferentes unidades ecológicas (comunidades vegetales y usos antrópicos) contenidos en el conjunto de dato.";
                        layername = "shape_vegetacion";
                        metadataHtmlUrl = "metadatos/html/uso_suelo_vegetacion.html";
                        metadataZipUrl = "metadatos/zip/shape_uso_suelo_vegetacion_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_vegetacion_s6') {
                        element = rec.get('id');
                        title = "Uso de suelo y vegetación VI";
                        shapename = "shape_usv250s6_ccl";
                        source = "<b>INEGI 2016</b>&nbsp;1:250 000";
                        summary = "Los Conjuntos de Datos Vectoriales de Uso del Suelo y Vegetación, Escala 1:250 000 - Serie VI, contiene información del Uso del Suelo y Vegetación obtenida a partir de la aplicación de técnicas de fotointerpretación con imágenes de satélite Landsat TM8 seleccionadas del año 2014. Esta interpretación está apoyada con trabajos de campo realizado de . Los Conjuntos de Datos contienen la ubicación, distribución y extensión de diferentes comunidades vegetales y usos agrícolas con sus respectivas variantes en tipos de vegetación, de usos agrícolas, e información ecológica relevante. Dicha información geográfica digital contiene datos estructurados en forma vectorial codificados de acuerdo con el Diccionario de Datos Vectoriales de Uso del Suelo y Vegetación Escala 1:250 000 (version 3)aplicables a las diferentes unidades ecológicas (comunidades vegetales y usos antrópicos) contenidos en el conjunto de dato. CONTENIDO - Tipos de vegetación por su afinidad ecológica y composición florística, agrupados en dos niveles jerárquicos. Los tipos de vegetación están definidos con base al sistema de clasificación de los tipos de vegetación de México del INEGI y ordenados por grandes grupos de vegetación. - Estado sucesional actual de la vegetación según el grado de cambio o alteración de la cubierta vegetal (Vegetación Secundaria). - Distribución de las comunidades vegetales con base en el reconocimiento de sus variantes definidas por elementos ecológicos, florísticos y fisonómicos distintivos. - Tipos de agricultura por disponibilidad de agua durante el ciclo agrícola y duración del ciclo de cultivo.";
                        layername = "shape_vegetacion_s6";
                        metadataHtmlUrl = "metadatos/html/usv250s6_ccl.html";
                        metadataZipUrl = "metadatos/zip/usv250s6_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_zonas_arqueologicas') {
                        element = rec.get('id');
                        title = "Zonas arqueológicas";
                        shapename = "shape_zonas_arqueologicas_ccl";
                        source = "<b>INAH</b>&nbsp;2016 (a través del INAI).";
                        summary = "El Instituto Nacional de Antropología e Historia (INAH) tiene bajo su resguardo un total de 187 zonas arqueológicas abiertas al público en todo el país, los 365 días del año.";
                        layername = "shape_zonas_arqueologicas";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_zonas_arqueologicas_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_zero_extinction') {
                        element = rec.get('id');
                        title = "Sitios Cero Extinción de anfibios";
                        shapename = "shape_zero_extinction_ccl";
                        source = "<b>CONABIO</b>&nbsp;Zonas críticas y de alto riesgo para la conservación de la biodiversidad de México";
                        summary = "De las 360 especies de anfibios de México (Flores-Villelay Canseco-Márquez 2004), 121 son prioritarias en esta estrategia de sitios cero extinciones; esto representa alrededor de 33% del total nacional. Todas estas especies presentan distribuciones restringidas de hasta 5 000 km2, son endémicas de México y 70 de ellas (58%) se encuentran consideradas en riesgo de extinción. Estas especies se encuentran distribuidas en 92 localidades de 18 estados. Entre los estados con mayor número de localidades y especies se encuentran Guerrero, Oaxaca y Veracruz. El número de especies en sitios cero extinciones varían de una a seis por localidad, pero un porcentaje considerable (45%) de las localidades presenta solo una especie. Entre las localidades con más especies están en los alrededores de Xico (Veracruz), Atoyac de Álvarez (Guerrero) e Ixtlán de Juárez (Oaxaca).";
                        layername = "shape_zero_extinction";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_zero_extinction_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_viveros') {
                        element = rec.get('id');
                        title = "Red de viveros de biodiversidad";
                        shapename = "shape_viveros_biodiversidad_ccl_paraSimbologia";
                        source = "<b>Pronatura Veracruz A.C.</b>";
                        summary = "Conjunto de viveros forestales, comunitarios, oficiales (entre otros), que interactúan por medio de la colaboración, capacitación, intercambio de semillas y experiencias para fortalecer la oferta y demanda de plantas forestales y nativas.";
                        layername = "shape_viveros";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_viveros_biodiversidad_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_aproximacion1') {
                        element = rec.get('id');
                        title = "Aproximación 1";
                        shapename = "shape_aproximacion_1_ccl";
                        source = "<b>Pronatura Veracruz A.C.</b>";
                        summary = "No cuenta con resumen.";
                        layername = "shape_aproximacion1";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_aproximacion_1_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_regiones_indigenas') {
                        element = rec.get('id');
                        title = "Regiones indigenas";
                        shapename = "shape_regiones_indigenas_ccl";
                        source = "<b>Eckart Boege</b>&nbsp;El Patrimonio Biocultural de los Pueblos Indígenas de México.";
                        summary = "Para generar las regiones indígenas en México, la CDI y el PNUD definieron a los municipios indígenas como los que tienen 40 por ciento, o más de población en hogares indígenas. La CDI y el PNUD consideraron importante la presencia indígena que, aun teniendo un porcentaje menor del 40 por ciento, es igual o mayor a 5000 personas en una región determinada.";
                        layername = "shape_regiones_indigenas";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_regiones_indigenas_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'treeUpgs') {
                        element = rec.get('id');
                        title = "Unidades Productoras de Germoplasma";
                        shapename = "shape_upgs_ccl";
                        source = "<b>CONAFOR</b>&nbsp;Comisión Nacional Forestal - Coordinación general de conservación y restauración - Gerencia de reforestación -Red mexicana de germoplasma forestal.";
                        summary = "Se entiende por Unidad Productora de Germoplasma Forestal a un área (predio o paraje) donde existe unas especie forestal con características sobresalientes, de la cual se puede colectar germoplasma para producir plantas con fines de reforestación o restauración de ecosistemas forestales.";
                        layername = "shape_upgs";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_upgs_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    }
                    //FIN DE NUEVAS
                    else if (rec.get('id') == 'tree_edafologia') {
                        element = rec.get('id');
                        title = "Edafología";
                        shapename = "shape_edafologia_ccl";
                        source = "Instituto Nacional de investigaciones Forestales y Agropecuarias (INIFAP) - Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (CONABIO), (1995)";
                        summary = "El mapa muestra los diferentes tipos de suelo que se encuentran a nivel nacional a partir de la unión de 32 coberturas: 17 a escala 1:250000 y 15 a 1:1000000. La información que se maneja es: tipo de suelo, textura, fase física, fase química. Los mapas digitalizados (INEGI) se obtuvieron a través del proyecto P147 Enriquecimiento y uso de la base de datos geográficos del INIFAP apoyado por CONABIO (1994).";
                        layername = "shape_edafologia";
                        metadataHtmlUrl = "metadatos/html/edafologia.html";
                        metadataZipUrl = "metadatos/zip/shape_edafologia_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_geologia') {
                        element = rec.get('id');
                        title = "Geología";
                        shapename = "shape_geologia_ccl";
                        source = "<b>INECC</b>";
                        summary = "El continuo nacional del conjunto de datos Vectoriales geológicos, representa las diversas unidades de rocas que afloran en el área, referidas a un tiempo geológico (unidades cronoestratigráficas), así como las estructuras geológicas originadas por los eventos tectónicos, uno de estos son los volcanes de los cuales se identifican los activos e inactivos, se incluye también zonas geotérmicas y se muestran los puntos con recursos minerales (minas).";
                        layername = "shape_geologia";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_geologia_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_climas') {
                        element = rec.get('id');
                        title = "Climas";
                        shapename = "shape_climas_ccl";
                        source = "García, E. - Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (CONABIO), (1998)";
                        summary = "Este mapa representa los diferentes tipos de climas de la República Mexicana de acuerdo a la clasificación de Koppen modificada por García, escala 1:1000000. El trabajo se realizo con el apoyo de la CONABIO, financiado en 1995. Para la elaboración del mapa se tomaron datos del Sistema Meteorológio Nacional, Comisión Federal de Electricidad y Comisión Nacional del Agua. Contando con un total de 3037 estaciones climatológicas.";
                        layername = "shape_climas";
                        metadataHtmlUrl = "metadatos/html/climas.html";
                        metadataZipUrl = "metadatos/zip/shape_climas_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_cuencas') {
                        element = rec.get('id');
                        title = "Cuencas";
                        shapename = "shape_cuencas_ccl";
                        source = "Instituto Nacional de Estadística Geografía e Informática (INEGI), Instituto Nacional de Ecología (INE), Comisión Nacional de Agua (CONAGUA), (2007).";
                        summary = "Mapa que muestra la configuración espacial de las cuencas hidrográficas de México con escala fuente 1: 250000. Se obtuvieron los límites de dichas unidades a partir de criterios topográficos e hidrográficos consensuados y aprobados por las instituciones participantes en su elaboración.";
                        layername = "shape_cuencas";
                        metadataHtmlUrl = "metadatos/html/cuencas_hidrograficas_2007.html";
                        metadataZipUrl = "metadatos/zip/shape_cuencas_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_zonas_funcionales') {
                        element = rec.get('id');
                        title = "Zonas funcionales";
                        shapename = "shape_zonas_funcionales_ccl";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_zonas_funcionales";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_oe_bobos') {
                        element = rec.get('id');
                        title = "OE Bobos";
                        shapename = "shape_oe_bobos_ccl";
                        source = "SEDEMA Veracruz";
                        summary = "Este shapefile muestra las Unidades de Gestión Ambiental (UGA) obtenidas en el Programa de Ordenamiento Ecológico Cuencas de los Rios Bobos y Solteros. <br>Fecha de decreto: 12/Mar/2008.";
                        layername = "shape_oe_bobos";
                        metadataHtmlUrl = "metadatos/html/OE_Bobos.htm";
                        metadataZipUrl = "metadatos/zip/OEBobos.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_oe_coatzacoalcos') {
                        element = rec.get('id');
                        title = "OE Coatzacoalcos";
                        shapename = "shape_oe_coatzacoalcos_ccl";
                        source = "SEDEMA Veracruz";
                        summary = "Este shapefile contiene las Unidades de Gestión Ambiental obtenidas en el Programa de Ordenamiento Ecológico Regional que regula y reglamenta el desarrollo de la región denominada Cuenca Baja del Río Coatzacoalcos.";
                        layername = "shape_oe_coatzacoalcos";
                        metadataHtmlUrl = "metadatos/html/OE_Coatzacoalcos.htm";
                        metadataZipUrl = "metadatos/zip/OECoatzacoalcos.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_oe_tuxpan') {
                        element = rec.get('id');
                        title = "OE Tuxpan";
                        shapename = "shape_oe_tuxpan_ccl";
                        source = "SEDEMA Veracruz";
                        summary = "Este shapefile contiene las Unidades de Gestión Ambiental (UGA) obtenidas en el Programa de Ordenamiento Ecológico Regional que regula y reglamenta el desarrollo de la región denominada Cuenca del Rio Tuxpan.";
                        layername = "shape_oe_tuxpan";
                        metadataHtmlUrl = "metadatos/html/OE_Tuxpan.htm";
                        metadataZipUrl = "metadatos/zip/OETuxpan.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_sarmod') {
                        element = rec.get('id');
                        title = "Cobertura SAR-MOD";
                        shapename = "shape_sarmod_ccl";
                        source = "Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (CONABIO), Coordinación General de Información y Análisis, Dirección General de Proyectos Interinstitucionales y Coordinación del Sistema de Información Espacial para el Soporte de Decisiones sobre Impactos a la Biodiversidad";
                        summary = "Actualización de la cobertura del Subsistema de Alta Resolución (SAR-MOD) con base en los ajustes e inclusión de polígonos de las Áreas Naturales Protegidas Federales de la República Mexicana, CONANP (2017). Se proporciona el dato espacial de la localizción y valor de importancia de los nodos de la malla regular de 5 X 5 km, que sirve de base para el muestreo del Sistema Nacional de Monitoreo de la Biodiversidad (SNMB), recortada para las ANP.";
                        layername = "shape_sarmod";
                        metadataHtmlUrl = "metadatos/html/SARMOD2017.html";
                        metadataZipUrl = "metadatos/zip/SARMOD2017.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_sitios_prior_restauracion2016') {
                        element = rec.get('id');
                        title = "Sitios prioritarios de restauración";
                        shapename = "shape_sitios_prior_restauracion2016_ccl";
                        source = "CONABIO, (2016)";
                        summary = "El deterioro ambiental de los ecosistemas y la pérdida de especies exponen la necesidad de formular e implementar estrategias y acciones de restauración ecológica que complementen los esfuerzos de protección y conservación de la biodiversidad. En México, más de la mitad de la vegetación natural se ha perdido o transformado en campos agrícolas, pastizales cultivados, asentamientos humanos, entre otros usos del suelo (INEGI 2003, 2013) y más de la mitad de los suelos muestran evidencias de algún tipo de degradación (Bollo Manent et al. 2014). Considerando que muchas áreas prioritarias para la conservación de la biodiversidad están afectadas por las actividades humanas, resulta clave contar con una guía espacial explícita para enfocar las acciones de restauración. Para brindar un panorama de las necesidades conservación y restauración más relevantes dentro de una estrategia de desarrollo territorial sustentable, se identificaron los sitios de atención prioritaria (SAP) y los sitios prioritarios de restauración (SPR). Estos estudios se basan en los resultados de los análisis de vacíos y omisiones para la conservación de la biodiversidad terrestre, dulceacuícola y costero-marina. Los SPR fueron diseñados para identificar áreas de alto valor biológico que requieren acciones de restauración para asegurar en el largo plazo la persistencia de su biodiversidad, función ecológica y los servicios ecosistémicos que proveen, y además buscan incrementar la conectividad de ecosistemas y la recuperación de hábitats de las especies más vulnerables. La identificación de SPR se basó en un análisis espacial multi-criterio que consideró 10 criterios agrupados en dos componentes que representaron por un lado la importancia biológica y, por el otro lado, la factibilidad de restauración. Se seleccionó, a partir de los valores integrados del modelo multicriterio, un área equivalente al 15% de la superficie continental del país, los cuales representan los SPR para guiar el cumplimiento de la Meta 15 de Aichi de 'restaurar 15% de las áreas degradadas, 'contribuyendo así a la adaptación al cambio climático y su mitigación, así como a la lucha contra la desertificación'.";
                        layername = "shape_sitios_prior_restauracion2016_ccl";
                        metadataHtmlUrl = "metadatos/html/spr_cw.html";
                        metadataZipUrl = "metadatos/zip/shape_sitios_prior_restauracion2016.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_zonas_restauracion_lgeepa') {
                        element = rec.get('id');
                        title = "Zonas de restauración LGEEPA";
                        shapename = "shape_zonas_restauracion_lgeepa_ccl";
                        source = "DOF 1998";
                        summary = "DECRETO por el que se declaran zonas de restauración ecológica diversas superficies afectadas por los incendios forestales de 1998.";
                        layername = "shape_zonas_restauracion_lgeepa_ccl";
                        metadataHtmlUrl = "metadatos/html/DOF_ZonasRestauracion.html";
                        metadataZipUrl = "metadatos/zip/zonas_restauracion_lgeepa.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    }

                    //NEWS
                    else if (rec.get('id') == 'tree_granizadas') {
                        element = rec.get('id');
                        title = "Granizadas";
                        shapename = "shape_granizadas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/descargas/hidrometeorologicos/Granizadas\">http://datosabiertos.segob.gob.mx/DatosAbiertos/descargas/hidrometeorologicos/Granizadas</a>";
                        summary = "";
                        layername = "shape_granizadas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/descargas/hidrometeorologicos/Granizadas\">http://datosabiertos.segob.gob.mx/DatosAbiertos/descargas/hidrometeorologicos/Granizadas</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_heladas') {
                        element = rec.get('id');
                        title = "Índice de días con heladas por municipio";
                        shapename = "shape_heladas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/heladas\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/heladas</a>";
                        summary = "";
                        layername = "shape_heladas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/heladas\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/heladas</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_nevadas') {
                        element = rec.get('id');
                        title = "Índice de peligro por nevadas por municipio";
                        shapename = "shape_nevadas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Indice\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Indice</a>";
                        summary = "Resumen";
                        layername = "shape_nevadas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Indice\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Indice</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_temp_minima_extrema') {
                        element = rec.get('id');
                        title = "Índice de temperatura mínima extrema por municipio";
                        shapename = "shape_temp_minima_extrema_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/temp\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/temp</a>";
                        summary = "Resumen";
                        layername = "shape_temp_minima_extrema_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/temp\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/temp</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_peligrobajastemperaturas_tempminheladas') {
                        element = rec.get('id');
                        title = "Peligro por bajas temperaturas índices temperatura mínima heladas";
                        shapename = "shape_peligrobajastemperaturas_tempminheladas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Grado/peligro\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Grado/peligro</a>";
                        summary = "Resumen";
                        layername = "shape_peligrobajastemperaturas_tempminheladas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Grado/peligro\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Grado/peligro</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_peligro_ciclonestropicales') {
                        element = rec.get('id');
                        title = "Peligro por ciclones tropicales";
                        shapename = "shape_peligro_ciclonestropicales_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/grado/ciclones\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/grado/ciclones</a>";
                        summary = "Resumen";
                        layername = "shape_peligro_ciclonestropicales_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/grado/ciclones\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/grado/ciclones</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_peligro_sequia') {
                        element = rec.get('id');
                        title = "Peligro por sequía";
                        shapename = "shape_peligro_sequia_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/G_peligro\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/G_peligro</a>";
                        summary = "Resumen";
                        layername = "shape_peligro_sequia_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/G_peligro\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/G_peligro</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    }
                    //NEWS
                    else if (rec.get('id') == 'tree_peligro_sequia_escalante2005') {
                        element = rec.get('id');
                        title = "Peligro por sequía (Escalante Sandoval 2005)";
                        shapename = "shape_peligro_sequia_escalante2005_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Escalante\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Escalante</a>";
                        summary = "Resumen";
                        layername = "shape_peligro_sequia_escalante2005_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Escalante\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Escalante</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_presencia_tornados_mun_mexico') {
                        element = rec.get('id');
                        title = "Presencia de tornados en municipios de México";
                        shapename = "shape_presencia_tornados_mun_mexico_ccl";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_presencia_tornados_mun_mexico_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_riesgo_por_bajastemperaturas') {
                        element = rec.get('id');
                        title = "Riesgo por bajas temperaturas";
                        shapename = "shape_riesgo_por_bajastemperaturas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/gr\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/gr</a>";
                        summary = "Resumen";
                        layername = "shape_riesgo_por_bajastemperaturas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/gr\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/gr</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_riesgo_por_granizo') {
                        element = rec.get('id');
                        title = "Riesgo por granizo";
                        shapename = "shape_riesgo_por_granizo_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/granizo\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/granizo</a>";
                        summary = "Resumen";
                        layername = "shape_riesgo_por_granizo_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/granizo\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/granizo</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_rieso_por_inundacion') {
                        element = rec.get('id');
                        title = "Riesgo por inundación";
                        shapename = "shape_rieso_por_inundacion_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Riesgo\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Riesgo</a>";
                        summary = "Resumen";
                        layername = "shape_rieso_por_inundacion_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Riesgo\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Riesgo</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_rieso_por_tormentaselectricas') {
                        element = rec.get('id');
                        title = "Riesgo por tormentas electricas";
                        shapename = "shape_rieso_por_tormentaselectricas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Peligro\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Peligro</a>";
                        summary = "Resumen";
                        layername = "shape_rieso_por_tormentaselectricas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Peligro\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Peligro</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_sequias_historicas') {
                        element = rec.get('id');
                        title = "Sequías historicas";
                        shapename = "shape_sequias_historicas_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Sequias\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Sequias</a>";
                        summary = "Resumen";
                        layername = "shape_sequias_historicas_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Sequias\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/hidrometeorologicos/Sequias</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_zonas_susceptibles_hundimientosdeslizamientos') {
                        element = rec.get('id');
                        title = "Zonas susceptibles a hundimientos y deslizamientos";
                        shapename = "shape_zonas_susceptibles_hundimientosdeslizamientos_ccl";
                        source = "<a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/geologicos/Zonas\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/geologicos/Zonas</a>";
                        summary = "Resumen";
                        layername = "shape_zonas_susceptibles_hundimientosdeslizamientos_ccl";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.get('title_summary').hide();
                        Ext.get('summary').hide();
                        Ext.getCmp('btn_descargar_metadato').disable();
                        Ext.get('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;"><a href=\"http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/geologicos/Zonas\">http://datosabiertos.segob.gob.mx/DatosAbiertos/Descargas/geologicos/Zonas</a></div>');
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_umas') {
                        element = rec.get('id');
                        title = "UMAS";
                        shapename = "shape_umas_ccl";
                        source = "SEMARNAT";
                        summary = "Las UMA se refieren a los predios e instalaciones registrados que operan de conformidad con un plan de manejo aprobado y dentro de los cuales se da seguimiento permanente al estado del hábitat y de poblaciones o ejemplares que ahí se distribuyen.Surgen de la necesidad de contar con alternativas viables de desarrollo socioeconómico en México, promoviendo la diversificación de actividades productivas en el sector rural mediante el binomio “conservación-aprovechamiento sustentable” de la vida silvestre, a través del uso racional, planificado y ordenado de los recursos naturales y revirtiendo los procesos de deterioro ambiental.Tienen como objetivo general la conservación del hábitat natural, poblaciones y ejemplares de especies silvestres. Pueden tener objetivos específicos de restauración, protección, mantenimiento, recuperación, reproducción, repoblación, reintroducción, investigación, rescate, resguardo, rehabilitación, exhibición, recreación, educación ambiental y aprovechamiento sustentable.";
                        layername = "shape_umas";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2005') {
                        element = rec.get('id');
                        title = "PSA 2005";
                        shapename = "shape_pago_servicios_ambientales_ccl_2005";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2005";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2006') {
                        element = rec.get('id');
                        title = "PSA 2006";
                        shapename = "shape_pago_servicios_ambientales_ccl_2006";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2006";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2007') {
                        element = rec.get('id');
                        title = "PSA 2007";
                        shapename = "shape_pago_servicios_ambientales_ccl_2007";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2007";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2008') {
                        element = rec.get('id');
                        title = "PSA 2008";
                        shapename = "shape_pago_servicios_ambientales_ccl_2008";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2008";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2009') {
                        element = rec.get('id');
                        title = "PSA 2009";
                        shapename = "shape_pago_servicios_ambientales_ccl_2009";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2009";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2010') {
                        element = rec.get('id');
                        title = "PSA 2010";
                        shapename = "shape_pago_servicios_ambientales_ccl_2010";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2010";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2011') {
                        element = rec.get('id');
                        title = "PSA 2011";
                        shapename = "shape_pago_servicios_ambientales_ccl_2011";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2011";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2012') {
                        element = rec.get('id');
                        title = "PSA 2012";
                        shapename = "shape_pago_servicios_ambientales_ccl_2012";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2012";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2013') {
                        element = rec.get('id');
                        title = "PSA 2013";
                        shapename = "shape_pago_servicios_ambientales_ccl_2013";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2013";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_psa2014') {
                        element = rec.get('id');
                        title = "PSA 2014";
                        shapename = "shape_pago_servicios_ambientales_ccl_2014";
                        source = "Fuente";
                        summary = "Resumen";
                        layername = "shape_psa2014";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    } else if (rec.get('id') == 'tree_precTotAnual') {
                        element = rec.get('id');
                        title = "Precipitación total anual";
                        shapename = "shape_precipitacion_total_anual_ccl";
                        source = "García, E. - CONABIO, (1998)";
                        summary = "El trazo de isoyetas se realizó tomando en cuenta el relieve, la dirección principal del viento y los efectos de barrera Montañosa como son: el efecto de sombra pluviométrica, el de embalse y el descenso y ascenso orográfico. En la construcción de las isoyetas quedaron como líneas maestras las de 50, 100, 200, 400, 600, 1000, 1500, 2000, 3000 y 4000 mm y como intermedias las de 300, 500, 800, 1200, 1800, 2500, 3500, y 4500 mm anuales. Lo anterior considerando que la precipitación aumenta en proporción geométrica.";
                        layername = "shape_precTotAnual";
                        metadataHtmlUrl = "metadatos/html/precipitacion_total_anual.html";
                        metadataZipUrl = "metadatos/zip/shape_precipitacion_total_anual_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_anpFederales') {
                        element = rec.get('id');
                        title = "ANP (federales)";
                        shapename = "shape_anps_federales_ccl";
                        source = "Comisión Nacional de Áreas Naturales Protegidas, Dirección de Evaluación y Seguimiento, Subdirección Encargada de la Coordinación de Geomática.";
                        summary = "Proporcionar datos espaciales de las Áreas Naturales Protegidas Federales de la República Mexicana.";
                        layername = "shape_anpFederales";
                        metadataHtmlUrl = "metadatos/html/anps_federales.html";
                        metadataZipUrl = "metadatos/zip/shape_anps_federales_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_anpEstatales') {
                        element = rec.get('id');
                        title = "ANP (estatales)";
                        shapename = "shape_anps_estatales_ccl";
                        source = "Bezaury-Creel J.E., J. Fco. Torres, L. M. Ochoa-Ochoa, Marco Castro-Campos, N. Moreno. (2009)";
                        summary = "Base de datos geográfica que integra la representación cartográfica de las áreas naturales protegidas (ANP), a nivel estatal y del Distrito Federal, decretadas hasta la fecha en la República Mexicana.";
                        layername = "shape_anpEstatales";
                        metadataHtmlUrl = "metadatos/html/anps_estatales.html";
                        metadataZipUrl = "metadatos/zip/shape_anps_estatales_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_anpMunicipales') {
                        element = rec.get('id');
                        title = "ANP (municipales)";
                        shapename = "shape_anps_municipales_ccl";
                        source = "Bezaury-Creel J.E., J. Fco. Torres, L. M. Ochoa-Ochoa, Marco Castro-Campos, N. Moreno. (2009)";
                        summary = "Base de Datos Geográfica de Áreas Naturales Protegidas Municipales de México - Versión 2.0.";
                        layername = "shape_anpMunicipales";
                        metadataHtmlUrl = "metadatos/html/anps_municipales.html";
                        metadataZipUrl = "metadatos/zip/shape_anps_municipales_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_aicas') {
                        element = rec.get('id');
                        title = "AICA";
                        shapename = "shape_aicas_ccl";
                        source = "Sección Mexicana del Consejo Internacional para la Preservación de las Aves CIPAMEX Comisión Nacional para el Conocimiento y Uso de la Biodiversidad CONABIO, (1999)";
                        summary = "Se presenta la delimitación de las Áreas de Importancia para la Conservación de las Aves (AICAS) en escala 1:250000. La delimitación se basó en la experiencia de alrededor de 40 especialistas, quienes durante un taller celebrado en 1996 dibujaron las áreas sobre un mapa en escala 1:4000000. Posteriormente, las 170 áreas identificadas se difundieron entre otros especialistas, invitando a más persona a participar, hasta conjuntar 193 áreas en 1997. Finalmente, entre 1998 y 1999, los especialistas regionales revisaron las 193 áreas y propusieron de manera definitiva 218 áreas sobre un mapa en escala 1:250000. Los límites de las áreas se vaciaron en cartas topográficas 1:250000, para posteriormente ser digitalizadas.";
                        layername = "shape_aicas";
                        metadataHtmlUrl = "metadatos/html/aicas.html";
                        metadataZipUrl = "metadatos/zip/shape_aicas_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_regionesTerrestres') {
                        element = rec.get('id');
                        title = "Regiones terrestres prioritarias";
                        shapename = "shape_regiones_terre_prioritarias_ccl";
                        source = "Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (Conabio), (2004)";
                        summary = "Este mapa tiene como objetivo representar a través de las regiones (un total de 152); unidades estables desde el punto de vista ambiental en la parte continental del territorio nacional, en donde se destaque la presencia de una riqueza ecosistémica así como una integridad biológica significativa. El mapa se encuentra a escala 1:1000000. Las regiones cubren un total de 515,558 km2 de superficie.";
                        layername = "shape_regionesTerrestres";
                        metadataHtmlUrl = "metadatos/html/regiones_terrestres_prioritarias.html";
                        metadataZipUrl = "metadatos/zip/shape_regiones_terrestres_prioritarias_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_regionesHidrologicas') {
                        element = rec.get('id');
                        title = "Regiones hidrológicas prioritarias";
                        shapename = "shape_regiones_hidro_prioritarias_ccl";
                        source = "Arriaga, L., V. Aguilar y J. Alcocer. (2002)";
                        summary = "Este mapa presenta las Regiones Hidrológicas Prioritarias de México (110 áreas). En octubre de 1997, la Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (CONABIO) inició el Programa de Regiones Prioritarias Marinas y Limnológicas de México, con el apoyo de las agencias The David and Lucile Packard Foundation (PACKARD), la Agencia Internacional Para el Desarrollo de los Estados Unidos de América (USAID), el Fondo Mexicano para la Conservación de la Naturaleza (FMCN) y el Fondo Mundial para la Naturaleza (WWF).";
                        layername = "shape_regionesHidrologicas";
                        metadataHtmlUrl = "metadatos/html/regiones_hidrologicas_prioritarias.html";
                        metadataZipUrl = "metadatos/zip/shape_regiones_hidrologicas_prioritarias_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_sitiosPrioritarios') {
                        element = rec.get('id');
                        title = "Sitios prioritarios terrestres";
                        shapename = "shape_sitios_prioritarios_terrestres_ccl";
                        source = "Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (CONABIO), Comisión Nacional de Áreas Naturales Protegidas (CONANP), The Nature Conservancy - Programa México (TNC), Pronatura. (2007)";
                        summary = "La planificación de la conservación de la biodiversidad terrestre es fundamental ya que México pertenece a uno de los países llamados megadiversos. La excepcional biodiversidad de México se expresa en la heterogeneidad de sus paisajes, ecosistemas y numerosas especies que se distribuyen en todo su territorio, albergando 12% de los organismos vivos del planeta. Sin embargo, esta biodiversidad se encuentra altamente amenazada por las altas tasas de deforestación y degradación ambiental. Aunado a esto, el tráfico ilegal de especies, la contaminación y el establecimiento de especies exóticas invasoras incrementan el riesgo de extinción de un gran número de especies. Lo anterior indica que el país enfrenta grandes retos de conservación por lo que sin duda es necesaria una planeación a múltiples escalas para representar todos los elementos de la biodiversidad. La presente cartografía representa los primeros resultados principales de la identificación de sitios prioritarios para la conservación de la biodiversidad terrestre. Se evaluó el nivel de protección con unidades de análisis de 256 km2 y datos de especies, comunidades y los principales factores que las amenazan. Se identificaron sitios de extrema, alta y media prioridad. Se identificaron 1093 unidades de media prioridad (frecuencia de selección 90-99% del ejercicio de priorización de acuerdo a las metas establecidas en los talleres), 1145 unidades de alta prioridad (frecuencia de selección 100% del ejercicio de priorización de acuerdo a las metas establecidas en los talleres) y 176 de extrema prioridad (frecuencia de selección 100%; coincidentes en dos ejercicio de priorización, el primero de acuerdo a las metas establecidas en los talleres y el segundo ejercicio con metas reducidas para los tipos de vegetación primaria y secundaria). El proceso de validación, de la información, a otras escalas y con la ayuda de especialistas sigue en marcha por lo que la información aquí mostrada no puede ser considerada como definitiva. Las instituciones participantes se han asegurado de suministrar información actualizada y correcta al momento de su publicación y de acuerdo a la metodología propuesta por especialistas en el tema que participaron en los talleres de expertos. Se autoriza el uso, reproducción y distribución de este producto con fines no comerciales siempre y cuando se cite.";
                        layername = "shape_sitiosPrioritarios";
                        metadataHtmlUrl = "metadatos/html/sitios_prioritarios_terrestres.html";
                        metadataZipUrl = "metadatos/zip/shape_sitios_prioritarios_terrestres_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_sitiosRamsar') {
                        element = rec.get('id');
                        title = "Sitios Ramsar";
                        shapename = "shape_ramsar_ccl";
                        source = "Comisión Nacional de Áreas Naturales Protegidas, Dirección de Evaluación y Seguimiento, Subdirección Encargada de la Coordinación de Geomática.";
                        summary = "Proporcionar datos espaciales de los Sitios Ramsar de la República Mexicana.";
                        layername = "shape_sitiosRamsar";
                        metadataHtmlUrl = "metadatos/html/ramsar.html";
                        metadataZipUrl = "metadatos/zip/shape_ramsar_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                    } else if (rec.get('id') == 'tree_areaTrabajo') {
                        element = rec.get('id');
                        title = "Nueva área de trabajo";
                        shapename = "shape_nueva_area_trabajopronatura_ccl";
                        source = "<b>Pronatura Veracruz A.C.</b>";
                        summary = "Muestra la nueva área de trabajo de Pronatura Veracruz A.C.";
                        layername = "shape_areaTrabajo";
                        metadataHtmlUrl = "";
                        metadataZipUrl = "metadatos/zip/shape_nueva_area_trabajo_ccl.zip";
                        updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl);
                        Ext.getCmp('metadato_mensaje_sinmetadato').show();
                    }
                }
            }
        });

        //TESTS APPEND CHILDS
        arbolTemas.getRootNode().appendChild({
            text: "Acervo",
            iconCls: 'server_icon',
            children: [{
                    text: "Zonas protegidas",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: true,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "APC",
                            id: 'tree_ubi',
                            layer: vectorPuntos,
                            expanded: false,
                            checked: true,
                            cls: 'x-tree-noicon',
                            children: [{
                                plugins: ['gx_layer'],
                                text: "vectorPoligonos",
                                id: 'vectorPoligonos',
                                layer: vectorPoligonos,
                                checked: true,
                                cls: 'x-tree-noicon'
                            }]
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "ADVC",
                            id: 'tree_advc',
                            layer: vectorPoligonosADVC,
                            expanded: false,
                            checked: true,
                            cls: 'x-tree-noicon'
                        }
                    ]
                },
                {
                    text: "Cartas topográficas",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: true,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Corrientes de agua",
                            id: 'tree_corrientes_agua',
                            layer: shape_corrientes_agua,
                            checked: true,
                            children: [],
                            onCheckChange: true,
                            cls: 'x-tree-noicon'
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Cuerpos de agua",
                            id: 'tree_cuerpos_agua',
                            layer: shape_cuerpos_agua,
                            checked: true,
                            children: [],
                            onCheckChange: true,
                            cls: 'x-tree-noicon'
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Localidades",
                            id: 'tree_localidades',
                            layer: shape_localidades,
                            checked: true,
                            children: [],
                            onCheckChange: true,
                            cls: 'x-tree-noicon'
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Vialidades",
                            id: 'tree_vialidades',
                            layer: shape_vialidades,
                            checked: true,
                            children: [],
                            onCheckChange: true,
                            cls: 'x-tree-noicon'
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Estados",
                            id: 'tree_estados',
                            layer: shape_estados,
                            checked: true,
                            children: [],
                            onCheckChange: true,
                            cls: 'x-tree-noicon'
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Municipios",
                            id: 'tree_municipios',
                            layer: shape_municipios,
                            checked: true,
                            children: [],
                            onCheckChange: true,
                            cls: 'x-tree-noicon'
                        }
                    ]
                },
                {
                    text: "Información física y biológica",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Edafología",
                            id: 'tree_edafologia',
                            layer: shape_edafologia,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Geología",
                            id: 'tree_geologia',
                            layer: shape_geologia,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Climas",
                            id: 'tree_climas',
                            layer: shape_climas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Cuencas",
                            id: 'tree_cuencas',
                            layer: shape_cuencas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Precipitación total anual",
                            id: 'tree_precTotAnual',
                            layer: shape_precTotAnual,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Uso de suelo y vegetación V",
                            id: 'tree_vegetacion',
                            layer: shape_vegetacion,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Uso de suelo y vegetación VI",
                            id: 'tree_vegetacion_s6',
                            layer: shape_vegetacion_s6,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Zonas funcionales",
                            id: 'tree_zonas_funcionales',
                            layer: shape_zonas_funcionales,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Áreas Protegidas",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "ANP (federales)",
                            id: 'tree_anpFederales',
                            layer: shape_anpFederales,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "ANP (estatales)",
                            id: 'tree_anpEstatales',
                            layer: shape_anpEstatales,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "ANP (municipales)",
                            id: 'tree_anpMunicipales',
                            layer: shape_anpMunicipales,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Ordenamientos ecologicos",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "OE Bobos",
                            id: 'tree_oe_bobos',
                            layer: shape_oe_bobos,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "OE Coatzacoalcos",
                            id: 'tree_oe_coatzacoalcos',
                            layer: shape_oe_coatzacoalcos,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "OE Tuxpan",
                            id: 'tree_oe_tuxpan',
                            layer: shape_oe_tuxpan,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Subsistema de Alta Resolución (SAR-MOD)",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                        plugins: ['gx_layer'],
                        text: "Cobertura SAR-MOD",
                        id: 'tree_sarmod',
                        layer: shape_sarmod,
                        cls: 'x-tree-noicon',
                        onCheckChange: true,
                        leaf: true
                    }]
                },
                {
                    text: "Información cultural",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Regiones indigenas",
                            id: 'tree_regiones_indigenas',
                            layer: shape_regiones_indigenas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Zonas arqueológicas",
                            id: 'tree_zonas_arqueologicas',
                            layer: shape_zonas_arqueologicas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Figuras prioritarias",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "AICA",
                            id: 'tree_aicas',
                            layer: shape_aicas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Regiones terrestres prioritarias",
                            id: 'tree_regionesTerrestres',
                            layer: shape_regionesTerrestres,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Regiones hidrológicas prioritarias",
                            id: 'tree_regionesHidrologicas',
                            layer: shape_regionesHidrologicas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Sitios prioritarios terrestres",
                            id: 'tree_sitiosPrioritarios',
                            layer: shape_sitiosPrioritarios,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Sitios Ramsar",
                            id: 'tree_sitiosRamsar',
                            layer: shape_sitiosRamsar,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Sitios Cero Extinción de anfibios",
                            id: 'tree_zero_extinction',
                            layer: shape_zero_extinction,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: 'Pago por servicios ambientales',
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "2005",
                            id: "tree_psa2005",
                            layer: shape_psa2005,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2006",
                            id: "tree_psa2006",
                            layer: shape_psa2006,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2007",
                            id: "tree_psa2007",
                            layer: shape_psa2007,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2008",
                            id: "tree_psa2008",
                            layer: shape_psa2008,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2009",
                            id: "tree_psa2009",
                            layer: shape_psa2009,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2010",
                            id: "tree_psa2010",
                            layer: shape_psa2010,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2011",
                            id: "tree_psa2011",
                            layer: shape_psa2011,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2012",
                            id: "tree_psa2012",
                            layer: shape_psa2012,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2013",
                            id: "tree_psa2013",
                            layer: shape_psa2013,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "2014",
                            id: "tree_psa2014",
                            layer: shape_psa2014,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },

                    ]
                },
                {
                    text: "Atributos del desarrollo sustentable",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Unidades Productoras de Germoplasma",
                            id: 'treeUpgs',
                            layer: shape_upgs,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "UMAS",
                            id: 'tree_umas',
                            layer: shape_umas,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Zonas de restauración",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Sitios prioritarios de restauración",
                            id: 'tree_sitios_prior_restauracion2016',
                            layer: shape_sitios_prior_restauracion2016_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Zonas de restauración LGEEPA",
                            id: 'tree_zonas_restauracion_lgeepa',
                            layer: shape_zonas_restauracion_lgeepa_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Riesgos y peligros",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Granizadas",
                            id: 'tree_granizadas',
                            layer: shape_granizadas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Índice de días con heladas por municipio",
                            id: 'tree_heladas',
                            layer: shape_heladas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Índice de peligro por nevadas por municipio",
                            id: 'tree_nevadas',
                            layer: shape_nevadas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Índice de temperatura mínima extrema por municipio",
                            id: 'tree_temp_minima_extrema',
                            layer: shape_temp_minima_extrema_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Peligro por bajas temperaturas índices temperatura mínima heladas",
                            id: 'tree_peligrobajastemperaturas_tempminheladas',
                            layer: shape_peligrobajastemperaturas_tempminheladas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Peligro por ciclones tropicales",
                            id: 'tree_peligro_ciclonestropicales',
                            layer: shape_peligro_ciclonestropicales_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Peligro por sequía",
                            id: 'tree_peligro_sequia',
                            layer: shape_peligro_sequia_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        //new
                        {
                            plugins: ['gx_layer'],
                            text: "Peligro por sequía (Escalante Sandoval 2005)",
                            id: 'tree_peligro_sequia_escalante2005',
                            layer: shape_peligro_sequia_escalante2005_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Riesgo por bajas temperaturas",
                            id: 'tree_riesgo_por_bajastemperaturas',
                            layer: shape_riesgo_por_bajastemperaturas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Riesgo por granizo",
                            id: 'tree_riesgo_por_granizo',
                            layer: shape_riesgo_por_granizo_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Riesgo por inundación",
                            id: 'tree_rieso_por_inundacion',
                            layer: shape_rieso_por_inundacion_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Riesgo por tormentas electricas",
                            id: 'tree_rieso_por_tormentaselectricas',
                            layer: shape_rieso_por_tormentaselectricas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Sequías historicas",
                            id: 'tree_sequias_historicas',
                            layer: shape_sequias_historicas_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Zonas susceptibles a hundimientos y deslizamientos",
                            id: 'tree_zonas_susceptibles_hundimientosdeslizamientos',
                            layer: shape_zonas_susceptibles_hundimientosdeslizamientos_ccl,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                },
                {
                    text: "Información Pronatura",
                    iconCls: 'layers_icon',
                    leaf: false,
                    expanded: false,
                    children: [{
                            plugins: ['gx_layer'],
                            text: "Nueva área de trabajo",
                            id: 'tree_areaTrabajo',
                            layer: shape_areaTrabajo,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        },
                        {
                            plugins: ['gx_layer'],
                            text: "Red de viveros de biodiversidad",
                            id: 'tree_viveros',
                            layer: shape_viveros,
                            cls: 'x-tree-noicon',
                            onCheckChange: true,
                            leaf: true
                        }
                    ]
                }

            ],
            expanded: true
        });

        //CREAMOS LA VENTANA DE "ACERCA DE PRONATURA VERACRUZ"
        var acercaPronatura = Ext.create('Ext.window.Window', {
            title: 'Acerca de Pronatura Veracruz A.C.',
            height: 480,
            width: 640,
            modal: true,
            draggable: false,
            layout: 'fit',
            closeAction: 'hide',
            iconCls: 'prona_icon',
            items: [{
                xtype: 'component',
                autoEl: {
                    tag: 'iframe',
                    src: 'resources/acercaPronatura.html'
                }
            }, ],
            listeners: {
                beforeclose: function () {
                    var tabMapa = Ext.getCmp('tabMapa');
                    panelCentro.setActiveTab(tabMapa);
                }
            }
        });

        //CREAMOS LA STORE PARA LAS OPCIONES DE PLAN DE MANEJO
        var storePlanManejo = Ext.create('Ext.data.Store', {
            fields: ['cuenta_con_plan_manejo'],
            data: [{
                    "cuenta_con_plan_manejo": "Si"
                },
                {
                    "cuenta_con_plan_manejo": "No"
                }
            ]
        });

        //CREAMOS LA STORE PARA LAS OPCIONES DE TENENCIA
        var storeTenencia = Ext.create('Ext.data.Store', {
            fields: ['tenencia'],
            data: [{
                    "tenencia": "Ejido"
                },
                {
                    "tenencia": "Privado"
                }
            ]
        });

        //CREAMOS LA VENTANA DE BUSQUEDA AVANZADA
        var busqueda_avanzada = Ext.create('Ext.window.Window', {
            title: 'Busqueda avanzada',
            height: 465,
            width: 400,
            layout: 'fit',
            resizable: false,
            minimizable: true,
            draggable: false,
            closable: true,
            closeAction: 'hide',
            iconCls: 'engrane_icon',
            listeners: {
                minimize: function () {
                    this.hide();
                },
                close: function () {
                    Ext.getCmp('ba_municipio').reset();
                    Ext.getCmp('ba_localidad').reset();
                    Ext.getCmp('ba_nombre_apc').reset();
                    Ext.getCmp('ba_vigencia').reset();
                    Ext.getCmp('ba_plan_manejo_principal').reset();
                    Ext.getCmp('ba_tenencia_principal').reset();
                    Ext.getCmp('ba_fecha_certificacion_principal').reset();

                }
            },
            items: [{
                xtype: 'form',
                bodyStyle: 'padding:5px 5px 5px 5px',
                layout: 'column',
                items: [

                    {
                        xtype: 'fieldset',
                        style: 'padding-top:5px;',
                        border: 0,
                        items: [{
                                layout: 'column',
                                border: 0,
                                items: [{
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'textfield',
                                            id: 'ba_municipio',
                                            fieldLabel: 'Municipio',
                                            name: 'ba_municipio',
                                            allowBlank: true,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'textfield',
                                            id: 'ba_localidad',
                                            fieldLabel: 'Localidad',
                                            name: 'ba_localidad',
                                            allowBlank: true,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'textfield',
                                            id: 'ba_nombre_apc',
                                            fieldLabel: 'Nombre APC',
                                            name: 'ba_nombre_apc',
                                            allowBlank: true,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'numberfield',
                                            id: 'ba_vigencia',
                                            fieldLabel: 'Vigencia',
                                            name: 'ba_vigencia',
                                            allowBlank: true,
                                            maxValue: 99,
                                            minValue: 1,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'combobox',
                                            id: 'ba_plan_manejo_principal',
                                            fieldLabel: 'Cuenta con plan de manejo',
                                            name: 'ba_plan_manejo_principal',
                                            allowBlank: true,
                                            editable: false,
                                            store: storePlanManejo,
                                            displayField: 'cuenta_con_plan_manejo',
                                            valueField: 'cuenta_con_plan_manejo',
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'combobox',
                                            id: 'ba_tenencia_principal',
                                            fieldLabel: 'Tenencia',
                                            name: 'ba_tenencia_principal',
                                            allowBlank: true,
                                            editable: false,
                                            store: storeTenencia,
                                            displayField: 'tenencia',
                                            valueField: 'tenencia',
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        style: 'padding-top:5px; padding-bottom:5px;',
                                        items: {
                                            xtype: 'label',
                                            text: 'Fecha de certificación',
                                            style: 'font-weight:bold; color:dimgray; text-decoration:underline; padding-top:20px; padding-bottom:20px;',
                                        }
                                    },

                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'datefield',
                                            id: 'ba_fecha_certificacion_principal',
                                            fieldLabel: 'Seleccionar',
                                            name: 'ba_fecha_certificacion_principal',
                                            allowBlank: true,
                                            minValue: '2012-01-01',
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            editable: false,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                },
                                                select: function (field, value, eOpts) {
                                                    Ext.getCmp('btn_operador_rango').enable();
                                                    var dia = value.getDate();
                                                    var mes = value.getMonth();
                                                    var anyo = value.getFullYear();
                                                    dia += 1;
                                                    switch (dia) {
                                                        case 1:
                                                            dia = "01";
                                                            break;
                                                        case 2:
                                                            dia = "02";
                                                            break;
                                                        case 3:
                                                            dia = "03";
                                                            break;
                                                        case 4:
                                                            dia = "04";
                                                            break;
                                                        case 5:
                                                            dia = "05";
                                                            break;
                                                        case 6:
                                                            dia = "06";
                                                            break;
                                                        case 7:
                                                            dia = "07";
                                                            break;
                                                        case 8:
                                                            dia = "08";
                                                            break;
                                                        case 9:
                                                            dia = "09";
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    switch (mes) {
                                                        case 0:
                                                            mes = "01";
                                                            break;
                                                        case 1:
                                                            mes = "02";
                                                            break;
                                                        case 2:
                                                            mes = "03";
                                                            break;
                                                        case 3:
                                                            mes = "04";
                                                            break;
                                                        case 4:
                                                            mes = "05";
                                                            break;
                                                        case 5:
                                                            mes = "06";
                                                            break;
                                                        case 6:
                                                            mes = "07";
                                                            break;
                                                        case 7:
                                                            mes = "08";
                                                            break;
                                                        case 8:
                                                            mes = "09";
                                                            break;
                                                        case 9:
                                                            mes = "10";
                                                            break;
                                                        case 10:
                                                            mes = "11";
                                                            break;
                                                        case 11:
                                                            mes = "12";
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    /*console.warn("Valor: "+ value);
                                                    console.warn("Día siguiente: "+ dia);
                                                    console.warn("Mes: "+ mes);
                                                    console.warn("Año: "+ anyo);*/
                                                    Ext.getCmp('ba_fecha_certificacion_principal2').setMinValue(anyo + "-" + mes + "-" + dia);
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            style: 'margin-top:0px; margin-left:5px;',
                                            xtype: 'splitbutton',
                                            id: 'split_operador_fc',
                                            text: 'Igual',
                                            menu: [{
                                                    text: 'Igual',
                                                    id: 'btn_operador_igual',
                                                    disabled: true,
                                                    handler: function () {
                                                        var split_operador_fc = Ext.getCmp('split_operador_fc');
                                                        split_operador_fc.setText('Igual');
                                                        this.disable();
                                                        Ext.getCmp('btn_operador_mayor').enable();
                                                        Ext.getCmp('btn_operador_menor').enable();
                                                        Ext.getCmp('btn_operador_rango').enable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal').setFieldLabel('Seleccionar');
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').disable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').reset();
                                                    }
                                                },
                                                {
                                                    text: 'Mayor',
                                                    id: 'btn_operador_mayor',
                                                    handler: function () {
                                                        var split_operador_fc = Ext.getCmp('split_operador_fc');
                                                        split_operador_fc.setText('Mayor');
                                                        this.disable();
                                                        Ext.getCmp('btn_operador_igual').enable();
                                                        Ext.getCmp('btn_operador_menor').enable();
                                                        Ext.getCmp('btn_operador_rango').enable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal').setFieldLabel('Seleccionar');
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').disable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').reset();
                                                    }
                                                },
                                                {
                                                    text: 'Menor',
                                                    id: 'btn_operador_menor',
                                                    handler: function () {
                                                        var split_operador_fc = Ext.getCmp('split_operador_fc');
                                                        split_operador_fc.setText('Menor');
                                                        this.disable();
                                                        Ext.getCmp('btn_operador_igual').enable();
                                                        Ext.getCmp('btn_operador_mayor').enable();
                                                        Ext.getCmp('btn_operador_rango').enable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal').setFieldLabel('Seleccionar');
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').disable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').reset();
                                                    }
                                                },
                                                {
                                                    text: 'Rango',
                                                    id: 'btn_operador_rango',
                                                    disabled: true,
                                                    handler: function () {
                                                        var split_operador_fc = Ext.getCmp('split_operador_fc');
                                                        split_operador_fc.setText('Rango');
                                                        this.disable();
                                                        Ext.getCmp('btn_operador_igual').enable();
                                                        Ext.getCmp('btn_operador_mayor').enable();
                                                        Ext.getCmp('btn_operador_menor').enable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal').setFieldLabel('De');
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').enable();
                                                        Ext.getCmp('ba_fecha_certificacion_principal2').reset();
                                                    }
                                                }

                                            ]
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'datefield',
                                            disabled: true,
                                            id: 'ba_fecha_certificacion_principal2',
                                            fieldLabel: 'Hasta',
                                            name: 'ba_fecha_certificacion_principal',
                                            allowBlank: true,
                                            maxValue: new Date(),
                                            format: 'Y-m-d',
                                            editable: false,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        style: 'padding-top:5px; padding-bottom:5px;',
                                        items: {
                                            xtype: 'label',
                                            text: 'Altitud (metros sobre el nivel del mar)',
                                            style: 'font-weight:bold; color:dimgray; text-decoration:underline; padding-top:20px; padding-bottom:20px;',
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'numberfield',
                                            id: 'ba_altitud_inicial',
                                            fieldLabel: 'De',
                                            name: 'ba_altitud_inicial',
                                            allowBlank: true,
                                            minValue: -12,
                                            maxValue: 2761,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    //TECLAS NUMERICAS: 48 al 57
                                                    if (e.keyCode == 48 || e.keyCode == 49 || e.keyCode == 50 || e.keyCode == 51 || e.keyCode == 52 || e.keyCode == 53 || e.keyCode == 54 || e.keyCode == 55 || e.keyCode == 56 || e.keyCode == 57) {
                                                        Ext.getCmp('ba_altitud_final').enable();
                                                    }
                                                },
                                                change: function (cmp, newValue, oldValue, eOpts) {
                                                    if (newValue == null) {
                                                        Ext.getCmp('ba_altitud_final').reset();
                                                        Ext.getCmp('ba_altitud_final').disable();
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        columnWidth: 0.5,
                                        border: 0,
                                        items: {
                                            xtype: 'numberfield',
                                            disabled: true,
                                            id: 'ba_altitud_final',
                                            fieldLabel: 'Hasta',
                                            name: 'ba_altitud_final',
                                            allowBlank: true,
                                            maxValue: 2762,
                                            enableKeyEvents: true,
                                            listeners: {
                                                keypress: function (name, e, eOpts) {
                                                    if (e.keyCode == 13) {
                                                        var ba_btnBuscar = Ext.getCmp('ba_btnBuscar');
                                                        ba_btnBuscar.fireEvent('click');
                                                    }
                                                },
                                                change: function (cmp, newValue, oldValue, eOpts) {
                                                    var altitud_inicial = Ext.getCmp('ba_altitud_inicial').getRawValue();
                                                    cmp.setMinValue(+altitud_inicial + 1);
                                                }
                                            }
                                        }
                                    }





                                ]
                            }

                        ]
                    }

                ],
                buttons: [{
                        xtype: 'splitbutton',
                        id: 'split_clean_form',
                        text: 'Limpiar: municipio',
                        iconCls: 'limpiar_icon',
                        listeners: {
                            click: function () {
                                var texto = this.getText();
                                if (texto == 'Limpiar: municipio')
                                    Ext.getCmp('ba_municipio').reset();
                                else if (texto == 'Limpiar: localidad')
                                    Ext.getCmp('ba_localidad').reset();
                                else if (texto == 'Limpiar: nombre APC')
                                    Ext.getCmp('ba_nombre_apc').reset();
                                else if (texto == 'Limpiar: vigencia')
                                    Ext.getCmp('ba_vigencia').reset();
                                else if (texto == 'Limpiar: plan de manejo')
                                    Ext.getCmp('ba_plan_manejo_principal').reset();
                                else if (texto == 'Limpiar: tenencia')
                                    Ext.getCmp('ba_tenencia_principal').reset();
                                else if (texto == 'Limpiar: fecha de certificación')
                                    Ext.getCmp('ba_fecha_certificacion_principal').reset();
                                else if (texto == 'Limpiar: todos')
                                    this.up('form').getForm().reset();
                            }
                        },
                        menu: [{
                                text: 'Municipio',
                                id: 'btn_clean_municipio',
                                disabled: true,
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: municipio');
                                    this.disable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Localidad',
                                id: 'btn_clean_localidad',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: localidad');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Nombre APC',
                                id: 'btn_clean_nombre_apc',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: nombre APC');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Vigencia',
                                id: 'btn_clean_vigencia',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: vigencia');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Plan de manejo',
                                id: 'btn_clean_plan_manejo',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: plan de manejo');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Tenencia',
                                id: 'btn_clean_tenencia',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: tenencia');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Fecha de certificación',
                                id: 'btn_clean_fecha_certificacion',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: fecha de certificación');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_todos').enable();
                                }
                            },
                            {
                                text: 'Todos',
                                id: 'btn_clean_todos',
                                iconCls: 'limpiar_icon',
                                handler: function () {
                                    split_clean_form = Ext.getCmp('split_clean_form');
                                    split_clean_form.setText('Limpiar: todos');
                                    this.disable();
                                    Ext.getCmp('btn_clean_municipio').enable();
                                    Ext.getCmp('btn_clean_localidad').enable();
                                    Ext.getCmp('btn_clean_nombre_apc').enable();
                                    Ext.getCmp('btn_clean_vigencia').enable();
                                    Ext.getCmp('btn_clean_plan_manejo').enable();
                                    Ext.getCmp('btn_clean_tenencia').enable();
                                    Ext.getCmp('btn_clean_fecha_certificacion').enable();
                                }
                            }
                        ]
                    },
                    {
                        text: 'Buscar',
                        id: 'ba_btnBuscar',
                        listeners: {
                            click: function () {
                                //RECUPERAMOS LOS VALORES DE LOS TEXFIELDS
                                var ba_municipio = Ext.getCmp('ba_municipio').value;
                                var ba_localidad = Ext.getCmp('ba_localidad').value;
                                var ba_nombre_apc = Ext.getCmp('ba_nombre_apc').value;
                                var ba_vigencia = Ext.getCmp('ba_vigencia').getRawValue();
                                var ba_plan_manejo_principal = Ext.getCmp('ba_plan_manejo_principal').value;
                                var ba_tenencia_principal = Ext.getCmp('ba_tenencia_principal').value;
                                var ba_fecha_certificacion_principal = Ext.getCmp('ba_fecha_certificacion_principal').getRawValue();
                                var ba_fecha_certificacion_principal2 = Ext.getCmp('ba_fecha_certificacion_principal2').getRawValue();
                                var ba_altitud_inicial = Ext.getCmp('ba_altitud_inicial').getRawValue();
                                var ba_altitud_final = Ext.getCmp('ba_altitud_final').getRawValue();

                                //SI TODOS LOS CAMPOS ESTAN VACIOS QUITAMOS TODOS LOS FILTROS DEL GRID Y LO RECARGAMOS
                                if (
                                    (ba_municipio == null || ba_municipio == '' || ba_municipio == 'undefined' || typeof ba_municipio == 'undefined') &&
                                    (ba_localidad == null || ba_localidad == '' || ba_localidad == 'undefined' || typeof ba_localidad == 'undefined') &&
                                    (ba_nombre_apc == null || ba_nombre_apc == '' || ba_nombre_apc == 'undefined' || typeof ba_nombre_apc == 'undefined') &&
                                    (ba_vigencia == null || ba_vigencia == '' || ba_vigencia == 'undefined' || typeof ba_vigencia == 'undefined') &&
                                    (ba_plan_manejo_principal == null || ba_plan_manejo_principal == '' || ba_plan_manejo_principal == 'undefined' || typeof ba_plan_manejo_principal == 'undefined') &&
                                    (ba_tenencia_principal == null || ba_tenencia_principal == '' || ba_tenencia_principal == 'undefined' || typeof ba_tenencia_principal == 'undefined') &&
                                    (ba_fecha_certificacion_principal == null || ba_fecha_certificacion_principal == '' || ba_fecha_certificacion_principal == 'undefined' || typeof ba_fecha_certificacion_principal == 'undefined') &&
                                    (ba_fecha_certificacion_principal2 == null || ba_fecha_certificacion_principal2 == '' || ba_fecha_certificacion_principal2 == 'undefined' || typeof ba_fecha_certificacion_principal2 == 'undefined') &&
                                    (ba_altitud_inicial == null || ba_altitud_inicial == '' || ba_altitud_inicial == 'undefined' || typeof ba_altitud_inicial == 'undefined') &&
                                    (ba_altitud_final == null || ba_altitud_final == '' || ba_altitud_final == 'undefined' || typeof ba_altitud_final == 'undefined')
                                ) {
                                    var supergrid = Ext.getCmp('supergrid');
                                    supergrid.store.clearFilter();
                                } else {
                                    if (ba_municipio != '')
                                        filtro_ba_municipio = new Ext.util.Filter({
                                            property: "municipio_principal",
                                            value: ba_municipio
                                        });
                                    else
                                        filtro_ba_municipio = new Ext.util.Filter({
                                            property: "municipio_principal",
                                            value: ''
                                        });
                                    if (ba_localidad != '')
                                        filtro_ba_localidad = new Ext.util.Filter({
                                            property: "localidad_principal",
                                            value: ba_localidad
                                        });
                                    else
                                        filtro_ba_localidad = new Ext.util.Filter({
                                            property: "localidad_principal",
                                            value: ''
                                        });
                                    if (ba_nombre_apc != '')
                                        filtro_ba_nombre_apc = new Ext.util.Filter({
                                            property: "nombre_apc_principal",
                                            value: ba_nombre_apc
                                        });
                                    else
                                        filtro_ba_nombre_apc = new Ext.util.Filter({
                                            property: "nombre_apc_principal",
                                            value: ''
                                        });
                                    if (ba_vigencia != '')
                                        filtro_ba_vigencia = new Ext.util.Filter({
                                            property: "vigencia_principal",
                                            value: ba_vigencia
                                        });
                                    else
                                        filtro_ba_vigencia = new Ext.util.Filter({
                                            property: "vigencia_principal",
                                            value: ''
                                        });
                                    if (ba_plan_manejo_principal != '')
                                        filtro_ba_plan_manejo_principal = new Ext.util.Filter({
                                            property: "plan_manejo_principal",
                                            value: ba_plan_manejo_principal
                                        });
                                    else
                                        filtro_ba_plan_manejo_principal = new Ext.util.Filter({
                                            property: "plan_manejo_principal",
                                            value: ''
                                        });
                                    if (ba_tenencia_principal != '')
                                        filtro_ba_tenencia_principal = new Ext.util.Filter({
                                            property: "tenencia_principal",
                                            value: ba_tenencia_principal
                                        });
                                    else
                                        filtro_ba_tenencia_principal = new Ext.util.Filter({
                                            property: "tenencia_principal",
                                            value: ''
                                        });
                                    if (ba_fecha_certificacion_principal != '')
                                        filtro_ba_fecha_certificacion_principal = new Ext.util.Filter({
                                            property: "fecha_certificacion_principal",
                                            value: ba_fecha_certificacion_principal
                                        });
                                    else
                                        filtro_ba_fecha_certificacion_principal = new Ext.util.Filter({
                                            property: "fecha_certificacion_principal",
                                            value: ''
                                        });
                                    if (ba_fecha_certificacion_principal2 != '')
                                        filtro_ba_fecha_certificacion_principal2 = new Ext.util.Filter({
                                            property: "fecha_certificacion_principal2",
                                            value: ba_fecha_certificacion_principal2
                                        });
                                    else
                                        filtro_ba_fecha_certificacion_principal2 = new Ext.util.Filter({
                                            property: "fecha_certificacion_principal2",
                                            value: ''
                                        });

                                    var filters = [];

                                    filters.push(
                                        filtro_ba_municipio,
                                        filtro_ba_localidad,
                                        filtro_ba_nombre_apc,
                                        filtro_ba_vigencia,
                                        filtro_ba_plan_manejo_principal,
                                        filtro_ba_tenencia_principal,
                                        filtro_ba_fecha_certificacion_principal,
                                        filtro_ba_fecha_certificacion_principal2
                                    );

                                    //ACTUALIZAMOS EL VALOR DEL "extraParams" EN LA STORE CON EL OPERADOR SELECCIONADO
                                    storePHP.getProxy().extraParams.op_fc = Ext.getCmp('split_operador_fc').getText();
                                    //ACTUALIZAMOS EL VALOR DEL "extraParams" EN LA STORE PARA BUSCAR POR RANGO DE ALTITUD
                                    storePHP.getProxy().extraParams.tipo_query = 'altitud';
                                    storePHP.getProxy().extraParams.altitud_inicial = ba_altitud_inicial;
                                    storePHP.getProxy().extraParams.altitud_final = ba_altitud_final;

                                    //AÑADIMOS EL ARREGLO DE FILTROS (LO CUAL RECARGA EL STORE AUTOMATICAMENTE)
                                    storePHP.filter(filters);

                                }
                            }
                        }
                    }
                ]
            }]
        });


        //DEFINIMOS EL MODELO DE LA STORE PHP
        Ext.define('APC', {
            extend: 'Ext.data.Model',
            fields: [{
                    name: 'gid',
                    type: 'string'
                },
                {
                    name: 'nombre_apc_principal',
                    type: 'string'
                },
                {
                    name: 'municipio_principal',
                    type: 'string'
                },
                {
                    name: 'localidad_principal',
                    type: 'string'
                },
                {
                    name: 'vigencia_principal',
                    type: 'string'
                },
                {
                    name: 'plan_manejo_principal',
                    type: 'string'
                },
                {
                    name: 'tenencia_principal',
                    type: 'string'
                },
                {
                    name: 'fecha_certificacion_principal',
                    type: 'string'
                },
                {
                    name: 'bbox',
                    type: 'string'
                }
            ],
            idProperty: 'gid'
        });

        var itemsPerPage = 20;
        var storePHP = Ext.create('Ext.data.Store', {
            id: 'storePHP',
            pageSize: itemsPerPage,
            autoLoad: false,
            model: 'APC',
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'jsonp',
                url: '/getStoreItems.php',
                reader: {
                    rootProperty: 'topics',
                    totalProperty: 'totalCount'
                },
                simpleSortMode: true,
                extraParams: {
                    op_fc: 'null',
                    tipo_query: 'normal',
                    altitud_inicial: 'null',
                    altitud_final: 'null'
                }
            },
            callbackKey: 'callback',
            sorters: [{
                property: 'nombre_apc_principal',
                direction: 'ASC'
            }]
        });

        var columns = [{
                xtype: 'rownumberer',
                width: 35,
                align: 'center'
            },
            {
                text: 'Nombre APC',
                dataIndex: 'nombre_apc_principal',
                sortable: true,
                width: 200,
                align: 'left'
            },
            {
                text: 'Municipio',
                dataIndex: 'municipio_principal',
                sortable: true,
                width: 200,
                align: 'left'
            },
            {
                text: 'Localidad',
                dataIndex: 'localidad_principal',
                sortable: true,
                width: 200,
                align: 'left'
            },
            {
                text: 'Vigencia',
                dataIndex: 'vigencia_principal',
                sortable: true,
                width: 110,
                align: 'center'
            },
            {
                text: 'Cuenta con plan de manejo',
                dataIndex: 'plan_manejo_principal',
                sortable: true,
                width: 215,
                align: 'center'
            },
            {
                text: 'Tenencia',
                dataIndex: 'tenencia_principal',
                sortable: true,
                width: 110,
                align: 'center'
            },
            {
                text: 'Fecha de certificación',
                dataIndex: 'fecha_certificacion_principal',
                sortable: true,
                width: 180,
                align: 'center'
            }
        ];

        var supergrid = Ext.create('Ext.grid.Panel', {
            preventHeader: true,
            id: 'supergrid',
            scrollable: 'x',
            frame: true,
            store: storePHP,
            columns: columns,
            dockedItems: [{
                xtype: 'pagingtoolbar',
                id: 'myToolbar',
                store: storePHP,
                dock: 'bottom',
                displayInfo: true
            }],
            autoLoad: true,
            listeners: {
                itemclick: function (grid, record, item, index, e) {
                    //#WORKING OK - NEED TO PASS THE CORRECT FEATURE INDEX
                    selectControl.unselectAll();
                    selectControl.select(vectorPoligonos.features[0]);
                    var bbox = grid.getStore().getAt(index).get('bbox');
                    bbox = bbox.split(",").map(Number);
                    map.zoomTo(17);
                    map.zoomToExtent(bbox);

                },
                'afterrender': function (grid) {
                    storePHP.reload();
                }
            }
        });


        var ventanaLogin = Ext.create('Ext.window.Window', {
            title: 'Iniciar sesión',
            id: 'ventanaLogin',
            height: 150,
            width: 320,
            modal: true,
            draggable: false,
            closeAction: 'hide',
            iconCls: 'prona_icon',
            listeners: {
                hide: function () {
                    this.down('form').getForm().reset();
                }
            },
            items: [{
                xtype: 'form',
                url: 'validaUsuario.php',
                method: 'POST',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                border: false,
                style: {
                    margin: '10px'
                },
                items: [{
                        xtype: 'textfield',
                        name: 'user',
                        allowBlank: false,
                        fieldLabel: 'Usuario',
                        inputType: 'text'
                    },
                    {
                        xtype: 'textfield',
                        name: 'pass',
                        allowBlank: false,
                        fieldLabel: 'Contraseña',
                        inputType: 'password'
                    }
                ],
                buttons: [{
                    text: 'Acceder',
                    formBind: true,
                    disabled: true,
                    handler: function () {
                        var form = this.up('form').getForm();
                        if (form.isValid()) {
                            form.submit({
                                success: function (form, action) {
                                    window.location.reload();
                                },
                                failure: function (form, action) {
                                    Ext.Msg.alert('Error', action.result.msg);

                                }
                            });
                        } else {
                            Ext.Msg.alert('Error', 'Datos no validos');
                        }
                    }
                }]
            }]
        });

        var panelNorte = Ext.create('Ext.panel.Panel', {
            region: 'north',
            id: 'panelNorte',
            preventHeader: true,
            layout: {
                type: 'hbox',
                align: 'middle',
                //pack:'start'
            },
            bodyStyle: 'background:#2a3f5d; color:white; font-family:Roboto;',
            items: [
                <?php if ($access === 'denied') {?> {
                    flex: 0.850,
                    xtype: 'box',
                    html: '<div style="float:left;"><a target="_blank" href="http://pronaturaveracruz.org"><img src="images/logo_geoportal.png" style="width:43px; height:40px; margin-top:2px; margin-left:5px;"></a></div><div style="float:left; padding:15px 0 10px 7px; border:0px solid coral; font-size:25px; font-family:Roboto-Condensed;">GEOPORTAL<a style="color:#98c93c; font-size:15px;"> Pronatura Veracruz A.C. </a></div>'
                },
                {
                    flex: 0.100,
                    xtype: 'button',
                    id: 'botonLogin',
                    text: '<span style="color:white;">Iniciar sesión</span>',
                    icon: 'images/new_icons/user.png',
                    style: {
                        backgroundColor: '#787878',
                        borderColor: 'transparent',
                        margin: '5px'
                    },
                    listeners: {
                        click: function () {
                            ventanaLogin.show();
                        }
                    }
                },
                {
                    flex: 0.050,
                    xtype: 'box',
                    html: '<span style="border:0px solid coral; display:flex; align-items:center; height:45px;"><a target="_blank" title="Facebook - Pronatura Veracruz A.C." href="http://www.facebook.com/PronaturaVeraruz"><img src="images/new_icons/facebook_wl.png"></a> <a target="_blank" href="http://www.youtube.com/user/pronaturaveracruz" title="Youtube - Pronatura Veracruz A.C."><img src="images/new_icons/youtube_wl.png"/></a><a target="_blank" title="Vimeo - Pronatura Veracruz A.C." href="http://vimeo.com/pronaturaveracruz"><img src="images/new_icons/vimeo_wl.png"></a> </span>'
                }
                <?php
                    } elseif ($access === 'granted') {
                        ?> {
                    flex: 0.820,
                    xtype: 'box',
                    html: '<div style="float:left;"><a target="_blank" href="http://pronaturaveracruz.org"><img src="images/PronaLogo.png" style="width:40px; height:40px;"></a></div><div style="float:left; padding:5px 0 5px 10px; border:0px solid coral; font-size:12px;">PORTAL DE GEOINFORMACIÓN<br><a style="color:#3892d3;"> Sistema de control y seguimiento de Áreas Privadas de Conservación </a> - <a style="color:#98c93c;"> Pronatura Veracruz A.C. </a></div>'
                },
                {
                    flex: 0.130,
                    xtype: 'splitbutton',
                    text: '<span style="color:white;"><?php echo $nombre_completo; ?></span>',
                    icon: 'images/new_icons/user.png',
                    style: {
                        backgroundColor: '#787878',
                        borderColor: 'transparent',
                        margin: '5px'
                    },
                    menu: [{
                        text: 'Cerrar sesión',
                        icon: 'images/new_icons/disconnect.png',
                        handler: function () {
                            location.href = 'logout.php';
                        }
                    }]
                },
                {
                    flex: 0.050,
                    xtype: 'box',
                    html: '<span style="border:0px solid coral; display:flex; align-items:center; height:45px;"><a target="_blank" title="Facebook - Pronatura Veracruz A.C." href="http://www.facebook.com/PronaturaVeraruz"><img src="images/new_icons/facebook_wl.png"></a> <a target="_blank" href="http://www.youtube.com/user/pronaturaveracruz" title="Youtube - Pronatura Veracruz A.C."><img src="images/new_icons/youtube_wl.png"/></a><a target="_blank" title="Vimeo - Pronatura Veracruz A.C." href="http://vimeo.com/pronaturaveracruz"><img src="images/new_icons/vimeo_wl.png"></a> </span>'
                }
                <?php
                    }
                ?>
            ]
        });


        var panelCentro = Ext.create('Ext.tab.Panel', {
            region: 'center',
            id: 'panelCentro',
            preventHeader: true,
            activeTab: 1,
            plain: true,
            items: [{
                    xtype: 'box',
                    title: 'Presentaci&oacute;n',
                    autoEl: {
                        tag: 'iframe',
                        src: 'resources/presentacion.html'
                    }
                },
                {
                    xtype: 'panel',
                    title: 'Mapa',
                    id: 'tabMapa',
                    dockedItems: {
                        xtype: 'toolbar',
                        dock: 'bottom',
                        items: [{
                            xtype: 'displayfield',
                            value: '<div style="font-size: 12px; color:dimgray; font-family:Roboto;"><b>Longitud:</b> 0.0000&nbsp;&nbsp;<b style="color:crimson;">|</b>&nbsp;&nbsp;<b>Latitud:</b> 0.0000</div>',
                            id: 'barraCoordenadas'
                        }]
                    },
                    layout: 'fit',
                    items: [panelMapa]
                },
                {
                    xtype: 'panel',
                    id: 'tab_metadata',
                    title: 'Metadatos',
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                            id: 'metadato_mensaje_inicial',
                            xtype: 'box',
                            hidden: false,
                            html: '<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;">Para obtener el metadato de algún <a style="color:navy;">tema</a> del portal, de <a style="color:navy;">clic</a> sobre cualquiera de ellos desde la pestaña <a style="color:navy;">Temas.</a></div>'
                        },
                        {
                            id: 'metadato_mensaje_sinmetadato',
                            xtype: 'box',
                            hidden: false,
                            html: '<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;">El <a style="color:navy;">tema</a> seleccionado no cuenta con metadato.</div>'
                        },
                        {
                            id: 'metadata',
                            xtype: 'box',
                            hidden: false,
                            autoEl: {
                                tag: 'iframe',
                                src: ''
                            }
                        }
                    ]
                },
                {
                    xtype: 'box',
                    title: 'Ayuda',
                    autoEl: {
                        tag: 'iframe',
                        src: 'resources/faq.html'
                    }
                },
                {
                    title: 'Acerca de Pronatura Veracruz A.C.'
                }
            ],
            defaults: {
                listeners: {
                    activate: function (tab, eOpts) {
                        if (tab.title == 'Acerca de Pronatura Veracruz A.C.') {
                            acercaPronatura.show();
                        }
                    }
                }
            }
        });



        var panelSur = Ext.create('Ext.panel.Panel', {
            region: 'south',
            id: 'panelSur',
            preventHeader: true,
            bodyStyle: 'background:#cecece; color:#2a3f5d; text-align:center; font-family:Roboto; font-weight:bold;',
            items: [{
                xtype: 'component',
                html: '© DR Pronatura A.C. Veracruz. Ignacio Zaragoza #153, zona centro, entre Justo Félix Fernéndez Lépez y Ayuntamiento, Coatepec, Veracruz, México. C.P. 91500.'
            }]

        });

        var overviewMap = Ext.create('GeoExt.OverviewMap', {
            id: 'panoramaMapa',
            overviewOptions: {
                autoPan: true,
                minRatio: 16,
                maxRatio: 5,
                mapOptions: {
                    numZoomLevels: 1
                }
            }
        });

        var panelIzquierdo = Ext.create('Ext.tab.Panel', {
            region: 'west',
            id: 'panelIzquierdo',
            preventHeader: true,
            activeTab: 0,
            plain: true,
            collapsible: true,
            collapseMode: 'mini',
            width: 100,
            maxWidth: 500,
            minWidth: 100,
            split: true,
            listeners: {
                resize: function () {
                    //console.log('Redimenzionar el panelpanoramaMapa y el overview');
                    //console.log('Width del panel izquierdo: ' + this.getWidth());
                    Ext.getCmp('panelpanoramaMapa').setWidth(this.getWidth());
                    Ext.getCmp('panoramaMapa').setWidth(this.getWidth());
                }
            },
            items: [

                {
                    xtype: 'panel',
                    title: 'Temas',
                    itemId: 'temas',
                    id: 'temas',
                    listeners: {
                        activate: function () {
                            var rw = Ext.getCmp('panelIzquierdo').getWidth();
                            var diferencia_restar = (rw * 40) / 100;
                            Ext.getCmp('panelIzquierdo').setWidth(rw - diferencia_restar);
                        }
                    },
                    tbar: [
                        '->',
                        {
                            xtype: 'tbtext',
                            text: 'Opciones: '
                        },
                        {
                            xtype: 'button',
                            enableToggle: true,
                            toggleGroup: 'tbar_options',
                            pressed: true,
                            allowDepress: true,
                            tooltip: 'Ver arbol de temas por categorias',
                            iconCls: 'ordenCategorias_icon',
                            handler: function () {
                                //console.log('Se muestra el panel: Arbol de categorias');
                                Ext.getCmp('arbolTemasOrden').hide();
                                Ext.getCmp('arbolTemas').show();
                            }
                        },
                        {
                            xtype: 'button',
                            enableToggle: true,
                            toggleGroup: 'tbar_options',
                            pressed: false,
                            allowDepress: true,
                            tooltip: 'Ordenar visibilidad de temas',
                            iconCls: 'ordenar_icon',
                            handler: function () {
                                //console.log('Se muestra el panel: Ordenar visibilidad');
                                Ext.getCmp('arbolTemas').hide();
                                Ext.getCmp('arbolTemasOrden').show();
                            }
                        }
                    ],
                    items: [
                        arbolTemas, arbolTemasOrden,
                        {
                            xtype: 'panel',
                            id: 'panelpanoramaMapa',
                            collapsible: true,
                            collapseDirection: 'bottom',
                            collapseMode: 'header',
                            listeners: {
                                collapse: function (panel, eOpts) {
                                    Ext.getCmp('arbolTemas').setHeight(heightarbolTemas + heightpanelpanoramaMapa - 115);
                                    Ext.getCmp('arbolTemasOrden').setHeight(heightarbolTemas + heightpanelpanoramaMapa - 115);

                                },
                                expand: function (panel, eOpts) {
                                    Ext.getCmp('arbolTemas').setHeight(heightarbolTemas);
                                    Ext.getCmp('arbolTemasOrden').setHeight(heightarbolTemas);
                                }
                            },
                            title: 'Panorama del mapa',
                            items: [
                                /*<?php// if($usuario_actual === 'someuser'){  ?>*/
                                overviewMap
                                /*<?php// } ?>*/
                            ]
                        }

                    ]
                },
                {
                    xtype: 'panel',
                    title: 'Buscar',
                    itemId: 'buscar',
                    id: 'buscar',
                    listeners: {
                        activate: function () {
                            var rw = Ext.getCmp('panelIzquierdo').getWidth();
                            var diferencia_sumar = (rw * 67) / 100;
                            Ext.getCmp('panelIzquierdo').setWidth(rw + diferencia_sumar);

                        }
                    },
                    items: [{
                            xtype: 'toolbar',
                            id: 'searchTb1',
                            layout: {
                                pack: 'center'
                            },
                            items: [{
                                    xtype: 'label',
                                    style: 'color:dimgray; font-weight:bold;',
                                    text: 'Buscar por:'
                                },
                                {
                                    xtype: 'splitbutton',
                                    id: 'split',
                                    tooltip: 'De click para cambiar el parametro de busqueda',
                                    text: 'Municipio',
                                    menu: [{
                                            text: 'Municipio',
                                            id: 'btmunicipio',
                                            disabled: true,
                                            handler: function () {
                                                splitbut = Ext.getCmp('split');
                                                //CAMBIAMOS EL TEXTO DEL BOTON
                                                splitbut.setText('Municipio');
                                                //LIMPIAMOS EL FILTRO
                                                supergrid.store.clearFilter();
                                                //DESHABILITAMOS EL SELECCIONADO EN EL MENU
                                                Ext.getCmp('btmunicipio').disable();
                                                //ACTIVAMOS TODOS LOS DEMAS
                                                Ext.getCmp('btnomapc').enable();
                                                Ext.getCmp('btloc').enable();
                                            }
                                        },
                                        {
                                            text: 'Nombre APC',
                                            id: 'btnomapc',
                                            handler: function () {
                                                splitbut = Ext.getCmp('split');
                                                //CAMBIAMOS EL TEXTO DEL BOTON
                                                splitbut.setText('Nombre APC');
                                                //LIMPIAMOS EL FILTRO
                                                supergrid.store.clearFilter();
                                                //DESHABILITAMOS EL SELECCIONADO EN EL MENU
                                                Ext.getCmp('btnomapc').disable();
                                                //ACTIVAMOS TODOS LOS DEMAS
                                                Ext.getCmp('btmunicipio').enable();
                                                Ext.getCmp('btloc').enable();
                                            }
                                        },
                                        {
                                            text: 'Localidad',
                                            id: 'btloc',
                                            handler: function () {
                                                splitbut = Ext.getCmp('split');
                                                //CAMBIAMOS EL TEXTO DEL BOTON
                                                splitbut.setText('Localidad');
                                                //LIMPIAMOS EL FILTRO
                                                supergrid.store.clearFilter();
                                                //DESHABILITAMOS EL SELECCIONADO EN EL MENU
                                                Ext.getCmp('btloc').disable();
                                                //ACTIVAMOS TODOS LOS DEMAS
                                                Ext.getCmp('btmunicipio').enable();
                                                Ext.getCmp('btnomapc').enable();
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        //
                        {
                            xtype: 'toolbar',
                            id: 'searchTb2',
                            layout: {
                                pack: 'center'
                            },
                            items: [{
                                    xtype: 'textfield',
                                    id: 'consulta',
                                    fieldLabel: '',
                                    emptyText: 'Ingrese su busqueda...',
                                    enableKeyEvents: true,
                                    listeners: {
                                        keypress: function (consulta, e, eOpts) {
                                            if (e.keyCode == 13) {
                                                var btnBuscar = Ext.getCmp('btnBuscar');
                                                btnBuscar.fireEvent('click');
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    id: 'btnBuscar',
                                    tooltip: 'Buscar',
                                    iconCls: 'lupa_icon',
                                    listeners: {
                                        click: function () {
                                            var consulta = Ext.getCmp('consulta').value;
                                            var parametro = Ext.getCmp('split').getText();
                                            var myToolbar = Ext.getCmp('myToolbar');
                                            if (consulta == null || consulta === null || consulta == '' || consulta == 'undefined' || typeof consulta == "undefined") {
                                                supergrid.store.clearFilter();
                                                storePHP.reload();
                                            } else {
                                                if (parametro == 'Municipio') {
                                                    storePHP.addFilter({
                                                        id: 'municipio_principal',
                                                        property: 'municipio_principal',
                                                        value: consulta
                                                    });
                                                    storePHP.sorters.clear();
                                                    storePHP.sorters.add(new Ext.util.Sorter({
                                                        property: 'municipio_principal',
                                                        direction: 'ASC'
                                                    }));
                                                } else if (parametro == 'Nombre APC') {
                                                    storePHP.addFilter({
                                                        id: 'nombre_apc_principal',
                                                        property: 'nombre_apc_principal',
                                                        value: consulta
                                                    });
                                                    storePHP.sorters.clear();
                                                    storePHP.sorters.add(new Ext.util.Sorter({
                                                        property: 'nombre_apc_principal',
                                                        direction: 'ASC'
                                                    }));
                                                } else if (parametro == 'Localidad') {
                                                    storePHP.addFilter({
                                                        id: 'localidad_principal',
                                                        property: 'localidad_principal',
                                                        value: consulta
                                                    });
                                                    storePHP.sorters.clear();
                                                    storePHP.sorters.add(new Ext.util.Sorter({
                                                        property: 'localidad_principal',
                                                        direction: 'ASC'
                                                    }));
                                                }
                                            }
                                        }
                                    }
                                },
                                //BOTON B. AVANZADA
                                {
                                    xtype: 'button',
                                    style: 'margin-left:0px;',
                                    tooltip: 'Busqueda avanzada',
                                    iconCls: 'engrane_icon',
                                    listeners: {
                                        click: function () {
                                            busqueda_avanzada.show();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    style: 'margin-left:0px;',
                                    tooltip: 'Limpiar busqueda',
                                    iconCls: 'limpiar_icon',
                                    listeners: {
                                        click: function () {
                                            var consulta = Ext.getCmp('consulta');
                                            consulta.reset();
                                            supergrid.store.clearFilter();
                                            storePHP.reload();
                                        }
                                    }
                                }


                            ]
                        },
                        supergrid
                    ]
                }

            ]
        });


        var panelDerecho = Ext.create('Ext.tab.Panel', {
            region: 'east',
            id: 'panelDerecho',
            deferredRender: false, //CARGAMOS TODAS LAS PESTAÑAS AUTOMATICAMENTE EN MEMORIA
            preventHeader: true,
            activeTab: 0,
            plain: true,
            resizable: false,
            collapsible: true,
            collapseMode: 'mini',
            width: 330,
            maxWidth: 330,
            minWidth: 330,
            split: true,
            items: [{
                    xtype: 'panel',
                    title: 'Información',
                    itemId: 'leyenda',
                    id: 'panel_informacion',
                    scrollable: 'y',
                    layout: {
                        type: 'fit',
                        pack: 'center',
                        align: 'center'
                    },
                    items: [{
                            id: 'mensaje_inicial',
                            xtype: 'component',
                            hidden: false,
                            html: '<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; /*margin:350px 25px 5px 25px;*/">Para obtener información básica de los <a style="color:navy;">temas</a> del portal, de <a style="color:navy;">clic</a> sobre cualquiera de ellos desde la pestaña <a style="color:navy;">Temas.</a></div>'
                        },
                        {
                            id: 'title_layer',
                            xtype: 'component',
                            hidden: true,
                            autoEl: {
                                html: 'OK-title_layer',
                                width: 90,
                                style: 'margin-left:25px; margin-bottom:15px;'
                            }
                        },
                        {
                            id: 'title_simbology',
                            xtype: 'component',
                            hidden: true,
                            html: '<div style="color:navy; font-size:12px; text-align:left; font-family:Roboto; margin:5px 25px 5px 25px;">Simbología:<hr></div>'
                        },
                        {
                            id: 'simbology',
                            xtype: 'component',
                            hidden: true,
                            autoEl: {
                                html: 'OK-simbologia',
                                width: 90,
                                style: 'margin-left:25px; margin-bottom:15px;'
                            }
                        },
                        {
                            id: 'title_source',
                            xtype: 'component',
                            hidden: true,
                            html: '<div style="color:navy; font-size:12px; text-align:left; font-family:Roboto; margin:5px 25px 5px 25px;">Fuente:<hr></div>'
                        },
                        {
                            id: 'source',
                            xtype: 'component',
                            hidden: true,
                            autoEl: {
                                html: 'OK-fuente',
                                width: 90,
                                style: 'margin-left:25px; margin-bottom:15px;'
                            }
                        },
                        {
                            id: 'title_summary',
                            xtype: 'component',
                            hidden: true,
                            html: '<div style="color:navy; font-size:12px; text-align:left; font-family:Roboto; margin:5px 25px 5px 25px;">Resumen:<hr></div>'
                        },
                        {
                            id: 'summary',
                            xtype: 'component',
                            hidden: true,
                            autoEl: {
                                html: 'OK-resumen',
                                width: 90,
                                style: 'margin-left:25px; margin-bottom:15px;'
                            }
                        },
                    ]
                },
                {
                    xtype: 'panel',
                    title: 'Atributos',
                    id: 'panelAtributos',
                    scrollable: true,
                    items: [{
                        id: 'mensaje_inicial_atributos',
                        xtype: 'component',
                        html: '<div style="color:dimgray; font-size:16px; text-align:justify; font-family:Roboto; /*margin:350px 25px 5px 25px;*/">Para obtener los atributos de un <a style="color:navy;">tema</a>:<br><br>1) El tema debe estar activo y visible.<sup style="font-weight:bold;">*</sup><br><br>2) Hacer <a style="color:navy;">clic</a> sobre el área deseada en el mapa.<br><br><a style="font-size:11px; font-weight:bold; font-style:italic;">* Con excepcion del tema "Ubicacion"</a></div>'
                    }]
                }
            ]

        });


        var viewport = Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            hideBorders: true,
            items: {
                layout: 'border',
                items: [panelNorte, panelCentro, panelSur, panelIzquierdo, panelDerecho]
            },
            listeners: {
                resize: function (leftPanel, width, height) {

                    console.warn('Viewport= width:' + width + ' height:' + height);

                    //DEFINIMOS EL ANCHO Y EL ALTO DE ACUERDO AL TAMAÑO DEL VIEWPORT (UTILIZANDO UNA REGLA DE TRES PARA LAS PROPORCIONES)
                    var widthPanelIzquierdo = (width * 17.5) / 100;
                    var widthPanelDerecho = (width * 17.5) / 100;
                    var widthPanelCentro = (width * 65) / 100;
                    //A LA ALTURA DEL VIEWPORT LE QUITAMOS 46 DE ALTO DEL PANEL NORTE Y 19 DE ALTO DEL PANEL SUR = 65
                    heightPaneles_LCR = (height - 65);

                    //CONDICIONAL PARA REDIMENSIONAR SI LA PESTAÑA ACTIVA NO ES LA PRIMERA
                    var panelIzquierdo = Ext.getCmp('panelIzquierdo');
                    var indicepestanaActiva = panelIzquierdo.getActiveTab();
                    var pestanaActiva = panelIzquierdo.items.indexOf(indicepestanaActiva);
                    if (pestanaActiva == '1') {
                        var diferencia_sumar = (widthPanelIzquierdo * 67) / 100;
                        Ext.getCmp('panelIzquierdo').setWidth(widthPanelIzquierdo + diferencia_sumar);
                    } else {
                        Ext.getCmp('panelIzquierdo').setWidth(widthPanelIzquierdo);
                    }

                    Ext.getCmp('panelIzquierdo').setHeight(heightPaneles_LCR);
                    Ext.getCmp('panelCentro').setWidth(widthPanelCentro);
                    Ext.getCmp('panelCentro').setHeight(heightPaneles_LCR);
                    Ext.getCmp('panelDerecho').setWidth(widthPanelDerecho);
                    Ext.getCmp('panelDerecho').setHeight(heightPaneles_LCR);


                    //CONTENIDOS DEL PANEL IZQUIERDO
                    heightarbolTemas = (heightPaneles_LCR * 60) / 100;
                    heightpanelpanoramaMapa = (heightPaneles_LCR * 40) / 100;
                    var heightpanoramaMapa = heightpanelpanoramaMapa - 50; //LEQUITAMOS 50 POR EL TITULO Y BORDE DEL PANEL QUE LO CONTIENE
                    var heightsearchTb1 = 35;
                    var heightsearchTb2 = 35;
                    var heightsupergrid = (heightPaneles_LCR - 70) - 35; //LE QUITAMOS 70PX DE LAS 2 TOOLBARS Y 35PX POR EL ESPACIO QUE OCUPA LA BARRA DE DESPLAZAMIENTO HORIZONTAL

                    Ext.getCmp('arbolTemasOrden').setWidth(widthPanelIzquierdo);
                    Ext.getCmp('arbolTemasOrden').setHeight(heightarbolTemas);

                    Ext.getCmp('arbolTemas').setWidth(widthPanelIzquierdo);
                    Ext.getCmp('arbolTemas').setHeight(heightarbolTemas);
                    Ext.getCmp('panelpanoramaMapa').setWidth(widthPanelIzquierdo);
                    Ext.getCmp('panelpanoramaMapa').setHeight(heightpanelpanoramaMapa);
                    Ext.getCmp('panoramaMapa').setWidth(widthPanelIzquierdo);
                    Ext.getCmp('panoramaMapa').setHeight(heightpanoramaMapa);
                    Ext.getCmp('searchTb1').setHeight(heightsearchTb1);
                    Ext.getCmp('searchTb2').setHeight(heightsearchTb2);
                    Ext.getCmp('supergrid').setHeight(heightsupergrid);

                    //CONTENIDOS DEL PANEL CENTRO

                    //CONTENIDOS DEL PANEL DERECHO
                    var padding_top_adjust = heightPaneles_LCR / 2;
                    var padding_LR = padding_top_adjust * 0.06;
                    Ext.getCmp('mensaje_inicial').getEl().dom.style.padding = padding_top_adjust + 'px ' + padding_LR + 'px 0 ' + padding_LR + 'px';
                    Ext.getCmp('mensaje_inicial_atributos').getEl().dom.style.padding = padding_top_adjust + 'px ' + padding_LR + 'px 0 ' + padding_LR + 'px';

                    Ext.getCmp('panel_informacion').setWidth(widthPanelDerecho);
                    Ext.getCmp('panel_informacion').setHeight(heightPaneles_LCR);
                    Ext.getCmp('panelAtributos').setWidth(widthPanelDerecho);
                    Ext.getCmp('panelAtributos').setHeight(heightPaneles_LCR);





                }
            }

        });
    }

});


var currentPopup;

function cerrarPopup(unselect) {
    if (currentPopup && currentPopup != this.popup) {
        currentPopup.hide();
        currentPopup = null;
    }
    if (unselect) {
        selectControl.unselectAll();
    }
}

function pasarAlGrid(nombre_apc, folio, hectareas, estado, municipio, vigencia, fecha_certificacion, numero_sedema, cuenta_plan_manejo) {

    //REQUEST JSONP PARA ENCONTRAR COINCIDENCIAS DE ATRIBUTOS EN ESTE APC
    Ext.data.JsonP.request({
        url: '/getAtributos.php',
        params: {
            nombre_apc: nombre_apc,
            folio: folio,
            hectareas: hectareas,
            estado: estado,
            municipio: municipio,
            vigencia: vigencia,
            fecha_certificacion: fecha_certificacion,
            numero_sedema: numero_sedema,
            cuenta_plan_manejo: cuenta_plan_manejo
        },
        success: function (result, request) {

            //SI EL ELEMENTO AL QUE SE LE DA CLIC NO HA SIDO CAPTURADO EN EL SISAPC, MOSTRARA UN PANEL INDICANDOLO
            if (result.nombre_apc === '' || result.nombre_apc.length === 0) {
                if (Ext.getCmp('panelConcentrador'))
                    Ext.getCmp('panelConcentrador').destroy();


                var panelConcentrador = Ext.create('Ext.panel.Panel', {
                    id: 'panelConcentrador',
                    resizable: false,
                    shadow: true,
                    border: false,
                    html: '<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;">El APC que seleccionó se encuentra georeferenciado, pero aún no se captura su información.</div>'
                });

                var panelAtributos = Ext.getCmp('panelAtributos');
                panelAtributos.removeAll();
                panelAtributos.add(panelConcentrador);
                panelAtributos.doLayout();

                Ext.getCmp('panelDerecho').setActiveTab(panelAtributos);
            } else {
                //FUNCIÓN PARA DAR FORMATO A LAS FECHAS
                function formatDate4apc(fecha) {
                    if (fecha != 'nul' || fecha != '') {
                        var fecha_separada = fecha.split("-");
                        var anyo = fecha_separada[0];
                        var mes = fecha_separada[1];
                        var dia = fecha_separada[2];
                        dia = dia.slice(0, -1);
                        switch (mes) {
                            case '01':
                                mes = 'Enero';
                                break;
                            case '02':
                                mes = 'Febrero';
                                break;
                            case '03':
                                mes = 'Marzo';
                                break;
                            case '04':
                                mes = 'Abril';
                                break;
                            case '05':
                                mes = 'Mayo';
                                break;
                            case '06':
                                mes = "Junio";
                                break;
                            case '07':
                                mes = "Julio";
                                break;
                            case '08':
                                mes = "Agosto";
                                break;
                            case '09':
                                mes = "Septiembre";
                                break;
                            case '10':
                                mes = "Octubre";
                                break;
                            case '11':
                                mes = "Noviembre";
                                break;
                            case '12':
                                mes = "Diciembre";
                                break;
                            default:
                                break;
                        }
                        mes = mes.toLowerCase();
                        return dia + " de " + mes + " de " + anyo;
                    } else {
                        return "No se ha certificado aún."
                    }
                }

                var items = [];
                if ((result.fecha_certificacion == 'null' || result.fecha_certificacion == '') && (result.numero_sedema == ''))
                    numero_sedema = "No se ha certificado aún.";

                //Cambiar etiqueta cuando esta vacion o NULL el campo
                var vigencia = result.vigencia;
                var fecha_certificacion = result.fecha_certificacion;
                var numero_sedema = result.numero_sedema;
                var cuenta_plan_manejo = result.cuenta_plan_manejo;

                if (vigencia == 'null' || vigencia == '')
                    vigencia = "Desconocida";
                if (fecha_certificacion == 'null' || fecha_certificacion == '')
                    fecha_certificacion = "Desconocida";
                else
                    fecha_certificacion = formatDate4apc(fecha_certificacion);
                if (numero_sedema == 'null' || numero_sedema == '')
                    numero_sedema = "Desconocido";
                if (cuenta_plan_manejo == 'null' || cuenta_plan_manejo == '')
                    cuenta_plan_manejo = "Desconocido";



                items.push({
                    xtype: "propertygrid",
                    header: true,
                    hideHeaders: true,
                    sortableColumns: false,
                    listeners: {
                        'beforeedit': {
                            fn: function () {
                                return false;
                            }
                        }
                    },
                    title: 'Información del APC',
                    source: {
                        "Nombre del APC": result.nombre_apc,
                        "Folio": result.folio,
                        "Superficie [ha]": result.hectareas,
                        "Estado": result.estado,
                        "Municipio": result.municipio,
                        "Vigencia [años]": vigencia,
                        "Fecha de certificación": fecha_certificacion,
                        "Número de certificación": numero_sedema,
                        "¿Cuenta con plan de manejo?": cuenta_plan_manejo,
                        "Fotos": {
                            xtype: 'button',
                            text: 'View'
                        }
                    }
                });

                var panelAPC = Ext.create('Ext.panel.Panel', {
                    header: false,
                    width: 320,
                    border: false,
                    flex: 1,
                    items: items
                });

                var panelDescargar = Ext.create('Ext.panel.Panel', {
                    title: 'Descargar',
                    header: false,
                    icon: 'images/new_icons/download.png',
                    width: 320,
                    border: false,
                    flex: 1,
                    bodyStyle: {
                        textAlign: 'center',
                        padding: '0px'
                    },
                    items: []
                });

                var showHeader = 0;
                if (result.cuenta_plan_manejo === 'Si') {
                    panelDescargar.add({
                        xtype: 'button',
                        icon: 'images/new_icons/file_extension_pdf.png',
                        style: {
                            backgroundColor: '#f5f5f5',
                            borderColor: 'transparent',
                            margin: '5px'
                        },
                        text: '<span style="font-weight:bold; color:#e72e35;">Plan de manejo</span>',
                        href: '../' + result.ruta_plan_de_manejo
                    });
                    showHeader++;
                }

                //console.log("Header: " + panelDescargar.getHeader());
                if (showHeader > 0) {
                    panelDescargar.header = true;
                    panelDescargar.updateHeader(true);
                }


                //CREAMOS LOS PANELES DE INFORMACION

                //ALTITUD
                function numberWithCommas(x) {
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
                var altitud = JSON.stringify(result.altitud);
               //console.log("Altitud en el panel derecho: " + altitud);
                altitud = altitud.split("_");
                //console.warn('Longitud del arreglo:' + altitud.length);
                if (altitud.length == 1) //PUNTO
                    createPanelInfo('panelAltitud', 'Altitud', numberWithCommas(altitud[0]) + " msnm.", '');
                else if (altitud.length == 2) //POLIGONO
                    createPanelInfo('panelAltitud', 'Altitud', numberWithCommas(altitud[0]) + " a " + numberWithCommas(altitud[1]) + " msnm.", '');

                createPanelInfo('panelLocalidades', 'Localidades más cercanas (500m)', JSON.stringify(result.localidades), 'Municipio,Estado');
                createPanelInfo('panelVialidades', 'Vialidades', JSON.stringify(result.vialidades), 'Entidad,Número de vías,Derecho,Jurisdicción,Condición');
                createPanelInfo('panelEdafologia', 'Edafología', JSON.stringify(result.edafologia), 'Tipo de textura,Fase física,Fase química');
                createPanelInfo('panelGeologia', 'Geología', JSON.stringify(result.geologia), 'Tipo,Entidad,Era,Período,Época,Origen');
                createPanelInfo('panelClimas', 'Climas', JSON.stringify(result.climas), 'Temperatura,Precipitación');
                createPanelInfo('panelCuencas', 'Cuencas', JSON.stringify(result.cuencas), 'Tipo,Región hidrológica,Subregión hidrológica,Tipo de patrón de drenaje');

                createPanelInfo('panelZonasFuncionales', 'Zonas funcionales', JSON.stringify(result.zonas_funcionales), 'Nombre de la cuenca,Región hidrológica,Subregión hidrológica');
                createPanelInfo('panelUMAS', 'UMAS', JSON.stringify(result.umas), 'Nombre,Vigencia,Propietario');
                createPanelInfo('panelPSA', 'Pago por servicios ambientales', JSON.stringify(result.psa), 'Año, Superficie,Monto total,Estado,Municipio');

                createPanelInfo('panelPTA', 'Precipitación total anual', JSON.stringify(result.pta), '');
                createPanelInfo('panelAICAS', 'AICAS', JSON.stringify(result.aicas), '');
                createPanelInfo('panelRTP', 'Regiones terrestres prioritarias', JSON.stringify(result.rtp), '');
                createPanelInfo('panelRHP', 'Regiones hidrológicas prioritarias', JSON.stringify(result.rhp), 'Región,Biodiversidad,Amenaza/riesgo,Uso,Falta de información');
                createPanelInfo('panelSPT', 'Sitios prioritarios terrestres (prioridad)', JSON.stringify(result.spt), '');
                createPanelInfo('panelRamsar', 'Sitios RAMSAR', JSON.stringify(result.ramsar), 'Estado,Municipios,Fecha');
                createPanelInfo('panelANPFederales', 'ANP Federales', JSON.stringify(result.anps_federales), 'Categoría de decresto según el DOF,Categoría que define al ANP por su vocacióny caracteristicas,Estados,Municipios,Región,Superficie total[ha],Superficie terrestre[ha],Superficie marina[ha],Fecha de la primera publicación en el DOF,Fecha de la última publicación en el DOF');
                createPanelInfo('panelANPEstatales', 'ANP Estatales', JSON.stringify(result.anps_estatales), 'Tipo,Estado,Fecha del decreto,Categoría,Fuente,Superficie[ha]');

                createPanelInfo('panelANPMunicipales', 'ANP Municipales', JSON.stringify(result.anps_municipales), 'Estado,Fecha del decreto,Categoría,Fuente,Superficie[ha]');

                createPanelInfo('panelZonasArqueologicas', 'Zonas arqueológicas', JSON.stringify(result.zonas_arqueologicas), 'Estado,Municipio,Culturas');
                createPanelInfo('panelVegetacion', 'Uso de suelo y vegetación V', JSON.stringify(result.vegetacion), '');
                createPanelInfo('panelZeroExtinction', 'Sitios Cero Extinción de anfibios', JSON.stringify(result.zero_extinction), 'Riesgo,Estado,Municipio,Localidad,Área protegida');
                createPanelInfo('panelViveros', 'Red de viveros de biodiversidad', JSON.stringify(result.viveros), 'Tipo,Estado,Municipio,Localidad');
                createPanelInfo('panelRegionesIndigenas', 'Regiones indigenas', JSON.stringify(result.regiones_indigenas), '');
                createPanelInfo('panelUpgs', 'Unidades Productoras de Germoplasma', JSON.stringify(result.upgs), 'Especie,Gerencia regional,Estado,Municipio,Dependencia,Propietario,Densidad,Rendimiento,Fecha de recolección');

                /**/

                //DESTRUIR EL panelConcentrador (POR SI YA EXISTIA)
                if (Ext.getCmp('panelConcentrador'))
                    Ext.getCmp('panelConcentrador').destroy();


                var panelConcentrador = Ext.create('Ext.panel.Panel', {
                    id: 'panelConcentrador',
                    layout: {
                        type: 'vbox',
                        align: 'left'
                    },
                    resizable: false,
                    shadow: true,
                    border: false,
                    items: [
                        panelAPC, panelDescargar, panelAltitud, panelLocalidades, panelVialidades, panelEdafologia, panelGeologia, panelClimas, panelCuencas, panelZonasFuncionales, panelPTA, panelAICAS, panelRTP, panelRHP, panelSPT, panelRamsar, panelPSA, panelANPFederales, panelANPEstatales, panelANPMunicipales, panelZonasArqueologicas, panelVegetacion, panelZeroExtinction, panelViveros, panelRegionesIndigenas, panelUpgs, panelUMAS
                    ]
                });


                var panelAtributos = Ext.getCmp('panelAtributos');
                panelAtributos.removeAll();
                panelAtributos.add(panelConcentrador);
                panelAtributos.doLayout();

                var panelDerecho = Ext.getCmp('panelDerecho');
                panelDerecho.setActiveTab(panelAtributos);

            }
        },
        failure: function (result, request) {
            console.warn('FAILURE!');
        }
    });
}

function updateTreeLayerInfo(element, title, shapename, source, summary, layername, metadataHtmlUrl, metadataZipUrl) {
    //CAMBIAMOS A LA PESTAÑA INFORMACIÓN
    var tab_informacion = Ext.getCmp('panel_informacion');
    var panelDerecho = Ext.getCmp('panelDerecho');
    panelDerecho.setActiveTab(tab_informacion);
    //OCULTAMOS EL MENSAJE INICIAL
    Ext.get('mensaje_inicial').hide();
    //MOSTRAMOS LOS TITULOS Y CONTENEDORES
    Ext.get('title_layer').show();
    Ext.get('title_simbology').show();
    Ext.get('simbology').show();
    Ext.get('title_source').show();
    Ext.get('source').show();
    Ext.get('title_summary').show();
    Ext.get('summary').show();
    //ACTUALIZAMOS DE ACUERDO A LOS ELEMENTOS DE LA CAPA
    Ext.get('title_layer').update('<div style="color:dimgray; font-size:18px; text-align:left; font-family:Roboto; margin-top:5px;">' + title + '</div>');
    Ext.get('simbology').update('<table border="0px"><tr><td><img src="' + rutaGeoserver + 'pronatura/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=16&LAYER=' + shapename + '&LEGEND_OPTIONS=forceLabels:on"></td></tr></table>');
    ////////////////////////////////////////////////
    Ext.get('source').update("<div style='font-family:Roboto; text-align:justify; margin-right:25px; border:0px solid coral; /*ADDED IN NOV2017*/ white-space:pre-wrap; word-wrap: break-word;'>" + source + "</div>");
    Ext.get('summary').update("<div style='font-family:Roboto; text-align:justify; margin-right:25px; border:0px solid green; /*ADDED IN NOV2017*/ white-space:pre-wrap; word-wrap: break-word;'>" + summary + "</div>");
    //ACTUALIZAMOS EL TEXTO DE LA CAPA ACTIVA
    Ext.get('capa_activa').update('Tema activo: <b>' + title + '</b>');
    //CAMBIAMOS LA CAPA DEL GETFEATUREINFO
    featureInfo.layers = [eval(layername)];
    //OCULTAMOS EL MENSAJE INICIAL DEL METADATO Y EL MENSAJE "SIN METADATO"
    Ext.getCmp('metadato_mensaje_sinmetadato').update('<div style="color:dimgray; font-size:16px; text-align:center; font-family:Roboto; margin:350px 25px 5px 25px;">El <a style="color:navy;">tema</a> seleccionado no cuenta con metadato.</div>');
    Ext.getCmp('metadato_mensaje_inicial').hide();
    Ext.getCmp('metadato_mensaje_sinmetadato').hide();
    //MOSTRAMOS EL BOX DONDE CARGA EL HTML
    Ext.getCmp('metadata').show();
    //HACEMOS RENDER A LA PESTAÑA METADATOS (tab_metadata) PARA INICIALIZARLA SIN TENER QUE DARLE CLIC
    var tab_metadata = Ext.getCmp('tab_metadata');
    tab_metadata.doAutoRender();
    //SI EL FRAME YA HIZO RENDER ENTONCES LE MANDAMOS LA RUTA DEL METADATO (URL)
    var frame = Ext.getCmp('metadata');
    if (frame && frame.rendered) {
        frame.getEl().dom.src = metadataHtmlUrl;
    }
    //DEFINIMOS LA RUTA PARA LA DESCARGA DEL METADATO (ZIP)
    ruta_shapefile_metadato_zip = metadataZipUrl;
    //ACTIVAMOS EL BOTON PARA DESCARGA
    Ext.getCmp('btn_descargar_metadato').enable();
    //DESTRUIR EL SLIDER (POR SI YA EXISTIA)
    if (Ext.getCmp('opacitySlider')) {
        //console.warn('Ya existe');  
        Ext.getCmp('opacitySlider').destroy();
        //Ext.getCmp('mapPanel_toolbar').doLayout();
    }

    if (element !== 'tree_ubi' && element !== 'tree_advc') {
        //CREAMOS EL SLIDER
        var actual_opacity = eval(layername).opacity;

        Ext.create('GeoExt.slider.LayerOpacity', {
            id: 'opacitySlider',
            xtype: "gx_opacityslider",
            layer: eval(layername),
            width: 100,
            x: 10,
            y: 10,
            increment: 5,
            aggressive: true,
            value: actual_opacity * 100,
            minValue: 0,
            maxValue: 100,
            plugins: Ext.create("GeoExt.slider.Tip", {
                getText: function (thumb) {
                    return Ext.String.format('Opacidad: {0}%', thumb.value);
                }
            })
        });
        //AGREGAMOS EL SLIDER RECIEN CREADO A LA BARRA (TOOLBAR)       
        var opacitySlider = Ext.getCmp('opacitySlider');
        var mapPanel_toolbar = Ext.getCmp('mapPanel_toolbar');
        //mapPanel_toolbar.add("-");
        mapPanel_toolbar.add(opacitySlider);
        eval(layername).setOpacity(actual_opacity);

    }



    Ext.getCmp('mapPanel_toolbar').doLayout();

}

function createPanelInfo(panelName, title, jsonString, headersArray) {


    /////////
    var cadena = jsonString;
    if (cadena === '["No se encontraron coincidencias"]') {
        eval(panelName + " = Ext.create('Ext.panel.Panel', {title: title,header: true,icon: 'images/new_icons/information.png',collapsed: true,collapsible: true,collapseDirection: 'top',collapseMode: 'header', width:320, flex: 1,items: [],bodyStyle: 'padding-left:0px'});");
    } else {
        eval(panelName + " = Ext.create('Ext.panel.Panel', {title: title,header: true,icon: 'images/new_icons/information.png',collapsed: false,collapsible: true,collapseDirection: 'top',collapseMode: 'header', width:320, flex: 1,items: [],bodyStyle: 'padding-left:0px'});");
    }
    ///////////////

    if (cadena === '["No se encontraron coincidencias"]') {
        eval(panelName + ".add({xtype: 'button',style: {backgroundColor: '#A12D2D',borderColor: 'transparent',margin: '5px'},text: '<span style=\"font-weight:bold; color:white;\">No se encontraron coincidencias</span>'});");

    } else {
        headersArray = headersArray.split(',');
        cadena = cadena.replace(/","/g, 'break');
        cadena = cadena.replace(/[\[\]']+/g, '');
        cadena = cadena.replace(/"/g, '');
        cadena = cadena.replace(/§/g, '°');
        cadena = cadena.replace(/¤/g, 'ñ');
        cadena = cadena.split("break");

        function formatString(string) {
            var firstLetter = string.charAt(0).toUpperCase();
            string = string.substring(1);
            string = string.toLowerCase();
            return firstLetter + string;
        }



        for (x = 0; x < cadena.length; x++) {
            cadena_elements = cadena[x].split("_");

            //VARIABLES DE LA BASE DE DATOS
            var text = "<span style='font-weight:bold; color:white;'>" + formatString(cadena_elements[0]) + "</span>";
            var tooltip = "";
            for (y = 1; y < cadena_elements.length; y++) {
                tooltip += "<b>" + headersArray[y - 1] + ": </b>" + formatString(cadena_elements[y]) + "<br>";
            }

            eval(panelName + ".add({xtype: 'button',style: {backgroundColor: '#787878',borderColor: 'transparent',margin: '5px'},text: text,tooltip: tooltip});");

        } //FIN DEL FOR
    }
}
</script>