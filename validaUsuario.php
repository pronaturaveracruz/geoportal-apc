<?php
error_reporting(0);
date_default_timezone_set("Mexico/General");
include "../includes/conexion.php";
$link = Conectarse();

$user = $_POST['user'];
$pass = $_POST['pass'];

$result = pg_query($link, "SELECT nombre_usuario,contrasena,nombre_completo FROM control_acceso
	LEFT JOIN plataforma ON control_acceso.id_plataforma=plataforma.id_plataforma
	LEFT JOIN usuario ON control_acceso.id_usuario=usuario.id_usuario
	WHERE control_acceso.id_plataforma='2' AND usuario.nombre_usuario='$user'");

if ($row = pg_fetch_assoc($result)) {
    if ($row["contrasena"] == $pass) {
        $nombre_completo = $row['nombre_completo'];
        session_start();
        $_SESSION['usuario'] = $user;
        echo json_encode(array("success" => true, "nombre_completo" => $nombre_completo));
    } else {
        $errorMsg = 'Contraseña incorrecta';
        echo json_encode(array("success" => false, "msg" => $errorMsg));
    }
} else {
    $errorMsg = 'El nombre de usuario es incorrecto!';
    echo json_encode(array("success" => false, "msg" => $errorMsg));
}

pg_free_result($result);
pg_close();
