<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Geoportal · Pronatura Veracruz AC</title>
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
    <!--Load ExtJS 5.1.0-->
    <script type="text/javascript" src="includes/ext-5.1.0/build/ext-all-debug.js"></script>
    <link rel="stylesheet" type="text/css"
        href="includes/ext-5.1.0/build/packages/ext-theme-crisp/build/resources/ext-theme-crisp-all-debug.css">
    <script type="text/javascript" src="includes/ext-5.1.0/build/packages/ext-locale/build/ext-locale-es.js"></script>
    <!--Load GeoExt 2.1.0 resources-->
    <link rel="stylesheet" type="text/css" href="includes/geoext2-2.1.0/resources/css/tree-neptune.css" />
    <link rel="stylesheet" type="text/css" href="includes/geoext2-2.1.0/resources/css/overviewmap.css" />
    <!-- Load OpenLayers 2.13.1-->
    <script src="includes/OpenLayers-2.13.1/OpenLayers.js"></script>
    <script src="includes/OpenLayers-2.13.1/lib/OpenLayers/Lang/es.js"></script>
    <script src="includes/OpenLayers-2.13.1/ScaleBar.js"></script>
    <script src="includes/OpenLayers-2.13.1/lib/OpenLayers/AnimatedCluster.js"></script>
    <link rel="stylesheet" type="text/css" href="includes/OpenLayers-2.13.1/scalebar-thin.css" />
    <!-- Load FontAwesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
        integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"
        integrity="sha256-7LkWEzqTdpEfELxcZZlS6wAx5Ff13zZ83lYO2/ujj7g=" crossorigin="anonymous"></script>
    <!--Loader map-->
    <script type="text/javascript" src="js/loader.js"></script>
</head>

<body>
    <div id="desc"></div>
    <?php include("js/map.php"); ?>
</body>

</html>