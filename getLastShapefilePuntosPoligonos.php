<?php
//error_reporting(0);
date_default_timezone_set("Mexico/General");
$fecha_actual = date("Ymd_His");
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

##FUNCIÓN PARA CREAR UN ARCHIVO ZIP
function create_zip($files = array(), $destination = '', $overwrite = false)
{
    if (file_exists($destination) && !$overwrite) {return false;}
    $valid_files = array();
    if (is_array($files)) {
        foreach ($files as $file) {
            if (file_exists($file)) {
                $valid_files[] = $file;
            }

        }
    }
    if (count($valid_files)) {
        $zip = new ZipArchive();
        if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }

        foreach ($valid_files as $file) {
            $file_wo_path = str_replace("downloads/lastShapefilePuntosPoligonos/", "", $file);
            $zip->addFile($file, $file_wo_path);
        }
        $zip->close();
        return file_exists($destination);
    } else {
        return false;
    }
}

##CONSULTA GENERAL
$select = "SELECT ok3.nombre_apc, ok3.hectareas, ok3.estado, ok3.municipio, ok3.localidad, ok3.fecha_certificacion, ok3.vigencia ";
$selectCount = "SELECT COUNT(*) ";
$consultaBase = "FROM( SELECT ok2.id_principal, ok2.figura_legal, trim(replace(ok2.nombre_completo,'  ', ' ')) AS nombre_completo, ok2.folio, ok2.nombre_apc, ok2.hectareas, ok2.hectareas_certificadas, ok2.estado, ok2.municipio, ok2.localidad, ok2.fecha_ingreso, ok2.estatus, ok2.fecha_certificacion, ok2.numero_sedema, ok2.motivo_no_procede, ok2.vegetacion, ok2.observaciones_vegetacion, ok2.especies, ok2.tecnico, ok2.descripcion_zona, ok2.nombre_proyecto, ok2.fase, ok2.vigencia, ok2.proteccion_legal, ok2.cuenta_plan_manejo, ok2.vigencia_plan_manejo, ok2.tenencia, ok2.ruta_doc_ingresados, ok2.ruta_fotografias, ok2.ruta_certificado, ok2.ruta_plan_de_manejo, ok2.organizacion, ok2.org_marca FROM ( SELECT ok.id_principal, ok.figura_legal, array_to_string(array_agg(ok.nombre_completo), ',') AS nombre_completo, ok.folio, ok.nombre_apc, ok.hectareas, ok.hectareas_certificadas, ok.estado, ok.municipio, ok.localidad, ok.fecha_ingreso, ok.estatus, ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion, ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca FROM ( SELECT apc_principal.id_principal, figura_legal.figura_legal, trim((propietario.nombre || ' ' || propietario.apat || ' ' || propietario.amat)) AS nombre_completo, apc_principal.folio, apc_principal.nombre_apc, apc_principal.hectareas, apc_principal.hectareas_certificadas, apc_principal.estado, apc_principal.municipio, apc_principal.localidad, apc_principal.fecha_ingreso, apc_principal.estatus, apc_principal.fecha_certificacion, apc_principal.numero_sedema, apc_principal.motivo_no_procede, resultado.vegetacion, apc_principal.observaciones_vegetacion, especies, (tecnico.nombre || ' ' || tecnico.apat || ' ' || tecnico.amat) AS tecnico, descripcion_zona, nombre_proyecto, fase, vigencia, proteccion_legal, cuenta_plan_manejo, vigencia_plan_manejo, tenencia, apc_principal.ruta_doc_ingresados, apc_principal.ruta_fotografias, apc_principal.ruta_certificado, apc_principal.ruta_plan_de_manejo, marca_sello.organizacion, (marca_sello.organizacion || ' - ' || marca_sello.marca) AS org_marca FROM ( SELECT resultante.id_apc, resultante.vegetacion, array_to_string(array_agg(categoria || ':' || nombre_cientifico), ',') AS especies FROM ( SELECT tv_apcs.id_apc, array_to_string(array_agg(categoria || ':' || tipo_vegetacion), ',') AS vegetacion FROM tv_apcs JOIN tipo_vegetacion ON tv_apcs.id_tipo_vegetacion = tipo_vegetacion.id_tipo_vegetacion GROUP BY tv_apcs.id_apc ORDER BY tv_apcs.id_apc) AS resultante LEFT JOIN objetivo_certificacion ON resultante.id_apc = objetivo_certificacion.id_apc GROUP BY resultante.id_apc, resultante.vegetacion ORDER BY resultante.id_apc ) AS resultado JOIN apc_principal ON resultado.id_apc = apc_principal.id_principal JOIN prop_apcs ON apc_principal.id_principal = prop_apcs.id_apc JOIN public.propietario ON prop_apcs.id_propietario = propietario.id_propietario LEFT JOIN public.tecnico ON apc_principal.tecnico = tecnico.id_tecnico LEFT JOIN concentrado_zona_proyecto ON apc_principal.id_czp = concentrado_zona_proyecto.id_czp LEFT JOIN zonas ON concentrado_zona_proyecto.id_zona = zonas.id_zona LEFT JOIN proyecto ON concentrado_zona_proyecto.id_proyecto = proyecto.id_proyecto LEFT JOIN marca_sello ON apc_principal.id_marca = marca_sello.id_marca LEFT JOIN figura_legal ON apc_principal.id_figura_legal = figura_legal.id_figura_legal ORDER BY propietario.nombre ASC ) AS ok GROUP BY ok.folio, ok.figura_legal, ok.id_principal, ok.nombre_apc, ok.hectareas, ok.hectareas_certificadas, ok.estado, ok.municipio, ok.localidad, ok.fecha_ingreso, ok.estatus, ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion, ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca ) AS ok2 )AS ok3 ";
$innerJoinPoints = "INNER JOIN shape_puntos_ccl ON ok3.nombre_apc = shape_puntos_ccl.nombre_apc AND ok3.folio = shape_puntos_ccl.folio AND ok3.nombre_completo = shape_puntos_ccl.propietari WHERE ok3.id_principal > 0 ";
$innerJoinPolygons = "INNER JOIN shape_poligonos_ccl_merge_final ON ok3.nombre_apc=shape_poligonos_ccl_merge_final.nombre_apc AND ok3.folio=shape_poligonos_ccl_merge_final.folio AND ok3.nombre_completo=shape_poligonos_ccl_merge_final.propietari WHERE ok3.id_principal > 0 AND ok3.figura_legal != 'Reserva municipal'";
#########################

$ogr2ogr = "ogr2ogr";
$proyeccion = "ccl";
$host = HOST_PG;
$user = USER_PG;
$frase = PASS_PG;
$nombre_archivo_puntos = "shape_puntos_" . $fecha_actual . "_" . $proyeccion;
$nombre_archivo_poligonos = "shape_poligonos_" . $fecha_actual . "_" . $proyeccion;

$destino_shapefile_puntos = PATH_FOR_SHAPEFILES_FOR_GEOPORTAL . $nombre_archivo_puntos;
$destino_shapefile_poligonos = PATH_FOR_SHAPEFILES_FOR_GEOPORTAL . $nombre_archivo_poligonos;

$consulta_puntos = $select . ", shape_puntos_ccl.geom AS geom_point " . $consultaBase . $innerJoinPoints;
$consulta_poligonos = $select . ", shape_poligonos_ccl_merge_final.geom AS geom_poly " . $consultaBase . $innerJoinPolygons;

/*
##DEBUG############
echo $ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_puntos . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"';
echo "\n\n" . $ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"';
###################
 */

#ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'lastShapefilePuntosPoligonos/'
$files = glob('downloads/lastShapefilePuntosPoligonos/*');
foreach ($files as $file) {
    if (is_file($file)) {
        unlink($file);
    }
}

shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_puntos . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');

#CREAR EL ARCHIVO ZIP
#1) AGREGAMOS A UN ARREGLO LA LISTA DE ARCHIVOS
$arreglo_archivos = array();
foreach (glob('downloads/lastShapefilePuntosPoligonos/' . $nombre_archivo_puntos . ".*") as $nombre_fichero) {
    array_push($arreglo_archivos, $nombre_fichero);
}
foreach (glob('downloads/lastShapefilePuntosPoligonos/' . $nombre_archivo_poligonos . ".*") as $nombre_fichero) {
    array_push($arreglo_archivos, $nombre_fichero);
}

#2) DEFINIMOS NOMBRE DEL ARCHIVO Y CREAMOS EL ZIP CON LA FUNCIÓN 'create_zip'
//$nombre_archivo_zip = "lastShapefilePuntosPoligonos/shape_apcs_puntos_poligonos_" . $fecha_actual . "_" . $proyeccion . ".zip";
$nombre_archivo_zip = "shape_apcs_puntos_poligonos_" . $fecha_actual . "_" . $proyeccion . ".zip";
$ruta_final = "downloads/lastShapefilePuntosPoligonos/" . $nombre_archivo_zip;
$result = create_zip($arreglo_archivos, $ruta_final);

if ($result) {
    if (!$ruta_final) {
        echo "error";
    } else {
        echo $ruta_final . "," . $nombre_archivo_zip;
    }
}
