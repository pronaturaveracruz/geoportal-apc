<?php
//error_reporting(0);
date_default_timezone_set("Mexico/General");
$fecha_actual = date("Ymd_His");
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();
#OBTENEMOS LOS REQUEST
$callback = $_REQUEST['callback'];
$limit = $_REQUEST['limit'];
$offset = $_REQUEST['start'];
$sort = $_REQUEST['sort'];
$dir = $_REQUEST['dir'];
$op_fc = $_REQUEST['op_fc'];

#AÑADIR AL EXTRAPARAMS DEL JSONP (JS)
$tipo_query = $_REQUEST['tipo_query']; #[NORMAL - ALTITUD]
$altitud_inicial = $_REQUEST['altitud_inicial'];
$altitud_final = $_REQUEST['altitud_final'];

if (isset($_REQUEST['table']) && $_REQUEST['table'] == 'shape_poligonos') {
    #NINGUNA ACCIÓN
} else {
    if (isset($_REQUEST['filter'])) {
        $filter = $_REQUEST['filter'];
        $filtro = explode(",", $filter);
        $cuantas_comas = count($filtro);

        for ($i = 0; $i < $cuantas_comas; $i++) {
            $ultimo = $cuantas_comas - 1;
            if ($i == 0) {
                $filtro[$i] = str_replace('[{"property":"', "", $filtro[$i]);
                $filtro[$i] = str_replace('"', "", $filtro[$i]);
            }
            if ($i == $ultimo) {
                $filtro[$i] = str_replace('"value":"', "", $filtro[$i]);
                $filtro[$i] = str_replace('"value":', "", $filtro[$i]); //AGREGADO
                $filtro[$i] = str_replace('"}]', "", $filtro[$i]);
                $filtro[$i] = str_replace('}]', "", $filtro[$i]); //AGREGADO
            } else if ($i % 2 == 0) {
                $filtro[$i] = str_replace('{"property":"', "", $filtro[$i]);
                $filtro[$i] = str_replace('"', "", $filtro[$i]);
            } else if ($i % 2 != 0) {
                $filtro[$i] = str_replace('"value":"', "", $filtro[$i]);
                $filtro[$i] = str_replace('"value":', "", $filtro[$i]); //AGREGADO
                $filtro[$i] = str_replace('"}', "", $filtro[$i]);
                $filtro[$i] = str_replace('}', "", $filtro[$i]); //AGREGADO
            }
        }

        $where = " gid>0";

        for ($i = 0; $i < $cuantas_comas; $i += 2) {
            $parametro = $filtro[$i + 1];
            if ($filtro[$i] == 'municipio_principal') {
                if ($parametro != '') {
                    $where .= " AND union_shapes.municipio_principal ~*'.*" . $parametro . ".*'";
                }

            }
            if ($filtro[$i] == 'localidad_principal') {
                if ($parametro != '') {
                    $where .= " AND union_shapes.localidad_principal ~*'.*" . $parametro . ".*'";
                }

            }
            if ($filtro[$i] == 'nombre_apc_principal') {
                if ($parametro != '') {
                    $where .= " AND union_shapes.nombre_apc_principal ~*'.*" . $parametro . ".*'";
                }

            }
            if ($filtro[$i] == 'vigencia_principal') {
                if ($parametro != '') {
                    $where .= " AND union_shapes.vigencia_principal =" . $parametro;
                }

            }
            if ($filtro[$i] == 'plan_manejo_principal') {
                if ($parametro != '' && $parametro != 'null') {
                    $where .= " AND union_shapes.plan_manejo_principal ='" . $parametro . "'";
                }

            }
            if ($filtro[$i] == 'tenencia_principal') {
                if ($parametro != '' && $parametro != 'null') {
                    $where .= " AND union_shapes.tenencia_principal ='" . $parametro . "'";
                }

            }
            if ($filtro[$i] == 'fecha_certificacion_principal') {
                if ($op_fc != 'Rango') {
                    if ($op_fc == 'Igual') {
                        $operador = '=';
                    } else if ($op_fc == 'Mayor') {
                        $operador = '>';
                    } else if ($op_fc == 'Menor') {
                        $operador = '<';
                    }

                    if ($parametro != '' && $op_fc != 'null') {
                        $where .= " AND union_shapes.fecha_certificacion_principal " . $operador . "'" . $parametro . "'";
                    }

                } else if ($op_fc == 'Rango') {
                    $fc_rango_de = $parametro;
                }
            }
            if ($filtro[$i] == 'fecha_certificacion_principal2') {
                if ($op_fc == 'Rango') {
                    $fc_rango_hasta = $parametro;
                    $where .= " AND union_shapes.fecha_certificacion_principal BETWEEN '" . $fc_rango_de . "' AND '" . $fc_rango_hasta . "'";
                }
            }
            if ($filtro[$i] == 'altitud_') {

            }

        }

        #QUERY CONDICIONADO
        if ($tipo_query == 'normal') {
            $query = "SELECT *
				FROM
				(
					SELECT
						shape_puntos_ccl.gid AS gid,
						shape_puntos_ccl.folio AS folio_capa,
						shape_puntos_ccl.geom AS geom,
						apc_principal.nombre_apc AS nombre_apc_principal,
						apc_principal.estado     AS estado_principal,
						apc_principal.municipio  AS municipio_principal,
						apc_principal.localidad AS localidad_principal,
						apc_principal.vigencia AS vigencia_principal,
						apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
						apc_principal.tenencia AS tenencia_principal,
						apc_principal.fecha_certificacion AS fecha_certificacion_principal,
						box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
					FROM shape_puntos_ccl
					INNER JOIN apc_principal ON shape_puntos_ccl.folio=apc_principal.folio AND shape_puntos_ccl.nombre_apc = apc_principal.nombre_apc
					GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
					UNION ALL
					SELECT
						shape_poligonos_ccl_merge_final.gid AS gid,
						shape_poligonos_ccl_merge_final.folio AS folio_capa,
						shape_poligonos_ccl_merge_final.geom AS geom,
						apc_principal.nombre_apc AS nombre_apc_principal,
						apc_principal.estado     AS estado_principal,
						apc_principal.municipio  AS municipio_principal,
						apc_principal.localidad AS localidad_principal,
						apc_principal.vigencia AS vigencia_principal,
						apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
						apc_principal.tenencia AS tenencia_principal,
						apc_principal.fecha_certificacion AS fecha_certificacion_principal,
						box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
					FROM shape_poligonos_ccl_merge_final
					INNER JOIN apc_principal ON shape_poligonos_ccl_merge_final.folio=apc_principal.folio AND shape_poligonos_ccl_merge_final.nombre_apc = apc_principal.nombre_apc
					GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
					ORDER BY " . $sort . " " . $dir .
                ")AS union_shapes" .
                " WHERE " . $where .
                " LIMIT " . $limit . " OFFSET " . $offset;

            $query_cuantas = "SELECT COUNT(*) AS count
				FROM
				(
					SELECT
						shape_puntos_ccl.gid AS gid,
						shape_puntos_ccl.folio AS folio_capa,
						shape_puntos_ccl.geom AS geom,
						apc_principal.nombre_apc AS nombre_apc_principal,
						apc_principal.estado     AS estado_principal,
						apc_principal.municipio  AS municipio_principal,
						apc_principal.localidad AS localidad_principal,
						apc_principal.vigencia AS vigencia_principal,
						apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
						apc_principal.tenencia AS tenencia_principal,
						apc_principal.fecha_certificacion AS fecha_certificacion_principal,
						box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
					FROM shape_puntos_ccl
					INNER JOIN apc_principal ON shape_puntos_ccl.folio=apc_principal.folio AND shape_puntos_ccl.nombre_apc = apc_principal.nombre_apc
					GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
					UNION ALL
					SELECT
						shape_poligonos_ccl_merge_final.gid AS gid,
						shape_poligonos_ccl_merge_final.folio AS folio_capa,
						shape_poligonos_ccl_merge_final.geom AS geom,
						apc_principal.nombre_apc AS nombre_apc_principal,
						apc_principal.estado     AS estado_principal,
						apc_principal.municipio  AS municipio_principal,
						apc_principal.localidad AS localidad_principal,
						apc_principal.vigencia AS vigencia_principal,
						apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
						apc_principal.tenencia AS tenencia_principal,
						apc_principal.fecha_certificacion AS fecha_certificacion_principal,
						box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
					FROM shape_poligonos_ccl_merge_final
					INNER JOIN apc_principal ON shape_poligonos_ccl_merge_final.folio=apc_principal.folio AND shape_poligonos_ccl_merge_final.nombre_apc = apc_principal.nombre_apc
					GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
				)AS union_shapes" .
                " WHERE " . $where;
        } else if ($tipo_query == 'altitud') {
            $where .= " AND msnm BETWEEN '" . $altitud_inicial . "' AND '" . $altitud_final . "'";

            $query = "SELECT folio, nombre_apc_principal, municipio_principal, localidad_principal, vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal, box2d(ST_Extent(ST_Transform(q3.geom, 3857))) AS bbox
				FROM
				(
					SELECT DISTINCT folio, nombre_apc_principal, municipio_principal, localidad_principal, vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal, geom
					FROM
					(
						SELECT *
						FROM
						(
							SELECT
								q1.gid,
								q1.folio,
								q1.nombre_apc,
								q1.msnm,
								q1.geom,
								apc_principal.nombre_apc AS nombre_apc_principal,
								apc_principal.estado     AS estado_principal,
								apc_principal.municipio  AS municipio_principal,
								apc_principal.localidad AS localidad_principal,
								apc_principal.vigencia AS vigencia_principal,
								apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
								apc_principal.tenencia AS tenencia_principal,
								apc_principal.fecha_certificacion AS fecha_certificacion_principal
							FROM
							(
								SELECT
									gid,
									folio,
									nombre_apc,
									ST_Value(rast, geom) AS msnm,
									geom
								FROM shape_puntos_ccl, raster_mx
								WHERE ST_Intersects(rast, geom)
							) AS q1
							INNER JOIN apc_principal ON q1.folio=apc_principal.folio AND q1.nombre_apc = apc_principal.nombre_apc
							GROUP BY q1.folio, q1.nombre_apc, q1.msnm, q1.geom, q1.gid, apc_principal.nombre_apc, apc_principal.estado, apc_principal.municipio, apc_principal.localidad, apc_principal.vigencia, apc_principal.cuenta_plan_manejo, apc_principal.tenencia, apc_principal.fecha_certificacion
							UNION ALL
							SELECT
								q1.gid,
								q1.folio,
								q1.nombre_apc,
								q1.msnm,
								q1.geom,
								apc_principal.nombre_apc AS nombre_apc_principal,
								apc_principal.estado     AS estado_principal,
								apc_principal.municipio  AS municipio_principal,
								apc_principal.localidad AS localidad_principal,
								apc_principal.vigencia AS vigencia_principal,
								apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
								apc_principal.tenencia AS tenencia_principal,
								apc_principal.fecha_certificacion AS fecha_certificacion_principal
							FROM
							(
								SELECT
									gid,
									folio,
									nombre_apc,
									(ST_ValueCount(rast)).value AS msnm,
									geom
								FROM shape_poligonos_ccl_merge_final, raster_mx
								WHERE ST_Intersects(rast, geom)
							) AS q1
							INNER JOIN apc_principal ON q1.folio=apc_principal.folio AND q1.nombre_apc = apc_principal.nombre_apc
							GROUP BY q1.folio, q1.nombre_apc, q1.msnm, q1.geom, q1.gid, apc_principal.nombre_apc, apc_principal.estado, apc_principal.municipio, apc_principal.localidad, apc_principal.vigencia, apc_principal.cuenta_plan_manejo, apc_principal.tenencia, apc_principal.fecha_certificacion
						) AS union_shapes
						WHERE" . $where .
                "ORDER BY msnm ASC
					) AS q2
				) AS q3
				GROUP BY folio, nombre_apc_principal, municipio_principal, localidad_principal, vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
				ORDER BY " . $sort . " " . $dir .
                " LIMIT " . $limit . " OFFSET " . $offset;

            $query_cuantas = "SELECT COUNT(*)
					FROM
					(
						SELECT folio, nombre_apc_principal, municipio_principal, localidad_principal, vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal, box2d(ST_Extent(ST_Transform(q3.geom, 3857))) AS bbox
						FROM
						(
							SELECT DISTINCT folio, nombre_apc_principal, municipio_principal, localidad_principal, vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal, geom
							FROM
							(
								SELECT *
								FROM
								(
									SELECT
										q1.gid,
										q1.folio,
										q1.nombre_apc,
										q1.msnm,
										q1.geom,
										apc_principal.nombre_apc AS nombre_apc_principal,
										apc_principal.estado     AS estado_principal,
										apc_principal.municipio  AS municipio_principal,
										apc_principal.localidad AS localidad_principal,
										apc_principal.vigencia AS vigencia_principal,
										apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
										apc_principal.tenencia AS tenencia_principal,
										apc_principal.fecha_certificacion AS fecha_certificacion_principal
									FROM
									(
										SELECT
											gid,
											folio,
											nombre_apc,
											ST_Value(rast, geom) AS msnm,
											geom
										FROM shape_puntos_ccl, raster_mx
										WHERE ST_Intersects(rast, geom)
									) AS q1
									INNER JOIN apc_principal ON q1.folio=apc_principal.folio AND q1.nombre_apc = apc_principal.nombre_apc
									GROUP BY q1.folio, q1.nombre_apc, q1.msnm, q1.geom, q1.gid, apc_principal.nombre_apc, apc_principal.estado, apc_principal.municipio, apc_principal.localidad, apc_principal.vigencia, apc_principal.cuenta_plan_manejo, apc_principal.tenencia, apc_principal.fecha_certificacion
									UNION ALL
									SELECT
										q1.gid,
										q1.folio,
										q1.nombre_apc,
										q1.msnm,
										q1.geom,
										apc_principal.nombre_apc AS nombre_apc_principal,
										apc_principal.estado     AS estado_principal,
										apc_principal.municipio  AS municipio_principal,
										apc_principal.localidad AS localidad_principal,
										apc_principal.vigencia AS vigencia_principal,
										apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
										apc_principal.tenencia AS tenencia_principal,
										apc_principal.fecha_certificacion AS fecha_certificacion_principal
									FROM
									(
										SELECT
											gid,
											folio,
											nombre_apc,
											(ST_ValueCount(rast)).value AS msnm,
											geom
										FROM shape_poligonos_ccl_merge_final, raster_mx
										WHERE ST_Intersects(rast, geom)
									) AS q1
									INNER JOIN apc_principal ON q1.folio=apc_principal.folio AND q1.nombre_apc = apc_principal.nombre_apc
									GROUP BY q1.folio, q1.nombre_apc, q1.msnm, q1.geom, q1.gid, apc_principal.nombre_apc, apc_principal.estado, apc_principal.municipio, apc_principal.localidad, apc_principal.vigencia, apc_principal.cuenta_plan_manejo, apc_principal.tenencia, apc_principal.fecha_certificacion
								) AS union_shapes
								WHERE" . $where .
                "ORDER BY msnm ASC
							) AS q2
						) AS q3
						GROUP BY folio, nombre_apc_principal, municipio_principal, localidad_principal, vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
					) AS q4";
        }

    } else #CONSULTA AL INICIALIZAR EL GRID (SIN FILTROS)
    {
        $query = "SELECT *
			FROM
			(
				SELECT
					shape_puntos_ccl.gid AS gid,
					shape_puntos_ccl.folio AS folio_capa,
					shape_puntos_ccl.geom AS geom,
					apc_principal.nombre_apc AS nombre_apc_principal,
					apc_principal.estado     AS estado_principal,
					apc_principal.municipio  AS municipio_principal,
					apc_principal.localidad AS localidad_principal,
					apc_principal.vigencia AS vigencia_principal,
					apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
					apc_principal.tenencia AS tenencia_principal,
					apc_principal.fecha_certificacion AS fecha_certificacion_principal,
					box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
				FROM shape_puntos_ccl
				INNER JOIN apc_principal ON shape_puntos_ccl.folio=apc_principal.folio AND shape_puntos_ccl.nombre_apc = apc_principal.nombre_apc
				GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
				UNION ALL
				SELECT
					shape_poligonos_ccl_merge_final.gid AS gid,
					shape_poligonos_ccl_merge_final.folio AS folio_capa,
					shape_poligonos_ccl_merge_final.geom AS geom,
					apc_principal.nombre_apc AS nombre_apc_principal,
					apc_principal.estado     AS estado_principal,
					apc_principal.municipio  AS municipio_principal,
					apc_principal.localidad AS localidad_principal,
					apc_principal.vigencia AS vigencia_principal,
					apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
					apc_principal.tenencia AS tenencia_principal,
					apc_principal.fecha_certificacion AS fecha_certificacion_principal,
					box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
				FROM shape_poligonos_ccl_merge_final
				INNER JOIN apc_principal ON shape_poligonos_ccl_merge_final.folio=apc_principal.folio AND shape_poligonos_ccl_merge_final.nombre_apc = apc_principal.nombre_apc
				GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
				ORDER BY " . $sort . " " . $dir .
            ")AS union_shapes" .
            " LIMIT " . $limit . " OFFSET " . $offset;

        $query_cuantas = "SELECT COUNT(*) AS count
			FROM
			(
				SELECT
					shape_puntos_ccl.gid AS gid,
					shape_puntos_ccl.folio AS folio_capa,
					shape_puntos_ccl.geom AS geom,
					apc_principal.nombre_apc AS nombre_apc_principal,
					apc_principal.estado     AS estado_principal,
					apc_principal.municipio  AS municipio_principal,
					apc_principal.localidad AS localidad_principal,
					apc_principal.vigencia AS vigencia_principal,
					apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
					apc_principal.tenencia AS tenencia_principal,
					apc_principal.fecha_certificacion AS fecha_certificacion_principal,
					box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
				FROM shape_puntos_ccl
				INNER JOIN apc_principal ON shape_puntos_ccl.folio=apc_principal.folio AND shape_puntos_ccl.nombre_apc = apc_principal.nombre_apc
				GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
				UNION ALL
				SELECT
					shape_poligonos_ccl_merge_final.gid AS gid,
					shape_poligonos_ccl_merge_final.folio AS folio_capa,
					shape_poligonos_ccl_merge_final.geom AS geom,
					apc_principal.nombre_apc AS nombre_apc_principal,
					apc_principal.estado     AS estado_principal,
					apc_principal.municipio  AS municipio_principal,
					apc_principal.localidad AS localidad_principal,
					apc_principal.vigencia AS vigencia_principal,
					apc_principal.cuenta_plan_manejo AS plan_manejo_principal,
					apc_principal.tenencia AS tenencia_principal,
					apc_principal.fecha_certificacion AS fecha_certificacion_principal,
					box2d(ST_Extent(ST_Transform(geom, 3857))) AS bbox
				FROM shape_poligonos_ccl_merge_final
				INNER JOIN apc_principal ON shape_poligonos_ccl_merge_final.folio=apc_principal.folio AND shape_poligonos_ccl_merge_final.nombre_apc = apc_principal.nombre_apc
				GROUP BY nombre_apc_principal,gid,folio_capa,geom,estado_principal, municipio_principal, localidad_principal,vigencia_principal, plan_manejo_principal, tenencia_principal, fecha_certificacion_principal
			)AS union_shapes";
    }
}

$res = pg_query($link, $query) or die('Query failed: ' . pg_last_error());
$result = pg_query($link, $query_cuantas) or die('Query failed: ' . pg_last_error());
$row = pg_fetch_assoc($result);
$count = $row['count'];
$items = array();

while ($row = pg_fetch_assoc($res)) {
    #PONEMOS LAS COMAS FALTANTES EN BBOX
    $bbox = $row['bbox'];
    $bbox = str_replace(" ", ",", $bbox);
    $bbox = str_replace("BOX(", "", $bbox);
    $bbox = str_replace(")", "", $bbox);
    $row['bbox'] = $bbox;
    array_push($items, $row);
}

$rows = $items;

if ($callback) {
    header('Content-Type: text/javascript');
    echo $callback . '(' . "{\"totalCount\":\"$count\",\"topics\":" . json_encode($rows) . "}" . ');';
} else {
    header('Content-Type: application/x-json');
    echo "{\"totalCount\":\"$count\",\"topics\":" . json_encode($rows) . "}";
}
