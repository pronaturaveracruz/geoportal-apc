<?php
error_reporting(0);
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

function capitalizarCadena($string)
{
    $primeraLetra = mb_convert_case(mb_substr($string, 0, 1, "UTF-8"), MB_CASE_TITLE, "UTF-8");
    $restoCadena = mb_convert_case(mb_substr($string, 1, mb_strlen($string), "UTF-8"), MB_CASE_LOWER, "UTF-8");
    return $primeraLetra . $restoCadena;
}

$callback = $_REQUEST['callback'];
$nombre_apc = $_REQUEST['nombre_apc'];
$folio = $_REQUEST['folio'];
$hectareas = $_REQUEST['hectareas'];
$estado = $_REQUEST['estado'];
$municipio = $_REQUEST['municipio'];
$vigencia = $_REQUEST['vigencia'];
$fecha_certificacion = $_REQUEST['fecha_certificacion'];
$numero_sedema = $_REQUEST['numero_sedema'];
$cuenta_plan_manejo = $_REQUEST['cuenta_plan_manejo'];

#BUSCAR PRIMERO EN 'shape_puntos_ccl' EL FOLIO Y EL NOMBRE DEL APC
$sql_cuantos_puntos = "SELECT COUNT(*) FROM shape_puntos_ccl WHERE nombre_apc = '" . $nombre_apc . "' AND folio = '" . $folio . "'";
$query_cuantos_puntos = pg_query($link, $sql_cuantos_puntos);
while ($row = pg_fetch_row($query_cuantos_puntos)) {
    $cuantos_puntos = $row[0];
}
if ($cuantos_puntos > 0) {
    //EJCUTAR CONSULTA EN SHAPE_PUNTOS_CCL
    $query_geom_punto = pg_query($link, "SELECT ST_AsText(geom)
	FROM shape_puntos_ccl
	WHERE nombre_apc = '$nombre_apc' AND folio = '$folio'");
    $row = pg_fetch_row($query_geom_punto);
    $geom_text = $row[0];
    $tipo_geom = 'Point';
} else {
    //EJECUTAR CONSULTA EN SHAPE_POLIGONOS_CCL
    $sql_cuantos_poligonos = "SELECT COUNT(*) FROM shape_poligonos_ccl_merge_final WHERE nombre_apc = '" . $nombre_apc . "' AND folio = '" . $folio . "'";
    $query_cuantos_poligonos = pg_query($link, $sql_cuantos_poligonos);
    while ($row = pg_fetch_row($query_cuantos_poligonos)) {
        $cuantos_poligonos = $row[0];
    }
    if ($cuantos_poligonos > 0) {
        //EJECUTAR CONSULTA EN SHAPE_PUNTOS_CCL
        $query_geom_punto = pg_query($link, "SELECT ST_AsText(geom)
		FROM shape_poligonos_ccl_merge_final
		WHERE nombre_apc = '$nombre_apc' AND folio = '$folio'");
        $row = pg_fetch_row($query_geom_punto);
        $geom_text = $row[0];
        $tipo_geom = 'Polygon';
    }
}

#EJECUTAR INTERSECCIONES CON LA GEOMETRIA OBTENIDA
##RUTAS DE FOTOS Y PLAN DE MANEJO
$query_rutas = pg_query($link, "SELECT ruta_fotografias, ruta_plan_de_manejo FROM apc_principal WHERE nombre_apc = '" . $nombre_apc . "' AND folio = '" . $folio . "'");
$items_ruta_fotografias = array();
$items_ruta_plan_de_manejo = array();
while ($row = pg_fetch_assoc($query_rutas)) {
    $ruta_fotografias = $row['ruta_fotografias'];
    $ruta_plan_de_manejo = $row['ruta_plan_de_manejo'];
    array_push($items_ruta_fotografias, $ruta_fotografias);
    array_push($items_ruta_plan_de_manejo, $ruta_plan_de_manejo);
}

#ALTITUD
if ($tipo_geom == 'Point') {
    $qalt = "SELECT ST_Value(rast, geom) AS msnm
		FROM
		(
			SELECT folio, nombre_apc, geom
			FROM shape_puntos_ccl
			WHERE folio='$folio' AND nombre_apc='$nombre_apc'
			ORDER BY nombre_apc
		) AS q1, raster_mx
		WHERE ST_Intersects(rast, geom)";
} else if ($tipo_geom == 'Polygon') {
    $qalt = "SELECT MAX(msnm), MIN(msnm)
		FROM
		(
			SELECT DISTINCT folio, nombre_apc , geom, (ST_ValueCount(rast)).value AS msnm
			FROM
			(
				SELECT folio, nombre_apc, geom
				FROM shape_poligonos_ccl_merge_final
				--WHERE afasdf IS NULL
				ORDER BY nombre_apc
			) AS q1, raster_mx
			WHERE ST_Intersects(rast, geom)
			ORDER BY nombre_apc ASC
		) AS q2
		WHERE folio='$folio' AND nombre_apc = '$nombre_apc'";
}
$query_altitud = pg_query($link, $qalt);
$items_altitud = array();
while ($row = pg_fetch_row($query_altitud)) {
    if ($tipo_geom == 'Point') {
        $cadena = $row[0];
    } else if ($tipo_geom == 'Polygon') {
        $cadena = $row[0] . "_" . $row[1];
    }

    array_push($items_altitud, $cadena);
}
if (empty($items_altitud)) {
    array_push($items_altitud, "No se encontraron coincidencias");
}

##LOCALIDADES MAS CERCANAS (500 METROS)(ST_DWITHIN)
$query_intersects_localidades = pg_query($link, "SELECT DISTINCT geom, nom_loc, nom_mun, nom_ent
FROM shape_localidades_ccl
WHERE ST_DWithin(shape_localidades_ccl.geom, ST_GeomFromText(
		'$geom_text',
		48402
	), 500) ORDER BY nom_loc ASC");
$items_localidades = array();
while ($row = pg_fetch_assoc($query_intersects_localidades)) {
    $cadena = capitalizarCadena($row['nom_loc']) . "_" . capitalizarCadena($row['nom_mun']) . "_" . capitalizarCadena($row['nom_ent']);
    array_push($items_localidades, $cadena);
}
if (empty($items_localidades)) {
    array_push($items_localidades, "No se encontraron coincidencias");
}

##VIALIDADES
$query_intersects_vialidades = pg_query($link, "SELECT DISTINCT tipo, entidad, num_vias, derecho, jurisdic, condicion
	FROM shape_vialidades_ccl
	WHERE ST_Intersects (shape_vialidades_ccl.geom,ST_GeomFromText('$geom_text',48402))
	ORDER BY tipo ASC");
$items_vialidades = array();
while ($row = pg_fetch_assoc($query_intersects_vialidades)) {
    $cadena = capitalizarCadena($row['tipo']) . "_" . capitalizarCadena($row['entidad']) . "_" . capitalizarCadena($row['num_vias']) . "_" . capitalizarCadena($row['derecho']) . "_" . capitalizarCadena($row['jurisdic']) . "_" . capitalizarCadena($row['condicion']);
    array_push($items_vialidades, $cadena);

}
if (empty($items_vialidades)) {
    array_push($items_vialidades, "No se encontraron coincidencias");
}

##EDAFOLOGIA
$query_intersects_edafologia = pg_query($link, "SELECT DISTINCT descripcio, desc_tex, desc_fasfi, desc_faqui
FROM shape_edafologia_ccl
WHERE ST_Intersects (shape_edafologia_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY descripcio ASC");
$items_edafologia = array();
while ($row = pg_fetch_assoc($query_intersects_edafologia)) {
    $cadena = capitalizarCadena($row['descripcio']) . "_" . capitalizarCadena($row['desc_tex']) . "_" . capitalizarCadena($row['desc_fasfi']) . "_" . capitalizarCadena($row['desc_faqui']);
    array_push($items_edafologia, $cadena);
}
if (empty($items_edafologia)) {
    array_push($items_edafologia, "No se encontraron coincidencias");
}

##GEOLOGIA
$query_intersects_geologia = pg_query($link, "SELECT DISTINCT clave, tipo_ley, entidad_le, era_ley, periodo_le, epoca_ley, origen_ley
FROM shape_geologia_ccl
WHERE ST_Intersects (shape_geologia_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY tipo_ley ASC");
$items_geologia = array();
while ($row = pg_fetch_assoc($query_intersects_geologia)) {
    $cadena = $row['clave'] . "_" . capitalizarCadena($row['tipo_ley']) . "_" . capitalizarCadena($row['entidad_le']) . "_" . capitalizarCadena($row['era_ley']) . "_" . capitalizarCadena($row['periodo_le']) . "_" . capitalizarCadena($row['epoca_ley']) . "_" . capitalizarCadena($row['origen_ley']);
    array_push($items_geologia, $cadena);
}
if (empty($items_geologia)) {
    array_push($items_geologia, "No se encontraron coincidencias");
}

##CLIMAS
$query_intersects_climas = pg_query($link, "SELECT DISTINCT shape_climas_ccl.clima_tipo, shape_climas_ccl.des_tem, shape_climas_ccl.desc_prec
FROM shape_climas_ccl
WHERE ST_Intersects (shape_climas_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY clima_tipo ASC");
$items_climas = array();
while ($row = pg_fetch_assoc($query_intersects_climas)) {
    $cadena = $row['clima_tipo'] . "_" . $row['des_tem'] . "_" . $row['desc_prec'];
    array_push($items_climas, $cadena);
}
if (empty($items_climas)) {
    array_push($items_climas, "No se encontraron coincidencias");
}

##CUENCAS
$query_intersects_cuencas = pg_query($link, "SELECT DISTINCT shape_cuencas_ccl.toponimo, shape_cuencas_ccl.tipo_cuen, shape_cuencas_ccl.reg_hid, shape_cuencas_ccl.sub_hid, shape_cuencas_ccl.tipo_dren
FROM shape_cuencas_ccl
WHERE ST_Intersects (shape_cuencas_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY toponimo ASC");
$items_cuencas = array();
while ($row = pg_fetch_assoc($query_intersects_cuencas)) {
    $cadena = $row['toponimo'] . "_" . $row['tipo_cuen'] . "_" . $row['reg_hid'] . "_" . $row['sub_hid'] . "_" . $row['tipo_dren'];
    array_push($items_cuencas, $cadena);
}
if (empty($items_cuencas)) {
    array_push($items_cuencas, "No se encontraron coincidencias");
}

##ZONAS FUNCIONALES
$query_intersects_zonas_funcionales = pg_query($link, "SELECT DISTINCT zonalt_ley, cuenca_nom, reg_hidro, subreg_hid
FROM shape_zonas_funcionales_ccl
WHERE ST_Intersects (shape_zonas_funcionales_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY zonalt_ley ASC");
$items_zonas_funcionales = array();
while ($row = pg_fetch_assoc($query_intersects_zonas_funcionales)) {
    $cadena = $row['zonalt_ley'] . "_" . $row['cuenca_nom'] . "_" . $row['reg_hidro'] . "_" . $row['subreg_hid'];
    array_push($items_zonas_funcionales, $cadena);
}
if (empty($items_zonas_funcionales)) {
    array_push($items_zonas_funcionales, "No se encontraron coincidencias");
}

##UMAS
$query_intersects_umas = pg_query($link, "SELECT DISTINCT clave, nombre_uma, vigencia, propietari
FROM shape_umas_ccl
WHERE ST_Intersects (shape_umas_ccl.geom,ST_GeomFromText('POINT(3034968.55280787 843400.850145595)',48402))
ORDER BY clave ASC");
$items_umas = array();
while ($row = pg_fetch_assoc($query_intersects_umas)) {
    $cadena = $row['clave'] . "_" . $row['nombre_uma'] . "_" . $row['vigencia'] . "_" . $row['propietari'];
    array_push($items_umas, $cadena);
}
if (empty($items_umas)) {
    array_push($items_umas, "No se encontraron coincidencias");
}

##PAGO POR SERVICIOS AMBIENTALES
$query_psa = pg_query($link, "SELECT DISTINCT modalidad, anyo, superficie, monto_tota, estado, municipio
FROM shape_pago_servicios_ambientales_ccl
WHERE ST_Intersects (shape_pago_servicios_ambientales_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY modalidad ASC");
$items_psa = array();
while ($row = pg_fetch_assoc($query_psa)) {
    $cadena = $row['modalidad'] . "_" . $row['anyo'] . "_" . $row['superficie'] . "_" . $row['monto_tota'] . "_" . $row['estado'] . "_" . $row['municipio'];
    array_push($items_psa, $cadena);
}
if (empty($items_psa)) {
    array_push($items_psa, "No se encontraron coincidencias");
}

##PRECIPITACIÓN TOTAL ANUAL
$query_intersects_precipitacion_total_anual = pg_query($link, "SELECT DISTINCT shape_precipitacion_total_anual_ccl.preci_rang
FROM shape_precipitacion_total_anual_ccl
WHERE ST_Intersects (shape_precipitacion_total_anual_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY preci_rang ASC");
$items_pta = array();
while ($row = pg_fetch_assoc($query_intersects_precipitacion_total_anual)) {
    $cadena = $row['preci_rang'];
    array_push($items_pta, $cadena);
}
if (empty($items_pta)) {
    array_push($items_pta, "No se encontraron coincidencias");
}

##AICAS
$query_intersects_precipitacion_total_anual = pg_query($link, "SELECT DISTINCT shape_aicas_ccl.nombre
FROM shape_aicas_ccl
WHERE ST_Intersects (shape_aicas_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_aicas = array();
while ($row = pg_fetch_assoc($query_intersects_precipitacion_total_anual)) {
    $cadena = $row['nombre'];
    array_push($items_aicas, $cadena);
}
if (empty($items_aicas)) {
    array_push($items_aicas, "No se encontraron coincidencias");
}

##REGIONES TERRESTRES PRIORITARIAS
$query_intersects_rtp = pg_query($link, "SELECT DISTINCT shape_regiones_terre_prioritarias_ccl.nombre
FROM shape_regiones_terre_prioritarias_ccl
WHERE ST_Intersects (shape_regiones_terre_prioritarias_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_rtp = array();
while ($row = pg_fetch_assoc($query_intersects_rtp)) {
    $cadena = $row['nombre'];
    array_push($items_rtp, $cadena);
}
if (empty($items_rtp)) {
    array_push($items_rtp, "No se encontraron coincidencias");
}

##REGIONES HIDROLOGICAS PRIORITARIAS
$query_intersects_rhp = pg_query($link, "SELECT DISTINCT shape_regiones_hidro_prioritarias_ccl.nombre, shape_regiones_hidro_prioritarias_ccl.region, shape_regiones_hidro_prioritarias_ccl.biodiv, shape_regiones_hidro_prioritarias_ccl.amenaza, shape_regiones_hidro_prioritarias_ccl.uso, shape_regiones_hidro_prioritarias_ccl.descono
FROM shape_regiones_hidro_prioritarias_ccl
WHERE ST_Intersects (shape_regiones_hidro_prioritarias_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_rhp = array();
while ($row = pg_fetch_assoc($query_intersects_rhp)) {
    $cadena = $row['nombre'];
    array_push($items_rhp, $cadena);
}
if (empty($items_rhp)) {
    array_push($items_rhp, "No se encontraron coincidencias");
}

#SITIOS PRIORITARIOS TERRESTRES
$query_intersects_spt = pg_query($link, "SELECT DISTINCT prioridad
FROM shape_sitios_prioritarios_terrestres_ccl
WHERE ST_Intersects (shape_sitios_prioritarios_terrestres_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY prioridad ASC");
$items_spt = array();
while ($row = pg_fetch_assoc($query_intersects_spt)) {
    $cadena = $row['prioridad'];
    array_push($items_spt, $cadena);
}
if (empty($items_spt)) {
    array_push($items_spt, "No se encontraron coincidencias");
}

##RAMSAR
$query_intersects_ramsar = pg_query($link, "SELECT DISTINCT ramsar, estado , municipios, fecha
FROM shape_ramsar_ccl
WHERE ST_Intersects (shape_ramsar_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY ramsar ASC");
$items_ramsar = array();
while ($row = pg_fetch_assoc($query_intersects_ramsar)) {
    $cadena = $row['ramsar'];
    array_push($items_ramsar, $cadena);
}
if (empty($items_ramsar)) {
    array_push($items_ramsar, "No se encontraron coincidencias");
}

##ANP FEDERALES
$query_intersects_anps_federales = pg_query($link, "SELECT DISTINCT nombre, cat_decret, cat_manejo, estados, municipios, region, superficie, s_terres, s_marina, prim_dec, ult_dof
FROM shape_anps_federales_ccl
WHERE ST_Intersects (shape_anps_federales_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_anps_federales = array();
while ($row = pg_fetch_assoc($query_intersects_ramsar)) {
    $cadena = $row['nombre'];
    array_push($items_anps_federales, $cadena);
}
if (empty($items_anps_federales)) {
    array_push($items_anps_federales, "No se encontraron coincidencias");
}

##ANP ESTATALES
$query_intersects_anps_estatales = pg_query($link, "SELECT DISTINCT nombre, tipo, estado, f_dec, categoria, fuente, sup_dec2
FROM shape_anps_estatales_ccl
WHERE ST_Intersects (shape_anps_estatales_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_anps_estatales = array();
while ($row = pg_fetch_assoc($query_intersects_anps_estatales)) {
    $cadena = $row['nombre'];
    array_push($items_anps_estatales, $cadena);
}
if (empty($items_anps_estatales)) {
    array_push($items_anps_estatales, "No se encontraron coincidencias");
}

##ANP MUNICIPALES
$query_intersects_anps_municipales = pg_query($link, "SELECT DISTINCT nombre, estado, f_dec, categoria, fuente, sup_dec
FROM shape_anps_municipales_ccl
WHERE ST_Intersects (shape_anps_municipales_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_anps_municipales = array();
while ($row = pg_fetch_assoc($query_intersects_anps_municipales)) {
    $cadena = $row['nombre'];
    array_push($items_anps_municipales, $cadena);
}
if (empty($items_anps_municipales)) {
    array_push($items_anps_municipales, "No se encontraron coincidencias");
}

##ZONAS ARQUEOLOGICAS
$query_intersects_zonas_arqueologicas = pg_query($link, "SELECT DISTINCT zonaarqueo, estado, municipio, culturas
FROM shape_zonas_arqueologicas_ccl
WHERE ST_Intersects (shape_zonas_arqueologicas_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY zonaarqueo ASC");
$items_zonas_arqueologicas = array();
while ($row = pg_fetch_assoc($query_intersects_zonas_arqueologicas)) {
    $cadena = $row['zonaarqueo'];
    array_push($items_zonas_arqueologicas, $cadena);
}
if (empty($items_zonas_arqueologicas)) {
    array_push($items_zonas_arqueologicas, "No se encontraron coincidencias");
}

##VEGETACION
$query_intersects_vegetacion = pg_query($link, "SELECT DISTINCT descripcio
FROM shape_vegetacion_serie_v_ccl
WHERE ST_Intersects (shape_vegetacion_serie_v_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY descripcio ASC");
$items_vegetacion = array();
while ($row = pg_fetch_assoc($query_intersects_vegetacion)) {
    $cadena = $row['descripcio'];
    array_push($items_vegetacion, $cadena);
}
if (empty($items_vegetacion)) {
    array_push($items_vegetacion, "No se encontraron coincidencias");
}

##ZERO EXTINCTION
$query_intersects_zero_extinction = pg_query($link, "SELECT DISTINCT especie, en_riesgo, estado, municipio, localidad, area_prote
FROM shape_zero_extinction_ccl
WHERE ST_Intersects (shape_zero_extinction_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY especie ASC");
$items_zero_extinction = array();
while ($row = pg_fetch_assoc($query_intersects_zero_extinction)) {
    $cadena = $row['especie'];
    array_push($items_zero_extinction, $cadena);
}
if (empty($items_zero_extinction)) {
    array_push($items_zero_extinction, "No se encontraron coincidencias");
}

#VIVEROS
$query_intersects_viveros = pg_query($link, "SELECT DISTINCT vivero, tipo, estado, municipio, localidad
FROM shape_viveros_biodiversidad_ccl
WHERE ST_Intersects (shape_viveros_biodiversidad_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY vivero ASC");
$items_vivero = array();
while ($row = pg_fetch_assoc($query_intersects_viveros)) {
    $cadena = $row['vivero'];
    array_push($items_vivero, $cadena);
}
if (empty($items_vivero)) {
    array_push($items_vivero, "No se encontraron coincidencias");
}

#REGIONES INDIGENAS
$query_intersects_regiones_indigenas = pg_query($link, "SELECT DISTINCT lengua
FROM shape_regiones_indigenas_ccl
WHERE ST_Intersects (shape_regiones_indigenas_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY lengua ASC");
$items_regiones_indigenas = array();
while ($row = pg_fetch_assoc($query_intersects_regiones_indigenas)) {
    $cadena = $row['lengua'];
    array_push($items_regiones_indigenas, $cadena);
}
if (empty($items_regiones_indigenas)) {
    array_push($items_regiones_indigenas, "No se encontraron coincidencias");
}

#UPGS
$query_intersects_upgs = pg_query($link, "SELECT DISTINCT nombre, especie, gerencia_r, estado, municipio, dependenci, propietari, densidad, rendimient, fecha_reco
FROM shape_upgs_ccl
WHERE ST_Intersects (shape_upgs_ccl.geom,ST_GeomFromText('$geom_text',48402))
ORDER BY nombre ASC");
$items_upgs = array();
while ($row = pg_fetch_assoc($query_intersects_upgs)) {
    $cadena = $row['nombre'];
    array_push($items_upgs, $cadena);
}
if (empty($items_upgs)) {
    array_push($items_upgs, "No se encontraron coincidencias");
}

if ($callback) {
    header('Content-Type: text/javascript');
    echo $callback . '({' .
    '"nombre_apc":' . json_encode($nombre_apc) . "," .
    '"folio":' . json_encode($folio) . "," .
    '"hectareas":' . json_encode($hectareas) . "," .
    '"estado":' . json_encode($estado) . "," .
    '"municipio":' . json_encode($municipio) . "," .
    '"vigencia":' . json_encode($vigencia) . "," .
    '"fecha_certificacion":' . json_encode($fecha_certificacion) . "," .
    '"numero_sedema":' . json_encode($numero_sedema) . "," .
    '"cuenta_plan_manejo":' . json_encode($cuenta_plan_manejo) . "," .
    '"ruta_fotografias":' . json_encode($ruta_fotografias) . "," .
    '"ruta_plan_de_manejo":' . json_encode($ruta_plan_de_manejo) . "," .
    '"altitud":' . json_encode($items_altitud) . "," .
    '"localidades":' . json_encode($items_localidades) . "," .
    '"vialidades":' . json_encode($items_vialidades) . "," .
    '"edafologia":' . json_encode($items_edafologia) . "," .
    '"geologia":' . json_encode($items_geologia) . "," .
    '"climas":' . json_encode($items_climas) . "," .
    '"cuencas":' . json_encode($items_cuencas) . "," .
    '"zonas_funcionales":' . json_encode($items_zonas_funcionales) . "," .
    '"umas":' . json_encode($items_umas) . "," .
    '"psa":' . json_encode($items_psa) . "," .
    '"pta":' . json_encode($items_pta) . "," .
    '"aicas":' . json_encode($items_aicas) . "," .
    '"rtp":' . json_encode($items_rtp) . "," .
    '"rhp":' . json_encode($items_rhp) . "," .
    '"spt":' . json_encode($items_spt) . "," .
    '"ramsar":' . json_encode($items_ramsar) . "," .
    '"anps_federales":' . json_encode($items_anps_federales) . "," .
    '"anps_estatales":' . json_encode($items_anps_estatales) . "," .
    '"anps_municipales":' . json_encode($items_anps_municipales) . "," .
    '"zonas_arqueologicas":' . json_encode($items_zonas_arqueologicas) . "," .
    '"vegetacion":' . json_encode($items_vegetacion) . "," .
    '"zero_extinction":' . json_encode($items_zero_extinction) . "," .
    '"viveros":' . json_encode($items_vivero) . "," .
    '"regiones_indigenas":' . json_encode($items_regiones_indigenas) . "," .
    '"upgs":' . json_encode($items_upgs)

        . '});';
} else {
    header('Content-Type: application/x-json');
    echo "{\"totalCount\":\"$count\",\"topics\":" . json_encode($items) . "}";
}
